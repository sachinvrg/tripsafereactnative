import Util from '../../common/util';
import Colors from '../../values/colors';
import { THEMEKEY } from '../../hooks/constants';

const GlobalStyles = {
  ViewStyle: {
    flex: 1,
   // backgroundColor: Colors.APPTHEME,
  },
  footerStyle: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  errorTextColor: {
    color: 'red',
    fontFamily: Util.getFontFamily('regular'),
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
},
  errorContainer: {},
  btnStyle: {
    // height: 55,
    height: Util.getHeight(6.5),
    width: Util.getWidth(80),
  },
  btnTextStyle: {
    fontFamily: Util.getFontFamily('regular'),
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: Util.normalize(14),
    lineHeight: 19,
  },
  searchInput: {
    width: Util.getWidth(80),
    borderColor: Colors.EXTRACOLORS.DEARKBORDER,
    elevation: 0.5,
    shadowColor: Colors.EXTRACOLORS.DEARKBORDER,
    alignSelf: 'center',
    fontSize: Util.normalize(28),
    borderWidth: 1,
  },
  safearea: {
    flex: 1,
    backgroundColor: Colors.WHITE,
},
};
export default GlobalStyles;
