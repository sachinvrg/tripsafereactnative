
import React, { useContext, useEffect, useState } from 'react';
import { View, Image, FlatList, StatusBar } from 'react-native';
import { Text } from 'galio-framework';

// Hooks, Api component calling
import AppContext from '../../hooks/app-context';
import { useHttp } from '../../hooks/useHttpss';
import {
  GET,

} from '../../hooks/constants';

// Global helper classes
// import HeaderComponent, {
//   AdminHeaderComponent,
// } from '../../components/DrawerHeader';

import strings from '../../values/stringEn';

// Local styles
import styles from './styles';
import Colors from '../../values/colors';
import Util from '../../common/util';
import helper from '../../common/helper';
import { TouchableOpacity } from 'react-native-gesture-handler';
import HeaderComponent from '../../components/HeaderComponent';
import LinearGradient from 'react-native-linear-gradient';
import images from '../../assets/Images/imagePath/images';


const RecentVisitedScreen = props => {
  const { navigate } = props.navigation;
  const context = useContext(AppContext);

  const { state } = context;



  useEffect(() => {


  }, []);




  return (
    <LinearGradient
      colors={['#113CB4', '#091850']}

      style={styles.linearGradient}
    >
      <StatusBar barStyle='light-content' hidden={false} backgroundColor="#113CB4" />

      <HeaderComponent
        title="Recently Visited"
        notification={true}

      />
      <View style={styles.viewContainer}>
        <View style={styles.buttonBottom} >
          <TouchableOpacity >
            <Image
              source={images.plusMore}
              resizeMode='contain' />
          </TouchableOpacity>
        </View>



      </View>

    </LinearGradient >
  );
};

export default RecentVisitedScreen;
