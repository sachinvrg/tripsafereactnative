import { StyleSheet } from 'react-native';
import Util from '../../common/util';
import Colors from '../../values/colors';

const styles = StyleSheet.create({
  button: {
    borderRadius: 50,
    backgroundColor: Colors.THEME.SUCCESS,
    paddingHorizontal: 50,
    paddingVertical: 12
  },
  textButton: {
    color: Colors.EXTRACOLORS.WHITE,
    fontSize: Util.normalize(13)
  },
  viewRow: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width:Util.getWidth(60),
    alignSelf:'center'
    // marginTop: '10%'
  },
  viewRound: {
    width: 50,
    alignItems: 'center',
    height: 50,
    borderRadius: 50 / 2,
    // marginRight: 15,
    backgroundColor: 'white'
  },
  iconStylefb: {
    flex: 1,
    width: 10,
    height: 10,
    resizeMode: 'contain'
  },
  iconStyle: {
    flex: 1,
    width: 20,
    height: 20,
    resizeMode: 'contain'
  }
});
export default styles;
