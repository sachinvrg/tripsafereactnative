import React, { useContext, useState } from 'react';
import {
    View,
    Image,
    FlatList,
    TouchableOpacity,
    Alert,
    ImageBackground,
} from 'react-native';

import { Icon, Text } from 'galio-framework';
// Hooks, Api component calling
import { USERDETAILS, USER_TYPE_TOOL_USER } from '../../hooks/constants';
import AppContext from '../../hooks/app-context';
import { SCREENINDEX } from '../../hooks/constants';

// Global helper classes
import Colors from '../../values/colors';
// Local styles
import styles from './styles';
import images from '../../assets/Images/imagePath/images';
import strings from '../../values/stringEn';
import helper from '../../common/helper';
import { StackActions, NavigationActions } from 'react-navigation';
import Util from '../../common/util';
import { BottomNavOption } from '../../constants/navigationConstants';

const BottomMenuScreen = props => {
    const context = useContext(AppContext);
    const { state } = context;
    console.log("State", state);
    const [screenState, setScreenState] = useState('Dashboard');


    return (
        <View style={[styles.bottomView]}>
            {(
                BottomNavOption
            ).map(item => {
                console.log("props.navigation", props.navigation.state.index);

                return (
                    <TouchableOpacity
                        style={styles.iconView}
                        onPress={() => {
                            context.dispatch({
                                type: SCREENINDEX,
                                payload: {
                                    index: item.index,
                                    screen: item.screen,
                                },
                            });
                            const resetToMain = StackActions.reset({
                                index: 0,
                                actions: [NavigationActions.navigate({ routeName: 'Home' })],
                                key: null,
                            });
                            props.navigation.dispatch(resetToMain);


                            props.navigation.navigate(item.screen);

                        }}>



                        <View style={props.navigation.state.index == item.index ? styles.bgActive : null}>

                            <Image
                                source={
                                    props.navigation.state.index == item.index
                                        ? images.BottomBar[item.active]
                                        : images.BottomBar[item.InActive]
                                }
                                resizeMode={'contain'}
                                style={styles.iconActive}
                            />
                            {props.navigation.state.index == item.index && <Text style={styles.bottomFont}>
                                {item.name}
                            </Text>}

                        </View>



                    </TouchableOpacity>
                );
            })}
        </View>
    );
};

export default BottomMenuScreen;
