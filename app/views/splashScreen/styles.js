import { StyleSheet } from 'react-native';
import Util from '../../common/util';

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  splashLogo: {
    height: Util.getHeight(30),
    width: Util.getWidth(50),
  },
  container: {
    flex: 1,
    resizeMode: 'cover', 
    width:'100%'
    // alignSelf: 'baseline'
  },
});
export default styles;
