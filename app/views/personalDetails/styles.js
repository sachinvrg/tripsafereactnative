import Util from '../../common/util';
import Colors, { getTheme } from '../../values/colors';
import { Platform, StyleSheet, Dimensions } from 'react-native';
import { THEMEKEY } from '../../hooks/constants';

const homeStyle = StyleSheet.create({
    linearGradient: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        // borderRadius: 5,

        width: '100%',
    },
    viewContainer: {
        flex: 1, width: '100%',
        borderTopLeftRadius: 30,
        backgroundColor: Colors.EXTRACOLORS.WHITE,
        borderTopRightRadius: 30,
        alignItems: 'center'
    },
    TabContainer: {
        flex: 1, width: '100%', alignItems: 'center'
    },
    tabViewOne: {
        flex: 1,
        width: '100%',
        height: Util.getHeight(75),

        alignItems: 'center',
        // backgroundColor: 'green'

    },
    tabViewThree: {
        flex: 1,
        width: '100%',
        height: Util.getHeight(120),

        alignItems: 'center',
        // backgroundColor: 'green'

    },
    inputView: {
        alignSelf: 'center',
        width: Util.getWidth(80),
        height: Util.getHeight(10),
        borderColor: Colors.EXTRACOLORS.APPTEXT,
        elevation: 0.5,

        shadowColor: Colors.EXTRACOLORS.APPTEXT,
    },

    textParegraph: {
        width: Util.getWidth(75),
        marginTop: Util.getHeight(2),
        marginBottom:Util.getHeight(2),
        fontSize: Util.normalize(12)
    }

});
export default homeStyle;
