import {THEMEKEY} from '../../app/hooks/constants';

const Colors1 = {
  APPTHEME: '#2E71E2',

  NavyBlue:'#091850',


  AppTheme: {
    AppBackground: '#FFFFFF',
    AppBackgroundSecondary: '#E5E5E5',

    ColorPrimary: '#357DE5',
    ColorSecondary: '#DCE5F4',

    ColorTertiary: '#EFF4FC',
  },

  TextColor: {
    PrimaryTextColor: '#082344',
    SecondaryTextColor: 'rgba(8, 35, 68, 0.3)',

    LabelTextPrimary: 'rgba(8, 35, 68, 0.6)',
    LabelTextSecondary: '#134999',

    ErrorPrimary: '#FF0000',
    ErrorSecondary: '#EC4B4B',

    Warning: '#FF9C09',
    Success: '#45DF31',
  },

  BorderColor: {
    BorderColorPrimary: '#E4E9F2',
    BorderColorSecondary: '#DCE5F4',
  },

  ///////////////////////////////////////////

  COMPONENTS: {
    INPUT: '#0099FF',
    PLACEHOLDER: '#AAC2E5',
    NAVBAR: '#F9F9F9',
    BLOCK: '#808080',
    ICON: '#AAC2E5',
    border: '#dedede',
  },
  THEME: {
    THEME: '#B23AFC',
    PRIMARY: '#B23AFC',
    INFO: '#1232FF',
    ERROR: '#FE2472',
    WARNING: '#FF9C09',
    SUCCESS: '#04AC22',
    BACKGROUND: '#FFFFFF',
    BACKGROUND1: 'red',


    TEXTCOLOR: '#082344',
  },
  EXTRACOLORS: {
    APPCOLOR: '#CCDFFB',
    APPTEXT: '#4E8AE2',
    WHITE: '#FFFFFF',
    BLACK: '#000000',
    GREY: '#898989',
    ICONCOLOR: '#082344',
    MUTED: '#9FA5AA',
    TRANSPARENT: 'transparent',
    NEUTRAL: 'rgba(255,255,255, 0.65)',
    ICONBACK: 'rgba(204, 223, 251, 0.65)',
    HINT: '#ebe8e8',
    LIGHTTEXT: 'rgba(29,25,82,0.8)',
    // BORDER: 'rgba(29,25,82,0.2)',
    BORDER: '#CCDFFB',
    DEARKBORDER: 'rgba(46, 113, 226,0.4)',
    CARDCOLOR: '#FFFFFF',
    DARKBLUE: '#102A4A',
    BLACK_SECONDARY: '#222',
    BORDERCOLOR: '#CCDFFB',
    ActiveColor:'#3366FF'
  },
  TEXTCOLORS: {
    CLIENTLOGO: '#3B5998',
    DATEBTNCOLOR: '#7a899a',
    INACTIVETABCOLOR: '#5c708a',
  },
  BUTTONCOLOR: '#357DE5',
  SEARCHBAR: '#6B7B8F',
  HEADERTITLE: '#082344',
  MODELBACKGROUDCOLOR: 'rgba(17, 60, 180, 0.6)',
  MODELBACKGROUDCOLORPROFILE: 'rgba(0, 0, 0, 0.9)',

  MODELCONTAINERBACKGROUDCOLOR: '#ecf0f1',
  MODELNUMBERBTNCOLOR: 'rgba(204, 223, 251, 0.35)',
  MODELNUMBERHEADINGCOLOR: '#082344',
  DATEPICKERRIGHTARROW: '#cdd3d9',
  BLACK: '#000000',
};

const Colors2 = {
  APPTHEME: '#3B5998',
  NavyBlue:'#091850',
  AppTheme: {
    AppBackground: '#121212',
    AppBackgroundSecondary: '#FFFFFF',

    ColorPrimary: '#B23AFC',
    ColorSecondary: '#DCE5F4',
    ColorTertiary: '#ebe8e8',
  },

  TextColor: {
    PrimaryTextColor: '#082344',
    SecondaryTextColor: '#8F9BB3',

    LabelTextPrimary: 'rgba(8, 35, 68, 0.6)',
    LabelTextSecondary: '#134999',

    ErrorPrimary: '#EB5757',
    ErrorSecondary: '#EC4B4B',

    Warning: '#FF9C09',
    Success: '#45DF31',
  },

  BorderColor: {
    BorderColorPrimary: '#102A4A',
    BorderColorSecondary: '#DCE5F4',
  },

  ////////////////////////////////////

  COMPONENTS: {
    INPUT: '#B23AFC',
    PLACEHOLDER: '#FFFFFF',
    NAVBAR: '#F9F9F9',
    BLOCK: '#808080',
    ICON: '#AAC2E5',
    border: '#dedede',
  },
  THEME: {
    THEME: '#B23AFC',
    PRIMARY: '#B23AFC',
    INFO: '#1232FF',
    ERROR: '#FE2472',
    WARNING: '#FF9C09',
    SUCCESS: '#45DF31',
    BACKGROUND: '#121212',
    TEXTCOLOR: '#FFFFFF',
  },
  EXTRACOLORS: {
    APPCOLOR: '#CCDFFB',
    APPTEXT: '#B23AFC',
    WHITE: '#FFFFFF',
    BLACK: '#ebe8e8',
    GREY: 'rgba(8, 35, 68, 0.6)',
    ICONCOLOR: '#FFFFFF',
    MUTED: '#9FA5AA',
    TRANSPARENT: 'transparent',
    NEUTRAL: 'rgba(255,255,255, 0.65)',
    ICONBACK: 'rgba(205, 228, 298, 0.89)',
    HINT: '#ebe8e8',
    LIGHTTEXT: '#ebe8e8',
    BORDER: '#E0E0E0',
    DEARKBORDER: '#CCDFFB',
    CARDCOLOR: '#2A2A2A',
    DARKBLUE: '#102A4A',
    BORDERCOLOR: '#CCDFFB',
    ActiveColor:'#3366FF'

  },
  TEXTCOLORS: {
    CLIENTLOGO: '#3B5998',
    SIDEBARTEXT: '#292626',
    INACTIVETABCOLOR: '#5c708a',
  },
  BUTTONCOLOR: '#B23AFC',
  SEARCHBAR: '#ebe8e8',

  HEADERTITLE: '#ebe8e8',
  MODELBACKGROUDCOLOR: 'rgba(0, 0, 0, 0.9)',
  MODELBACKGROUDCOLORPROFILE: 'rgba(0, 0, 0, 0.9)',

  MODELCONTAINERBACKGROUDCOLOR: '#ecf0f1',
  MODELNUMBERBTNCOLOR: '#dce5f4',
  MODELNUMBERHEADINGCOLOR: '#082344',
  DATEPICKERRIGHTARROW: '#fff',
};
const getTheme = THEMEKEY => {
  switch (THEMEKEY) {
    case 1:
      return Colors1;
      break;
    case 2:
      return Colors2;
      break;
    default:
      break;
  }
};
const Colors = getTheme(THEMEKEY);
export default Colors;
// #9B1F19
