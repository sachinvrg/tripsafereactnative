// jay soni (24/6/2020)

import React from 'react';
import { View, SafeAreaView, StatusBar } from 'react-native';
import TripSafe from './App';
import Styles from './app/assets/css/globalStyle'
import { FixScreenWithoutPadding } from './app/components/WrapperView/FixScreenWithoutPadding';
import RecentTestScreen from './app/views/homeScreen/RecentTestScreen';
import SplashScreen from './app/views/splashScreen';
import { GlobalState } from './app/hooks/globalState';


// const returnAppView = () => {
//   return (
//     <TripSafe />

//   )
// }


const Application = props => {
  return (
    <View style={{flex:1}}>
      <GlobalState>
        <TripSafe />
      </GlobalState>
    </View>
    // <FixScreenWithoutPadding children={returnAppView}/>
  );
};

export default Application;