// 'use strict';

import React, { useEffect, useContext, useState } from 'react';
import {
    Text,
    TextInput,
    View,
    Animated, Dimensions,
    TouchableOpacity,
    Image, ImageBackground, StyleSheet,
    Alert,
    StatusBar,
    Nativ
} from 'react-native';
import { Checkbox } from 'galio-framework'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-simple-toast';
import DeviceInfo from 'react-native-device-info';

import { getUniqueId, getManufacturer } from 'react-native-device-info';

import {
    POST, CREDENTIALS, USERDETAILS
} from '../../hooks/constants';
import { useHttp } from '../../hooks/useHttpss';

import AppContext from '../../hooks/app-context';

import COLOR from '../../values/colors';
import strings from '../../values/stringEn';
import styles from './styles';
import Images from '../../assets/Images/imagePath/images';
import Scroller from "./scroller";
import helper from '../../common/helper';

import { signIn, forgotPassword } from '../../network/Networksettings';


import { ScrollView } from 'react-native-gesture-handler';
import { TextInputComponent } from '../../components/TextInput';
import { ButtonComponent } from '../../components/Button';
import { SocialLoginComponent } from '../../components/SocialLoginButton';
import images from '../../assets/Images/imagePath/images';
var mailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const LoginScreen = props => {
    const { navigation } = props;
    const context = useContext(AppContext);
    console.log("ContextonLigin", context);

    const { state } = context;
    const [loader, setLoader] = useState(false);
    const [isFocusedMail, setIsFoucusedMail] = useState(false);
    const [isFocusedResetMail, setIsFoucusedResetMail] = useState(false);
    const [emailCorrect, setEmailCorrect] = useState(false);
    const [resetEmailCorrect, setResetEmailCorrect] = useState(false);


    const [deviceId, setDeviceId] = useState('');
    const [isFocusedPassword, setIsFocusedPassword] = useState(false);
    const [animation, setAnimation] = useState(new Animated.Value(0));
    const screenHeight = Dimensions.get("window").height;

    const [formDataResetPass, setFormDataResetPass] = useState({
        email: {
            err: false,
            errMsg: '',
            propname: 'email',
            value: '',
            required: true,
            checkMobile: true,
        }
    })

    const [formData, setFormData] = useState({
        email: {
            err: false,
            errMsg: '',
            propname: 'email',
            value: '',
            required: true,
            checkMobile: true,
        },
        password: {
            err: false,
            errMsg: '',
            propname: 'password',
            value: '', 
            required: true,
        },
        checkbox: {
            propname: strings.propName.password,
            value: false,
            required: false,
        },

    });
    const handleOpen = () => {

        Animated.timing(animation, {
            toValue: 1,
            duration: 300,
            useNativeDriver: true,
        }).start();
    };
    const handleClose = () => {
        Animated.timing(animation, {
            toValue: 0,
            duration: 200,
            useNativeDriver: true,
        }).start();
    };

    const backdrop = {
        transform: [
            {
                translateY: animation.interpolate({
                    inputRange: [0, 0.01],
                    outputRange: [screenHeight, 0],
                    extrapolate: "clamp",
                }),
            },
        ],
        opacity: animation.interpolate({
            inputRange: [0.01, 0.5],
            outputRange: [0, 1],
            extrapolate: "clamp",
        }),
    };

    const slideUp = {
        transform: [
            {
                translateY: animation.interpolate({
                    inputRange: [0.01, 1],
                    outputRange: [0, -1 * screenHeight],
                    extrapolate: "clamp",
                }),
            },
        ],
    };

    const changeTextReset = (name, value) => {

        if (value.match(mailformat)) {
            setResetEmailCorrect(true);
        } else {
            setResetEmailCorrect(false);
        }
        setFormDataResetPass({
            ...formDataResetPass,
            [name]: {
                ...formDataResetPass[name],
                err: false,
                errMsg: '',
                value,
                propname: name,
            },
        });
    }

    const changeText = (name, value) => {
        if (name === 'email') {

            if (value.match(mailformat)) {
                setEmailCorrect(true);
            } else {
                setEmailCorrect(false);
            }
        }
        setFormData({
            ...formData,
            [name]: {
                ...formData[name],
                err: false,
                errMsg: '',
                value,
                propname: name,
            },
        });
    };

    const handleFocusPassword = () => {
        setIsFocusedPassword(true);
    }

    const handleBlurPassword = () => {
        setIsFocusedPassword(false);
    }


    const handleFocusMail = () => {
        setIsFoucusedMail(true);
    }

    const handleBlurMail = () => {
        setIsFoucusedMail(false);
    }
    const handelFocusReset = () => {
        setIsFoucusedResetMail(true);
    }

    const handelBlurReset = () => {
        setIsFoucusedResetMail(false);

    }
    useEffect(() => {
        getDeviceId()
        helper
            .fetch_item_async(CREDENTIALS)
            .then(data => {
                const parsedData = JSON.parse(data);
                if (parsedData) {
                    setFormData(parsedData);
                }
            })
            .catch(() => { });

            

    }, []);
    const getDeviceId = () => {

        var id = DeviceInfo.getUniqueId();
        console.log('device_id', id);

        setDeviceId(id);


    }
    // navigation.navigate('Home')

    const onLoginClick = () => {
        // setEmailCorrect(true);
        setLoader(true);
        helper.validateLoginForm(formData).then(({ error, formDataObj }) => {
            if (!error) {
                if (formData.checkbox.value) {
                    helper.store_item_async(CREDENTIALS, JSON.stringify(formData));
                } else {
                    helper.store_item_async(CREDENTIALS, null);
                }
                const loginRequest = helper.processData(formData);
                delete loginRequest.checkbox;
                loginApi(loginRequest);
            } else {
                setFormData(formDataObj);
                setTimeout(() => {
                    setLoader(false);
                }, 100);
            }
        });
    };


    const onResetClick = () => {

        setLoader(true);

        helper.validateLoginForm(formDataResetPass).then(({ error, formDataObj }) => {
            if (!error) {

                const resetRequest = helper.processData(formDataResetPass);
                forgotPasswordApi(resetRequest);

            } else {
                setFormDataResetPass(formDataObj);
                setTimeout(() => {
                    setLoader(false);
                }, 100);
            }
        });

    }

    const forgotPasswordApi = async body => {
        setEmailCorrect(false);

        body.email = body.email.toLowerCase();
        // delete body.email

        // body.email = 'sanket.zeeweesoft@gmail.com';
        // body.device_name = deviceId
        setLoader(true);
        await useHttp(forgotPassword, POST, null, body)
            .then(res => {
                setLoader(false);
                setEmailCorrect(false);

                setFormDataResetPass({
                    email: {
                        err: false,
                        errMsg: '',
                        propname: 'email',
                        value: '',
                        required: true,
                        checkMobile: true,
                    }
                })
                console.log("response", res.data.message);
                handleClose()
                setEmailCorrect(false);

                Alert.alert("Success", res.data.message, [
                    { text: strings.Ok_btn, onPress: () => setLoader(false) }
                ]);


            })
            .catch(err => {
                console.log('error', err);

                setLoader(false);
                handleClose()
                setFormDataResetPass({
                    email: {
                        err: false,
                        errMsg: '',
                        propname: 'email',
                        value: '',
                        required: true,
                        checkMobile: true,
                    }
                })

                setTimeout(() => {

                    Alert.alert("Error", err.errors.email[0], [
                        { text: strings.Ok_btn, onPress: () => setLoader(false) }
                    ]);
                }, 100);

            });
    };
    const loginApi = async body => {
        body.email = body.email.toLowerCase();
        body.device_name = deviceId
        setLoader(true);
        await useHttp(signIn, POST, null, body)
            .then(res => {
                setLoader(false);
                console.log("response", res.data.data.token);
                const userDetails = res.data.data.user;
                userDetails.authorization = res.data.data.token;

                console.log('UserDetails', JSON.stringify(userDetails));
                helper.store_item_async(USERDETAILS, JSON.stringify(userDetails));

                context.dispatch({
                    type: USERDETAILS,
                    payload: userDetails,
                });
                navigation.navigate('Home')

                Toast.show(
                    'Logged in successfully',
                    Toast.LONG,
                );


            })
            .catch(err => {
                console.log('error', err.errors.email);

                setLoader(false);

                setTimeout(() => {

                    Alert.alert("Error", err.errors.email[0], [
                        { text: strings.Ok_btn, onPress: () => setLoader(false) }
                    ]);
                }, 100);

            });
    };

    return (

        <View style={styles.containerScroll} >
            <StatusBar hidden={false} backgroundColor="#113CB4" />

            <Spinner visible={loader} />

            <ImageBackground source={Images.Bg} style={styles.container}>
                <KeyboardAwareScrollView
                    extraScrollHeight={10}
                    showsVerticalScrollIndicator={false}
                    enableOnAndroid={true}
                    scrollEnabled
                    keyboardShouldPersistTaps={'always'}>


                    <View style={styles.boxContainer}>
                        <View style={styles.imageContainer}>
                            <Image
                                style={styles.imageStyle}
                                source={Images.logoV}
                            />
                        </View>

                    </View>


                    <View style={styles.loginBox}>
                        <View style={styles.box3} />
                        <View style={styles.box2} />
                        <View style={styles.box1}>
                            <Text style={styles.loginContinue}>{strings.loginToContinue}</Text>
                            <Text style={styles.pleaseEnterCredential}>{strings.pleaseEnterCredentials}</Text>
                            <TextInputComponent
                                ActiveTint={isFocusedMail ? true : false}
                                onFocus={handleFocusMail}
                                onBlur={handleBlurMail}
                                iconLeft={Images.email}
                                placeholder={strings.enterEmail}
                                iconLeftShow={true}
                                iconRight={Images.checked}
                                iconRightShow={emailCorrect ? true : false}
                                onChangeText={text => changeText('email', text)}
                                help={formData.email.errMsg}
                                value={formData.email.value}

                            />
                            <TextInputComponent
                                ActiveTint={isFocusedPassword ? true : false}
                                onFocus={handleFocusPassword}
                                onBlur={handleBlurPassword}
                                iconLeft={Images.lock}
                                iconLeftShow={true}
                                onChangeText={text => changeText('password', text)}
                                // bottomHelp={formData.password.err}
                                help={formData.password.errMsg}
                                value={formData.password.value}
                                secureTextEntry={true}
                                placeholder={strings.enterPassword}
                            />
                            <View style={styles.radioForgetView}>
                                <Checkbox
                                    checkboxStyle={styles.checkbox}
                                    label={'Remeber Me'}
                                    color={'#3366FF'}
                                    labelStyle={styles.rememberMeText}
                                    labelStyle={styles.rememberMeText}
                                    onChange={value => changeText('checkbox', value)}
                                    checked={formData.checkbox.value}

                                />
                                <TouchableOpacity onPress={() => handleOpen()}>
                                    <Text style={styles.forgetPsw}>
                                        {'Forgot password?'}
                                    </Text>
                                </TouchableOpacity>
                            </View>

                            <View style={{ marginTop: '10%' }}>
                                <ButtonComponent
                                    onClick={() => onLoginClick()}
                                    buttonText={strings.login} />
                            </View>
                        </View>




                    </View>

                    <SocialLoginComponent
                        iconOne={Images.iconFb}
                        iconTwo={Images.iconGoogle}
                        iconThree={images.iconApple} />
                    <TouchableOpacity onPress={() => navigation.navigate('SignUpScreen')} style={{ marginTop: '5%', alignSelf: 'center' }}>
                        <Text style={{ color: 'white', fontSize: 12 }}>
                            {'Don`t have an account?'}<Text style={{
                                fontWeight: 'bold',
                                textDecorationLine: 'underline'
                            }}>{'Sign up here'}</Text>

                        </Text>

                    </TouchableOpacity>


                </KeyboardAwareScrollView>
                <Animated.View style={[StyleSheet.absoluteFill, styles.cover, backdrop]}>
                    <View style={[styles.sheet]}>
                        <Animated.View style={[styles.popup, slideUp]}>
                            <View style={{ width: "100%", flexDirection: 'row', paddingVertical: '5%', justifyContent: 'space-evenly' }} >
                                <Text style={styles.resetPassword}>{strings.resetPassword}</Text>
                                <TouchableOpacity onPress={() => handleClose()} style={styles.closeButton}>
                                    <Image
                                        style={
                                            styles.iconStyle
                                        }
                                        source={images.crossIcon}
                                        tintColor={'black'}
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={{ width: "100%", flexDirection: 'row' }} >

                                <Image
                                    style={
                                        styles.iconForgotPass
                                    }
                                    source={images.forgotPassword}
                                />
                            </View>
                            <Text style={styles.forgotPasswordTetx}>
                                {strings.forgotPasswordText}
                            </Text>
                            <Text style={styles.forgotPasswordSentence}>
                                {strings.forgetSentence}
                            </Text>
                            <View style={{ width: '80%', marginTop: '2%' }}>
                                <TextInputComponent
                                    iconLeftShow={false}
                                    label
                                    labelText={strings.emailAddress}
                                    ActiveTint={isFocusedResetMail ? true : false}
                                    onFocus={handelFocusReset}
                                    onBlur={handelBlurReset}
                                    placeholder={strings.enterEmail}

                                    iconRight={Images.checked}
                                    iconRightShow={resetEmailCorrect ? true : false}
                                    onChangeText={text => changeTextReset('email', text)}
                                    help={formDataResetPass.email.errMsg}
                                    value={formDataResetPass.email.value}

                                />
                            </View>
                            <View style={{ marginTop: '5%' }}>
                                <ButtonComponent onClick={() => onResetClick()}
                                    buttonText={strings.resetPassword} />
                            </View>
                            <Scroller />
                        </Animated.View>
                    </View>
                </Animated.View>
            </ImageBackground>
        </View>
    )

}
export default LoginScreen;
