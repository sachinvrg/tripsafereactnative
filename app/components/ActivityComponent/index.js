import React, { useContext } from 'react';
import {
    View,
    TouchableOpacity,
    ScrollView,
    ImageBackground,
    TextInput,
    Image,
} from 'react-native';
import { SafeAreaView } from 'react-navigation';
import {
    Input,
    Icon,
    Text,
    Button,
    Checkbox,
} from 'galio-framework';

import Styles from './styles';
import Colors from '../../values/colors';
import styles from './styles';
import images from '../../assets/Images/imagePath/images';
import Util from '../../common/util';


export const RecentActivityComponent = (props) => {
    const { buttonText, onClick } = props;
    return (
        <TouchableOpacity onPress={onClick}>
            <View style={Styles.cardView}>
                <View style={{ flexDirection: 'row', height: '100%', justifyContent: 'space-between' }}>
                    <View style={styles.viewLeft}>
                        <View style={{backgroundColor:'#113CB4', width:'4%',height:Util.getHeight(10),}} />

                        <Image
                            style={
                                styles.qrImage
                            }
                            source={images.ic}
                        />
                    </View>
                    <View style={styles.viewRight}>

                        <Text style={styles.textId}>
                            Updated
                        </Text>
                        <Text style={styles.textIdValue}>
                            {'11 May 2020'}<Text style={styles.textId}>{' 6:30 PM'}</Text>
                        </Text>

                        <Text style={styles.testReport}>
                            {'Negative'}
                        </Text>
                    </View>

                </View>

            </View>
        </TouchableOpacity>
    )
};

