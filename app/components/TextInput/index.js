import React, { useContext } from 'react';
import {
    View,
    TouchableOpacity,
    ScrollView,
    ImageBackground,
    TextInput,
    Image,
} from 'react-native';
import { SafeAreaView } from 'react-navigation';
import {
    Input,
    Icon,
    Text,
    Button,
    Checkbox,
} from 'galio-framework';

import Styles from './styles';
import Colors from '../../values/colors';


export const TextInputComponent = (props) => {
    const { value, help, onChangeText, iconRight, iconRightShow, iconLeft, type, label, labelText, iconLeftShow, onFocus, onBlur, placeholder, secureTextEntry, extraStyle, ActiveTint } = props;

    console.log('help', help);

    return (
        <View>
            <View style={[Styles.formGroup]}>
                {label && <Text style={ActiveTint ? Styles.inputLableActive : Styles.inputLable}>{labelText}</Text>
                }
                <View style={ActiveTint ? Styles.inputViewActive : Styles.inputViewInActive}>
                    {iconLeftShow &&
                        <View style={Styles.inputRow}  >
                            <Image
                                resizeMode={'contain'}
                                style={{ alignSelf: 'center' }}
                                source={iconLeft}
                                tintColor={ActiveTint || labelText == "Address" ? Colors.EXTRACOLORS.ActiveColor : 'lightgray'}
                            />
                        </View>
                    }

                    <TextInput
                        onFocus={onFocus}
                        onBlur={onBlur}
                        style={Styles.textInput}
                        placeholder={placeholder}
                        secureTextEntry={secureTextEntry}
                        keyboardType={type}
                        onChangeText={onChangeText}

                        value={value}
                    />
                    {iconRightShow &&
                        <View style={Styles.rightIconView}>

                            <Image
                                resizeMode={'contain'}
                                source={iconRight}
                                placeholder="Email"
                            />
                        </View>}



                </View>
                {help != null && help != '' && <Text style={Styles.helpText}>{help}</Text>}

            </View>


        </View>
    )
};

