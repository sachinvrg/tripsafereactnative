// @ created by shivani lakhena
import React, { useState } from 'react';
import {
  View,
  Modal,
  Image,
  KeyboardAvoidingView,
  TouchableOpacity,
} from 'react-native';
import { Text, Button, Icon } from 'galio-framework/src';
import { ScrollView } from 'react-native-gesture-handler';
// Hooks, Api component calling

// Global helper classes
import Colors from '../../values/colors';
import strings from '../../values/stringEn';

// Local styles and Images
import styles from './style';
import images from '../../assets/Images/imagePath/images';
import Util from '../../common/util';

const FloatingButtonModal = props => {
  const {
    ismodel,
    titleText,
    btnText,
    onButtonClick,
    activateMessage,
    isClose,
    hideTitle,
    cancelText,
    button,
    onShow,
    qrScan
    
  } = props;

  const [modalVisible, setModalVisible] = useState(ismodel);
  const showModalVisible = visible => {
    onShow(false);
    setModalVisible(visible);
  };

  return (
    <View >
      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => showModalVisible(false)}>

        <View
          style={[
            styles.container,
          ]}>
          <View style={{ width: '100%', height: '50%' }}>
            <View style={styles.horizontalView1}>

              <View style={styles.horizontalView2} >
                <Text style={styles.textFloatingButton}>
                  {"Sacn Qr Code"}
                </Text>
                <TouchableOpacity onPress={qrScan} style={styles.floatingLabe2} >

                  <Image
                    source={images.scan}
                    resizeMode='contain' />

                </TouchableOpacity>

              </View>
            </View>
            <View style={styles.horizontalView1}>

              <View style={styles.horizontalView2} >
                <Text style={styles.textFloatingButton}>
                  {"Add Test via Ref.No."}
                </Text>
                <TouchableOpacity style={styles.floatingLabe2} >

                  <Image
                    source={images.AddTest}
                    resizeMode='contain' />

                </TouchableOpacity>

              </View>
            </View>
            <View style={styles.horizontalView1}>

              <View style={styles.horizontalView2} >
                <Text style={styles.textFloatingButton}>
                  {"Check-in to location"}
                </Text>
                <TouchableOpacity style={styles.floatingLabe2} >

                  <Image
                    source={images.checkIn}
                    resizeMode='contain' />

                </TouchableOpacity>

              </View>
            </View>

            <View style={styles.horizontalView1}>

              <View style={styles.horizontalView2} >
                <Text style={styles.textFloatingButton}>
                  {"Add Travel Doc / Visa"}
                </Text>
                <TouchableOpacity style={styles.floatingLabe2} >

                  <Image
                    source={images.AddTravelDoc}
                    resizeMode='contain' />

                </TouchableOpacity>

              </View>
            </View>



          </View>

          <TouchableOpacity onPress={onShow} style={styles.floatingLabel} >

            <Image
              source={images.cross}
              resizeMode='contain' />

          </TouchableOpacity>

        </View>

      </Modal>
    </View>
  );
};
export default FloatingButtonModal;
