import {StyleSheet, Platform} from 'react-native';
import Util from '../../common/util';

import Colors, {getTheme} from '../../values/colors';
import {THEMEKEY} from '../../hooks/constants';
const styles = StyleSheet.create({
  flex1: {
    flex: 1,
  },

  //Search Box UI Styles
  searchText: {
    fontWeight: 'bold',
    color: Colors.SEARCHBAR,
    fontSize: Util.normalize(15),
    fontFamily: Util.getFontFamily('regular'),
  },
  separatorStyle: {
    width: Util.getWidth(0.5),
    height: Util.getHeight(2),
    backgroundColor: Colors.AppTheme.ColorPrimary,
    marginLeft: 6,
    marginRight: 1,
  },
  innerTab: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: 5,
  },

  tab: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
  tabText: {
    marginHorizontal: 20,
    paddingVertical: 10,
    fontWeight:'bold',
    // fontFamily: Util.getFontFamily('bold'),
    fontSize: Util.normalize(12),
    // borderBottomWidth: 3,
    // borderColor: Colors.AppTheme.ColorPrimary,
  },
  tabTextDisabled: {
    marginHorizontal: 20,
    paddingVertical: 10,
    fontWeight:'bold',

    // fontFamily: Util.getFontFamily('bold'),
    fontSize: Util.normalize(12),
    color: Colors.SEARCHBAR,
  },
  tabBorder: {
    height: 3,
    width: '90%',
    backgroundColor: Colors.THEME.SUCCESS,
    alignSelf: 'center',
  },
  tabBorder2: {
    borderBottomWidth: 3,
    borderBottomColor: Colors.AppTheme.ColorPrimary,
    // alignSelf: 'center',
  },
  textSepratorView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    overflow: 'hidden',
  },
  //Filter UI Styles
  filterView: {
    alignSelf: 'flex-end',
    marginRight: '2%',
    height: Util.getHeight1(2),
    alignItems: 'center',
    flexDirection: 'row',
    // backgroundColor:"red"
  },
  // filterText: {
  //   fontWeight: '700',
  //   fontSize: 13,
  //   fontFamily: Util.getFontFamily('regular'),
  //   color: Colors.EXTRACOLORS.ICONCOLOR,
  //   marginLeft: 5,
  // },
  filterText: {
    color: Colors.EXTRACOLORS.ICONCOLOR,
    marginLeft: 5,
    fontFamily: Util.getFontFamily('regular'),
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: Util.normalize(11),
    lineHeight: 14,
  },

  //Tab Component UI Styles
  tabUI: {
    height: Util.getHeight(6),
    width: '100%',
    flexDirection: 'row',
    backgroundColor: Colors.EXTRACOLORS.WHITE,
    borderRadius: 23,
    borderColor: Colors.AppTheme.ColorPrimary,
    borderWidth: 1,
    overflow: 'hidden',
    //paddingHorizontal: 8,
  },

  tabUITwo: {
    height: Util.getHeight(6),
    width: '100%',
    flexDirection: 'row',
    backgroundColor: Colors.EXTRACOLORS.WHITE,
    borderRadius: 23,
    borderColor: Colors.AppTheme.ColorPrimary,
    borderWidth: 1,
    paddingHorizontal: 8,
  },
  selectedtabItem: {
    flex: 1,
    backgroundColor: Colors.AppTheme.ColorPrimary,
    margin: Util.getWidth(1),
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    // paddingHorizontal: 10,
  },
  unSelectedtabItem: {
    flex: 1,
    margin: Util.getWidth(1),
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: 'red',
  },
  selectedtabText: {
    color: Colors.EXTRACOLORS.WHITE,
    fontSize: Util.normalize(10.2),
    fontFamily: Util.getFontFamily('regular'),
    overflow: 'hidden',
  },
  unSelectedtabText: {
    color: Colors.AppTheme.ColorPrimary,
    fontSize: Util.normalize(10.2),
    fontFamily: Util.getFontFamily('regular'),
  },
  btnStyle: {
    height: Util.getHeight(7),
    width: Util.getWidth(39),
  },
  btnStyle1: {
    height: Util.getHeight(6.5),
    width: Util.getWidth(40),
    borderColor: Colors.AppTheme.ColorPrimary,
    borderWidth: 1,
  },
  btnCancel: {
    color: Colors.AppTheme.ColorPrimary,
    fontWeight: 'bold',
    fontFamily: Util.getFontFamily('regular'),
    fontSize: Util.normalize(15),
  },
  ScanViewButton: {
    flexDirection: 'row',
    width: '100%',
    alignSelf: 'center',
    justifyContent: 'space-between',
  },
  btnChek: {
    // width: '100%',
    // backgroundColor: 'blue'
  },
  btnCheckStyle: {
    fontWeight: '700',
    fontFamily: Util.getFontFamily('regular'),
  },
  ScanView: {
    flexDirection: 'row',
    width: '85%',
    alignSelf: 'center',
    marginVertical: Util.getHeight(1.5),
  },
  scanOuterView: {
    backgroundColor: Colors.MODELNUMBERBTNCOLOR,
    marginHorizontal: '2%',
    borderRadius: 7,
    flex: 1,
    alignItems: 'center',
    paddingVertical: Util.getHeight(1.5),
  },
  imageStyle: {
    width: Util.getWidth(10),
    height: Util.getWidth(10),
  },
  countText: {
    flexWrap: 'wrap',
    color: Colors.HEADERTITLE,
    fontWeight: 'bold',
    fontSize: Util.normalize(13),
    letterSpacing: 0.3,
    fontFamily: Util.getFontFamily('regular'),
  },
  imageStyle1: {
    width: Util.getWidth(7.8),
    height: Util.getWidth(7.8),
    marginTop: Util.getHeight(0.6),
  },

  scannImage: {
    width: Util.getWidth(15),
    height: Util.getWidth(10),
  },
  scannImage2: {
    width: Util.getWidth(13),
    height: Util.getWidth(8),
    backgroundColor: Colors.WHITE,
  },
  scannedText: {
    flexWrap: 'wrap',
    color: Colors.HEADERTITLE,
    fontWeight: 'bold',
    letterSpacing: 0.3,
    fontFamily: Util.getFontFamily('regular'),
    fontSize: Util.normalize(15),
  },

  scannedTextTwo: {
    flexWrap: 'wrap',
    color: Colors.HEADERTITLE,
    fontWeight: 'bold',
    letterSpacing: 0.3,
    fontFamily: Util.getFontFamily('regular'),
    fontSize: Util.normalize(11),
  },
  TextStyle: {
    flex: 2,
    alignItems: 'flex-end',
    marginRight: '8%',
  },
  dataFonts: {
    fontFamily: Util.getFontFamily('regular'),
    fontSize: Util.normalize(15),
  },
  cardView: {
    flexDirection: 'row',
    marginHorizontal: '2%',
    borderBottomWidth: 0.5,
    borderColor: Colors.EXTRACOLORS.BORDER,
    height: Platform.OS === 'android' ? Util.getHeight(12) : Util.getHeight(10),
    // flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  userList: {
    height: Util.getHeight(8),
    width: '100%',
    flexDirection: 'row',
    borderBottomColor: Colors.EXTRACOLORS.BORDER,
    borderBottomWidth: 0.5,
    alignItems: 'center',
  },
  avatarParent: {
    height: Util.getHeight(6),
    width: Util.getHeight(6),
    borderRadius: Util.getHeight(3),
    overflow: 'hidden',
    marginHorizontal: 10,
  },
  userAvatar: {
    height: Util.getHeight(6),
    width: Util.getHeight(6),
    borderRadius: Util.getHeight(3),
    overflow: 'hidden',
  },
  borderView: {
    width: '100%',
    height: 0.5,
    backgroundColor: Colors.EXTRACOLORS.DEARKBORDER,
  },

  listActivateUser: {
    borderColor: Colors.MODELNUMBERBTNCOLOR,
    borderWidth: 1,
    borderRadius: 10,
    marginVertical: 10,
    paddingVertical: 10,
  },

  userProfile: {
    borderColor: Colors.EXTRACOLORS.APPCOLOR,
    borderWidth: 1,
    borderRadius: 5,
    marginVertical: 10,
    paddingVertical: 10,
  },

  //ToolView List Item Style
  transferListItem: {
    height: Util.getHeight(8),
    flexDirection: 'row',
    // justifyContent: 'space-between',
    borderBottomColor: Colors.EXTRACOLORS.BORDER,
    borderBottomWidth: 0.5,
    width: '100%',
  },
  lableView: {
    width: Util.getWidth(25),
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  lableViewTransfer: {
    width: Util.getWidth(15),
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  lableViewCheckBox: {
    width: Util.getWidth(35),
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  lableViewRight: {
    // backgroundColor:"red",
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  lableViewRemove: {
    width: '25%',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  lableViewRemoveTwo: {
    width: '25%',
    justifyContent: 'center',
    alignItems: 'center',
  },

  lableViewDate: {
    width: '50%',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  lableViewDateTwo: {
    width: '50%',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  lableViewThree: {
    width: '20%',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  lableViewThree2: {
    width: '30%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  lableViewTwo: {
    width: '30%',
    marginHorizontal: '1%',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  itemText: {
    fontFamily: Util.getFontFamily('regular'),
    fontSize: Util.normalize(12),
    marginLeft: 5,
  },
  itemText2: {
    fontFamily: Util.getFontFamily('regular'),
    fontSize: Util.normalize(12),
    color: Colors.TextColor.ErrorPrimary,
    marginLeft: 5,
  },
  itemTextTwo: {
    fontFamily: Util.getFontFamily('regular'),
    fontSize: Util.normalize(10),
    //marginLeft: 5,
  },
  itemTextThree: {
    fontFamily: Util.getFontFamily('regular'),
    fontSize: Util.normalize(12),
  },
  itemTextReturn: {
    fontFamily: Util.getFontFamily('regular'),
    fontSize: Util.normalize(10),
    marginLeft: '-5%',
    alignSelf: 'flex-start',
    alignContent: 'flex-start',
  },
  flexDirRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
  flexDirRowTwo: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: '48%',
    alignItems: 'center',
  },
  flexDirRowListItem: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: '75%',
    alignItems: 'center',
  },
  statusStyle: {
    height: 10,
    width: 10,
    borderRadius: 5,
    alignSelf: 'center',
  },
  statusStyleTwo: {
    height: 8,
    width: 8,
    borderRadius: 4,
    marginRight: '5%',
    alignSelf: 'center',
  },
  moreIconTool: {
    height: 15,
    width: 15,
    alignSelf: 'center',
    tintColor: Colors.EXTRACOLORS.MUTED,
  },
  listHeader: {
    height: 55,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  hedertext: {
    fontSize: Util.normalize(13),
    fontWeight: 'bold',
    fontFamily: Util.getFontFamily('regular'),
  },
  valueText: {
    fontSize: Util.normalize(12),
    fontFamily: Util.getFontFamily('regular'),
  },
  docParent: {
    height: Util.getHeight(7.5),
    width: Util.getWidth(85),
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    borderStyle: 'dashed',
    borderWidth: 1.5,
    borderRadius: 1,
    borderColor: Colors.EXTRACOLORS.APPCOLOR,
  },
  underLineText: {
    color: Colors.APPTHEME,
    fontSize: Util.normalize(13),
    marginHorizontal: 5,
    fontFamily: Util.getFontFamily('bold'),
    borderBottomWidth: 1,
    borderColor: Colors.APPTHEME,
  },
  normaltext: {
    // color: Colors.APPTHEME,
    fontSize: Util.normalize(13),
  },
  searchParent: {
    flexDirection: 'row',
  },
  errorMsg: {
    paddingVertical: 5,
    fontFamily: Util.getFontFamily('regular'),
    color: Colors.THEME.ERROR,
    fontSize: 16 * 0.8,
    fontStyle: 'italic',
  },

  categoryCardView: {
    backgroundColor: Colors.EXTRACOLORS.CARDCOLOR,
    marginHorizontal: '2%',
    marginBottom: 15,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: Colors.EXTRACOLORS.DEARKBORDER,
    height: Platform.OS === 'android' ? Util.getHeight(26) : Util.getHeight(18),
    shadowColor: Colors.EXTRACOLORS.DEARKBORDER,
    padding: 10,
    width: '46%',
  },
  imageInCard: {
    flex: 0.7,
    backgroundColor: Colors.MODELNUMBERBTNCOLOR,
    borderRadius: 10,
    // justifyContent: 'center',
    alignItems: 'center',
    // padding: 20,
    paddingBottom: Util.getHeight(1),
  },
  imageCatgory: {
    flex: 1,
    height: Util.getHeight(11),
    width: Util.getHeight(11),
  },
  itemDetail: {
    flex: 0.3,
    paddingTop: 10,
  },
  productName: {
    fontFamily: Util.getFontFamily('bold'),
    color: Colors.THEME.TEXTCOLOR,
    fontSize: Util.normalize(13),
    textAlign: 'center',
    width: '90%',
  },
  moreParent: {
    right: 0,
    position: 'absolute',
    top: 15,
  },
  moreIcon: {
    height: 15,
    width: 15,
    tintColor: Colors.EXTRACOLORS.MUTED,
  },
  checkBoxStyle: {
    borderRadius: Util.getWidth(5),
    borderWidth: 1.2,
    height: Util.getWidth(5),
    width: Util.getWidth(5),
    right: 5,
    //top: 5,
  },
  checkboxParent: {
    width: '100%',
    alignItems: 'flex-start',
  },

  statusItem: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: Util.getHeight(6),
    paddingHorizontal: Util.getWidth(2),
    borderBottomColor: Colors.EXTRACOLORS.BORDER,
    borderBottomWidth: 0.5,
    marginVertical: 3,
  },
  flexDirRow1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
  statusStyleView: {
    height: Util.getHeight(1.5),
    width: Util.getHeight(1.5),
    borderRadius: Util.getHeight(0.75),
    marginRight: Util.getWidth(2),
    alignSelf: 'center',
  },
  itemTextLable: {
    fontFamily: Util.getFontFamily('regular'),
    fontSize: Util.normalize(12),
  },
  checkBoxToolStatus: {
    borderRadius: Util.getWidth(5),
    borderWidth: 1.2,
    height: Util.getWidth(5),
    width: Util.getWidth(5),
  },
  noDataParent: {
    height: Util.getHeight(10),
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  noDataText: {
    fontSize: Util.normalize(13),
    color: Colors.AppTheme.ColorPrimary,
    fontFamily: Util.getFontFamily('bold'),
  },
  listHeaderReport: {
    height: 55,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    // alignSelf: 'flex-start',
  },
  listItemReport: {
    height: 55,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    borderBottomColor: Colors.EXTRACOLORS.BORDER,
    borderBottomWidth: 0.5,
    // alignSelf: 'flex-start',
  },
  lableViewReportBookings: {
    minWidth: Util.getWidth(25),
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginHorizontal: Util.getWidth(3),
  },
});
export default styles;
