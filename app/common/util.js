import {Dimensions, PixelRatio, Platform} from 'react-native';

const {width: SCREEN_WIDTH} = Dimensions.get('window');

// based on iphone 5s's scale
const scale = SCREEN_WIDTH / 320;

const Util = {
  normalize: size => {
    const newSize = size * scale;
    if (Platform.OS === 'ios') {
      return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2;
    } else {
      return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2;
    }
  },
  getHeight: percente => {
    percente = !percente ? 100 : percente;
    return (Util.getWindowSize().height * percente) / 100;
  },
  getWidth: percente => {
    percente = !percente ? 100 : percente;
    return (Util.getWindowSize().width * percente) / 100;
  },
  getHeight1: heightPercent => {
    // percente = !percente ? 100 : percente;
    // return (Util.getWindowSize().height * percente) / 100;
    const screenHeight = Dimensions.get('window').height;
    const elemHeight = parseFloat(heightPercent);
    return PixelRatio.getPixelSizeForLayoutSize(
      (screenHeight * elemHeight) / 100,
    );
  },
  getWindowSize: () => Dimensions.get('window'),
  getFontFamily: type => {
    switch (type) {
      case 'bold':
        return 'OpenSans-Bold';
      case 'regular':
        return 'OpenSans-Regular';
      default:
        return 'OpenSans-Regular';
    }
  },
};

// module.exports = Util;
export default Util;
