import React, { useContext } from 'react';
import {
    View,
    TouchableOpacity,
    ScrollView,
    ImageBackground,
    TextInput,
    Image,
} from 'react-native';
import { SafeAreaView } from 'react-navigation';
import {
    Input,
    Icon,
    Text,
    Button,
    Checkbox,
} from 'galio-framework';

import Styles from './styles';
import Colors from '../../values/colors';


export const SocialLoginComponent = (props) => {
    const { iconOne, iconOneClick, iconTwoClick, iconThreeClick, iconTwo, iconThree } = props;
    return (
        <View style={Styles.viewRow}>
            <TouchableOpacity onPress={iconOneClick} style={Styles.viewRound}>
                <Image
                    style={
                        Styles.iconStylefb
                    }
                    source={iconOne}
                />

            </TouchableOpacity>
            <TouchableOpacity onPress={iconTwoClick} style={Styles.viewRound}>
                <Image
                    style={
                        Styles.iconStyle
                    }
                    source={iconTwo}
                />
            </TouchableOpacity>
            <TouchableOpacity onPress={iconThreeClick} style={Styles.viewRound}>
                <Image
                    style={
                        Styles.iconStyle
                    }
                    source={iconThree}
                />

            </TouchableOpacity>

        </View>

    )
};

