import React, { useContext } from 'react';
import {
    View,
    TouchableOpacity,
    ScrollView,
    ImageBackground,
    TextInput,
    Image,
} from 'react-native';
import { SafeAreaView } from 'react-navigation';
import {
    Input,
    Icon,
    Text,
    Button,
    Checkbox,
} from 'galio-framework';

import Styles from './styles';
import Colors from '../../values/colors';
import styles from './styles';
import images from '../../assets/Images/imagePath/images';


export const ProfileCardComponent = (props) => {
    const { buttonText,userName,userId,imageQr,lastUpdatedDate, image, onClick } = props;
    return (
        <TouchableOpacity onPress={onClick}>
            <View style={Styles.cardView}>
                <View style={{ flexDirection: 'row', height: '100%', justifyContent: 'space-between' }}>
                    <View style={styles.viewLeft}>
                        <Image
                            source={image}
                            style={styles.profilePicImage}
                        />
                        <Text style={styles.textName}>
                            {userName}
                        </Text>
                        <Text style={styles.textId}>
                            {'ID No:  '}<Text style={styles.textIdValue}>{userId}</Text>
                        </Text>

                        <Text style={styles.textId}>
                            {'Last Updated: '}<Text style={styles.textId}>{lastUpdatedDate}</Text>
                        </Text>
                    </View>
                    <View style={styles.viewRight}>
                        <Image
                            style={
                                styles.qrImage
                            }
                            source={imageQr}
                        />
                    </View>

                </View>

            </View>
        </TouchableOpacity>
    )
};

