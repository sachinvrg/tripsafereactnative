import { StyleSheet } from 'react-native';
import Util from '../../common/util';
import Colors from '../../values/colors';

const styles = StyleSheet.create({
  cardView: {
    marginTop: 10,
    borderRadius: 10,
    width: Util.getWidth(84),
    height: Util.getHeight(15),
    backgroundColor: Colors.EXTRACOLORS.WHITE,
    paddingHorizontal: 50,
    paddingVertical: 12,
    shadowColor: '#000000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 1,
    // borderLeftColor:Colors.APPTHEME,
    // borderLeftWidth:Util.getWidth(1.2),
    shadowRadius: 5,
    alignItems: 'center',
    // justifyContent: 'center',
    elevation: 4,
    // ali:'flex-end',
    backgroundColor: Colors.EXTRACOLORS.WHITE
  },
  textButton: {
    color: Colors.EXTRACOLORS.WHITE,
    fontSize: Util.normalize(13)
  },
  viewLeft: {
    width: Util.getWidth(28.9),
    
    flexDirection:'row',
    alignSelf:'flex-start',
    alignContent:'flex-start',
    alignItems:'center',
    // backgroundColor:'red'
  },
  viewRight: {
    width: Util.getWidth(55),
    marginLeft: Util.getWidth(1)
  },
  qrImage: {
    flex: 1,
    width: Util.getWidth(25),
    height: Util.getHeight(12),
    marginLeft:'-15%',
    alignItems:'flex-end',
    resizeMode: 'contain'
  },
  profilePicImage: {
    height: Util.getHeight(8),
    width: Util.getHeight(8),
    borderRadius: Util.getHeight(10),
    marginLeft: '8%',

  },
  textName: {
    color: Colors.NavyBlue,
    fontSize: Util.normalize(16),
    fontWeight: 'bold',
  },
  textId: {
    color: Colors.TextColor.SecondaryTextColor,
    fontSize: Util.normalize(12),

  },
  textIdValue: {
    color: Colors.NavyBlue,
    fontSize: Util.normalize(12),
    fontWeight: 'bold',
  },
  testReport: {
    color: Colors.THEME.SUCCESS,
    fontSize: Util.normalize(11),
    marginTop:Util.getHeight(2)
  }
});
export default styles;
