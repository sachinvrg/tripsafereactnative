import { StyleSheet } from 'react-native';
import Util from '../../common/util';
import Colors from '../../values/colors';

const styles = StyleSheet.create({
  button: {
    borderRadius: 50,
    backgroundColor: Colors.THEME.SUCCESS,
    paddingHorizontal: 50,
    width:Util.getWidth(70),
    alignItems:'center',
    paddingVertical: 12
  },
  textButton: {
    color: Colors.EXTRACOLORS.WHITE,
    fontSize: Util.normalize(13)
  }
});
export default styles;
