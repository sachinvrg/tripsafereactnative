// Import react component

import React, { Component } from "react";
import { Platform, StyleSheet, StatusBar, Text, View } from "react-native";
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

// Import Screen

import SplashScreen from './app/views/splashScreen';
import SliderIndex from './app/views/IntroSlider/Index';
import LoginScreen from './app/views/loginScreen/index'
import SignUpScreen from './app/views/signUpScreen/index'
import HomeScreen from './app/views/homeScreen/bottomTabs'
// import QrScanner from './app/components/QrComponent/QrScanner';



// Import Colors and style

import Colors from './app/values/colors';
import PersonalDetailsScreen from "./app/views/personalDetails";
// import QrScreen from "./app/views/QRScanner/QRCodeScanner";

const TripSafe = createStackNavigator({
  Splash: {
    screen: SplashScreen,
    navigationOptions: () => ({
      header: null,
    }),
  },
  IntroSlider:{
    screen: SliderIndex,
    navigationOptions: () => ({
      header: null,
    }),
  },
  LoginScreen:{
    screen: LoginScreen,
    navigationOptions: () => ({
      header: null,
    }),
  },
  PersonalDetails:{
    screen: PersonalDetailsScreen,
    navigationOptions: () => ({
      header: null,
    }),
  },
  SignUpScreen:{
    screen: SignUpScreen,
    navigationOptions: () => ({
      header: null,
    }),
  },
  Home: {
    screen: HomeScreen,
    navigationOptions: () => ({
      header: null,
      title: 'Home',
    }),
  },
  // QRScreen: {
  //   screen: QrScreen,
  //   navigationOptions: () => ({
  //     header: null,
  //   }),
  // },

  
},
  {
    initialRouteName: 'Splash',
    defaultNavigationOptions: {
      headerTitleStyle: { color: Colors.whiteColor },
      // headerStyle: {
      //   backgroundColor: Colors.appColor,
      // },
      // headerTintColor: Colors.whiteColor,
    },
  },
);

const App = createAppContainer(TripSafe);

export default App;

