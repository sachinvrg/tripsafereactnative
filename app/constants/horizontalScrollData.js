import images from "../assets/Images/imagePath/images";

export const HorizontalScrollOption = [
    {
        name: 'Authorisation & Access',
        images: images.settings,
        index: 0,
    },
    {
        name: 'Quick Check-in',
        images: images.sendLocation,
        index: 1,
    },
    {
        name: 'Travel Docs & Visas',
        images: images.notePad,
        index: 2,
    },
    {
        name: 'Find Doctor',
        images: images.firstAdd,
        index: 3,
    },

]