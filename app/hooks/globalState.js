import React, { useReducer } from 'react';
import AppContext from './app-context';
import {
  USERDETAILS,
  ISLOADING,
  LOGOUT,
  SCREENINDEX,
  USER_TYPE,
  ROUTES_KEY,
} from './constants';

export const GlobalState = props => {

  const initialState = {
    UserDetails: {},

    isLoading: false,

    clearState: false,
    screenIndex: {
      index: 0,
      screen: 'Dashboard',
    },
  };
  /**
   * this is the reducer function which will handle the actions
   * @param {*} state
   * @param {*} action
   */
  function reducer(state, action) {
    switch (action.type) {
      case USERDETAILS:
        return { ...state, UserDetails: action.payload };
      case ROUTES_KEY:
        return { ...state, routesKey: action.payload };
      case SCREENINDEX:
        return { ...state, screenIndex: action.payload };
      case LOGOUT:
        return { ...state, isLoading: false, data: {}, cartList: [] };
     

      default:
        return state;
    }
  }

  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <AppContext.Provider
      value={{
        state: state,
        dispatch: dispatch,
      }}>
      {props.children}
    </AppContext.Provider>
  );
};
