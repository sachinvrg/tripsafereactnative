
import React, { useContext, useEffect, useState } from 'react';
import { View, StatusBar, ScrollView, Dimensions, StyleSheet, Animated, FlatList, Image, ImageBackground } from 'react-native';
import { Text } from 'galio-framework';
import moment from 'moment';
// Hooks, Api component calling
import AppContext from '../../hooks/app-context';
import { useHttp } from '../../hooks/useHttpss';
import {
  GET,
  USERDETAILS
} from '../../hooks/constants';

// Global helper classes
// import HeaderComponent, {
//   AdminHeaderComponent,
// } from '../../components/DrawerHeader';

import strings from '../../values/stringEn';
import LinearGradient from 'react-native-linear-gradient'
import Spinner from 'react-native-loading-spinner-overlay';

// Local styles
import styles from './styles';
import Colors from '../../values/colors';
import Util from '../../common/util';
import helper from '../../common/helper';
import { TouchableOpacity } from 'react-native-gesture-handler';
import images from '../../assets/Images/imagePath/images';
import HeaderComponent from '../../components/HeaderComponent';
import { ProfileCardComponent } from '../../components/ProfileCardView';
import { GetUserDetails } from '../../network/Networksettings';

import { HorizontalScrollOption } from '../../constants/horizontalScrollData';
import Scroller from '../loginScreen/scroller';
import { RecentActivityComponent } from '../../components/ActivityComponent';
import FloatingButtonModal from '../../components/floatingModel';
import ProfileViewModal from '../../components/profileModel';


const HomeScreen = props => {
  const { navigation } = props;
  const context = useContext(AppContext);
  const { state } = context;
  const { UserDetails } = state;
  const [animation, setAnimation] = useState(new Animated.Value(0));
  const screenHeight = Dimensions.get("window").height;
  const [userDetails, setUserDetails] = useState({})
  const [floatingModal, setFloatingModal] = useState(false);
  const [profileModal, setProfileModal] = useState(false);
  const [loader, setLoader] = useState(false);


  const authUserData = helper.fetch_item_async(USERDETAILS);
  // const parsedUserDetails = JSON.parse(authUserData)


  // console.log('userDetails', parsedUserDetails);
  console.log('authUserData>>>>>>>>t', state);


  useEffect(() => {
    getUserDetails()


    authUserData
      .then(data => {
        const parsedUserDetails = JSON.parse(data)

        setUserDetails(parsedUserDetails)
        // console.log('userDetails>>>>>', JSON.parse(data));


      })
      .catch(() => {
      });




  }, []);


  const getUserDetails = () => {
    setLoader(true);
    useHttp(GetUserDetails, GET, state.UserDetails.authorization)
      .then(res => {
        console.log('userdata on home screen', res.data.data);

        if (res.data && res.data.data) {
          setUserDetails(res.data.data);
          let userDataObject = res.data.data;
          let tempUser = { ...state.UserDetails, ...userDataObject };
          context.dispatch({
            type: USERDETAILS,
            payload: tempUser,
          });
          helper.store_item_async(USERDETAILS, JSON.stringify(tempUser));
          userData.map(item => {
            if (userDataObject.hasOwnProperty(item.key)) {
              item.value = userDataObject[item.key];
            }
          });
        }
        setTimeout(() => {
          setLoader(false);
        }, 500);
      })
      .catch(_err => {
        setTimeout(() => {
          setLoader(false);
        }, 500);
      });
  };
  const onQrPress = () => {
    navigate('QRScreen', {
      onGoBack: data => onSelect(data),
    });
  };

  const onSelect = item => {
    console.log("Qr scaned", item);

  };

  const logOut = () => {

    //  alert('hello')
    helper.remove_item_async(USERDETAILS);
    //navigation.goBack();
    navigation.navigate('LoginScreen');
  }
  return (

    <LinearGradient
      colors={['#113CB4', '#091850']}

      style={styles.linearGradient}
    >
      <StatusBar barStyle='light-content' hidden={false} backgroundColor="#113CB4" />
      {<Spinner visible={loader} />}

      <HeaderComponent
        title="Welcome"
        notification={true}
      // onClick={() => logOut()}
      />

      <View style={{ flex: 1, width: '100%' }}>
        {userDetails && <View style={{ alignItems: 'center' }}>
          <ProfileCardComponent
            image={UserDetails.avatar ? { uri: `${UserDetails.avatar}` } : images.profileImage}
            userName={`${userDetails.first_name} ${userDetails.last_name}`}
            userId={`${userDetails.id}`}
            imageQr={{ uri: `${userDetails.qr}` }
            }
            onClick={() => setProfileModal(true)}
            lastUpdatedDate={moment(userDetails.updated_at).format('LL')} />

        </View>}


        <View style={styles.bottomContainer}>
          <View style={styles.horizontalScroll}>

            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
              {(
                HorizontalScrollOption
              ).map(item => {
                return (
                  <TouchableOpacity style={item.index === 0 ? styles.tabButton2 : styles.tabButton}>
                    <View style={item.index === 0 ? styles.iconBg1 : styles.iconBg3 ? (item.index === 1 ? styles.iconBg2 : styles.iconBg3) : styles.iconBg3}>
                      <Image
                        style={
                          styles.iconStyle
                        }
                        tintColor={item.index === 0 ? '' : 'white'}
                        source={item.images}
                      />
                    </View>
                    <Text style={item.index === 0 ? styles.fontStyle2 : styles.fontStyle}>{item.name}</Text>
                  </TouchableOpacity>
                );
              })}

            </ScrollView>

          </View>
          <Text style={styles.recentActivity}>{"Recent Activity"}</Text>

          <View style={{ alignItems: 'center' }}>
            <RecentActivityComponent />
          </View>

          <View style={styles.buttonBottom} >
            <TouchableOpacity onPress={() => setFloatingModal(true)} >
              <Image
                source={images.plusMore}
                resizeMode='contain' />
            </TouchableOpacity>
          </View>


        </View>
      </View>

      <View style={styles.heightWidthZero}>
        {floatingModal && (
          <FloatingButtonModal
            // qrScan={() => onQrPress()}
            ismodel={floatingModal}
            onShow={() => setFloatingModal(false)}

          />
        )}
      </View>
      <View style={styles.heightWidthZero}>
        {profileModal && (
          <ProfileViewModal
            ismodel={profileModal}
            onShow={() => setProfileModal(false)}
            image={UserDetails.avatar ? { uri: `${UserDetails.avatar}` } : images.profileImage}
            userName={`${UserDetails.first_name} ${UserDetails.last_name}`}
            userId={`${UserDetails.id}`}
            qrImage={{ uri: `${userDetails.qr}` }}
            // onClick={() => setProfileModal(true)}
            lastUpdatedDate={moment(UserDetails.updated_at).format('LL')}

          />
        )}
      </View>



    </LinearGradient>



  );
};

export default HomeScreen;
