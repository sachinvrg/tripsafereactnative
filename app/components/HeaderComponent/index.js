import React, { useContext, useEffect } from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import {
  NavBar,
  Icon,
  Text,
  Button,
  Checkbox,
} from 'galio-framework';

// Hooks, Api component calling
import AppContext from '../../hooks/app-context';

// Global helper classes

// Local styles
import styles from './styles';
// import images from '../../assets/images';
import Colors from '../../values/colors';
import Util from '../../common/util';
import { StackActions, NavigationActions } from 'react-navigation';
import { CartList } from '../../network/Networksettings';
import { GET, SCREENINDEX, CARTLIST } from '../../hooks/constants';
import { useHttp } from '../../hooks/useHttpss';
import { State } from 'react-native-gesture-handler';
import strings from '../../values/stringEn';
import images from '../../assets/Images/imagePath/images';



export const NotificationIcon = props => {
  const { onClick } = props;
  return (
    <TouchableOpacity onPress={onClick} style={styles.more}>
      <View>
        <Image source={images.bell} style={[styles.moreIcon]} />
      </View>
    </TouchableOpacity>
  );
};


export const Back = props => {
  const { navigateBack } = props;

  return (
    <TouchableOpacity onPress={navigateBack} style={styles.iconBackStyle}>
    <Image source={images.backIcon}/>
    </TouchableOpacity>

  );
};



const HeaderComponent = props => {
  const {
    navigation,
    back,
    title,
    notification,
    onClick,
    iconLeft,
    titleStyle,
    navigateBack,
    onMoreButtonNonListenerClick,

  } = props;
  const context = useContext(AppContext);
  const { state } = context;


  useEffect(() => {


  }, []);


  return (
    <NavBar
      title={title ? title : ''}
      titleStyle={[styles.titleStyle, titleStyle]}
      left={iconLeft && (
        <Back navigateBack={navigateBack} />
      )}

      right={
        <View>
          {notification && (
            <NotificationIcon onClick={onClick} />
          )}
        </View>
      }
    />
  );
};

export default HeaderComponent;
