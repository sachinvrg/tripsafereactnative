

'use strict';

import React, { useEffect, useContext, useState } from 'react';
import {
    Text,
    TextInput,
    View,
    TouchableOpacity, ScrollView,
    Image, ImageBackground, StyleSheet, Alert
} from 'react-native';
import { Checkbox } from 'galio-framework'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import AppContext from '../../hooks/app-context';
import {
    POST, CREDENTIALS
} from '../../hooks/constants';
import { useHttp } from '../../hooks/useHttpss';
import helper from '../../common/helper';
import DeviceInfo from 'react-native-device-info';

import { getUniqueId, getManufacturer } from 'react-native-device-info';
import { signUp } from '../../network/Networksettings';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-simple-toast';


import COLOR from '../../values/colors';
import strings from '../../values/stringEn';
import styles from './styles';
import Images from '../../assets/Images/imagePath/images';

// import { ScrollView } from 'react-native-gesture-handler';
import { TextInputComponent } from '../../components/TextInput';
import { ButtonComponent } from '../../components/Button';
import { SocialLoginComponent } from '../../components/SocialLoginButton';
import images from '../../assets/Images/imagePath/images';
import { CountryPickerComponent } from '../../components/CountryPicker';
var mailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const SignUpScreen = props => {
    const { navigation } = props;
    const context = useContext(AppContext);
    const { state } = context;
    const [loader, setLoader] = useState(false);
    const [isFocusedFirstName, setIsFocusedFirstName] = useState(false);
    const [isFocusedLastName, setIsFocusedLastName] = useState(false);
    const [deviceId, setDeviceId] = useState('');
    const [isFocusedNumber, setIsFocusedNumber] = useState(false);
    const [emailCorrect, setEmailCorrect] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');

    const [isFocusedMail, setIsFoucusedMail] = useState(false);
    const [isFocusedPassword, setIsFocusedPassword] = useState(false);

    const [formData, setFormData] = useState({

        first_name: {
            err: false,
            errMsg: '',
            propname: 'first_name',
            value: '',
            required: true,
        },
        last_name: {
            err: false,
            errMsg: '',
            propname: 'last_name',
            value: '',
            required: true,
        },
        phone: {
            err: false,
            errMsg: '',
            propname: 'phone',
            value: '',
            required: true,
        },
        email: {
            err: false,
            errMsg: '',
            propname: 'email',
            value: '',
            required: true,
            checkMobile: true,
        },
        password: {
            err: false,
            errMsg: '',
            propname: 'password',
            value: '',
            required: true,
        },

    });

    const changeText = (name, value) => {
        if (name === 'email') {

            if (value.match(mailformat)) {
                setEmailCorrect(true);
            } else {
                setEmailCorrect(false);
            }
        }
        setFormData({
            ...formData,
            [name]: {
                ...formData[name],
                err: false,
                errMsg: '',
                value,
                propname: name,
            },
        });
    };

    useEffect(() => {
        getDeviceId()

    }, []);
    const getDeviceId = () => {

        var id = DeviceInfo.getUniqueId();
        console.log('device_id', id);

        setDeviceId(id);


    }

    const handleFocusPassword = () => {
        setIsFocusedPassword(true);
    }
    const handleFocusFirstName = () => {
        setIsFocusedFirstName(true);
    }


    const handleBlurFirstName = () => {
        setIsFocusedFirstName(false);
    }


    const handleFocusLastName = () => {
        setIsFocusedLastName(true);
    }


    const handleBlurLastName = () => {
        setIsFocusedLastName(false);
    }

    const handleFocusNumber = () => {
        setIsFocusedNumber(true);
    }


    const handleBlurNumber = () => {
        setIsFocusedNumber(false);
    }


    const handleBlurPassword = () => {
        setIsFocusedPassword(false);
    }


    const handleFocusMail = () => {
        setIsFoucusedMail(true);
    }

    const handleBlurMail = () => {
        setIsFoucusedMail(false);
    }


    const onSignUpClick = () => {
        //  setEmailCorrect(true);

        setLoader(true);
        helper.validateForm(formData).then(({ error, formDataObj }) => {
            if (!error) {

                const signUpRequest = helper.processData(formData);

                signUpApi(signUpRequest);
            } else {
                setFormData(formDataObj);
                setTimeout(() => {
                    setLoader(false);
                }, 100);
            }
        });
    };

    const signUpApi = async body => {
        body.email = body.email.toLowerCase();
        body.device_name = deviceId
        setLoader(true);
        await useHttp(signUp, POST, null, body)
            .then(res => {
                setLoader(false);
                console.log("response", res);

                // navigation.replace('Home')
                navigation.goBack()


                Toast.show(
                    'Account Created successfully',
                    Toast.LONG,
                );


            })
            .catch(err => {
                setLoader(false);

                if (err.errors.email) {
                    setFormData({
                        ...formData,
                        ['email']: {
                            ...formData['email'],
                            err: true,
                            errMsg: err.errors.email[0],
                            value: formData.email.value,
                            propname: 'email',
                        },


                    });



                } else if (err.errors.phone) {
                    setFormData({
                        ...formData,
                        ['phone']: {
                            ...formData['phone'],
                            err: true,
                            errMsg: err.errors.phone[0],
                            value: formData.phone.value,
                            propname: 'phone',
                        },


                    });



                }

                console.log("formData in catch block", formData);


            });
    };

    return (

        <View style={styles.containerScroll} >
            <Spinner visible={loader} />

            <ImageBackground source={Images.Bg} style={styles.container}>
                <KeyboardAwareScrollView
                    extraScrollHeight={10}
                    showsVerticalScrollIndicator={false}
                    enableOnAndroid={true}
                    scrollEnabled
                    keyboardShouldPersistTaps={'always'}>

                    <ScrollView style={{ flex: 1 }}>
                        <View style={styles.loginBox}>
                            <View style={styles.box3} />
                            <View style={styles.box2} />
                            <View style={styles.box1}>
                                <Text style={styles.createAccount}>{strings.createAccount}</Text>
                                <TextInputComponent
                                    label
                                    iconLeftShow={false}
                                    labelText={strings.firstName}
                                    ActiveTint={isFocusedFirstName ? true : false}
                                    onFocus={handleFocusFirstName}
                                    onBlur={handleBlurFirstName}
                                    iconLeft={Images.email}
                                    placeholder={strings.enterFirstName}
                                    onChangeText={text => changeText('first_name', text)}
                                    help={formData.first_name.errMsg}
                                    value={formData.first_name.value}

                                />

                                <TextInputComponent
                                    iconLeftShow={false}
                                    label
                                    labelText={strings.lastName}
                                    ActiveTint={isFocusedLastName ? true : false}
                                    onFocus={handleFocusLastName}
                                    onBlur={handleBlurLastName}
                                    iconLeft={Images.lock}
                                    placeholder={strings.enterLastName}
                                    onChangeText={text => changeText('last_name', text)}
                                    help={formData.last_name.errMsg}
                                    value={formData.last_name.value}
                                />

                                <CountryPickerComponent
                                    iconLeftShow={true}
                                    label
                                    labelText={strings.contactNumber}
                                    ActiveTint={isFocusedNumber ? true : false}
                                    onFocus={handleFocusNumber}
                                    onBlur={handleBlurNumber}
                                    placeholder={strings.enterMobileNumber}
                                    onChangeText={text => changeText('phone', text)}
                                    help={formData.phone.errMsg}
                                    value={formData.phone.value}
                                />
                                <TextInputComponent
                                    iconLeftShow={false}
                                    label
                                    labelText={strings.email}
                                    ActiveTint={isFocusedMail ? true : false}
                                    onFocus={handleFocusMail}
                                    onBlur={handleBlurMail}
                                    placeholder={strings.enterEmail}
                                    onChangeText={text => changeText('email', text)}
                                    help={formData.email.errMsg}
                                    iconRight={Images.checked}
                                    iconRightShow={emailCorrect ? true : false}
                                    value={formData.email.value}

                                />
                                <TextInputComponent
                                    iconLeftShow={false}
                                    label
                                    labelText={strings.password}
                                    ActiveTint={isFocusedPassword ? true : false}
                                    onFocus={handleFocusPassword}
                                    onBlur={handleBlurPassword}
                                    onChangeText={text => changeText('password', text)}
                                    help={formData.password.errMsg}
                                    value={formData.password.value}
                                    secureTextEntry={true}
                                    placeholder={strings.enterPassword}
                                />
                                <View style={styles.radioForgetView}>

                                    <TouchableOpacity>
                                        <Text style={styles.byClicking}>{strings.byClicking} <Text style={styles.forgetPsw}>
                                            {strings.turmPrivacy}
                                        </Text></Text>

                                    </TouchableOpacity>
                                </View>

                                <View style={{ marginTop: '6%' }}>
                                    <ButtonComponent onClick={() => onSignUpClick()}
                                        buttonText={strings.createAccount} />
                                </View>
                            </View>




                        </View>
                        <SocialLoginComponent
                            iconOne={Images.iconFb}
                            iconTwo={Images.iconGoogle}
                            iconThree={images.iconApple} />
                        <TouchableOpacity onPress={() => navigation.goBack()} style={{ marginTop: '5%', paddingBottom: '5%', alignSelf: 'center' }}>
                            <Text style={{ color: 'white', fontSize: 12 }}>
                                {'Already have an account?'}<Text style={{
                                    fontWeight: 'bold',
                                    textDecorationLine: 'underline'
                                }}>{'Sign in here'}</Text>

                            </Text>

                        </TouchableOpacity>
                    </ScrollView>
                </KeyboardAwareScrollView>

            </ImageBackground>
        </View>
    )

}
export default SignUpScreen;
