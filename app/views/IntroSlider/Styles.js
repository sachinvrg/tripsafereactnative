
'use strict';

import { StyleSheet } from 'react-native';
import COLOR from '../../values/colors';
export default StyleSheet.create({
    fontBold: {
        fontFamily: 'Bold',
        includeFontPadding: false
    },
    fontLight: {
        fontFamily: 'Light',
        includeFontPadding: false
    },
    fontMedium: {
        fontFamily: 'Medium',
        includeFontPadding: false
    },
    fontRegular: {
        fontFamily: 'Regular',
        includeFontPadding: false
    },
    fontSemiBold: {
        fontFamily: 'SemiBold',
        includeFontPadding: false
    },
    fontExtraBold: {
        fontFamily: 'Poppins-ExtraBold',
        includeFontPadding: false
    },

       linearGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 5
    },

    safearea: {
        flex: 1,
        backgroundColor: COLOR.WHITE,
    },
    formGroup: {
        paddingBottom: 20,
        width: '100%'
    },
    formInlineGroup: {
        paddingBottom: 20,
        display: 'flex',
        flexDirection: 'row'
    },
    centerText: {
        display: 'flex',
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center'
    },
    tbRow: {
        flexDirection: 'row',
        paddingBottom: 5
    },
    tbCol: { justifyContent: 'center', flexDirection: 'column' },

    textWhite: {
        color: COLOR.WHITE
    },
    textPrimary: {
        color: COLOR.PRIMARY
    },
    textPrimaryDark: {
        color: COLOR.PRIMARY_DARK
    },
    textPrimaryLight: {
        color: COLOR.PRIMARY_LIGHT
    },
    textGrayLight: {
        color: COLOR.GRAY_LIGHT
    },
    textGray: {
        color: COLOR.GRAY
    },
    textGrayDark: {
        color: COLOR.GRAY_DARK
    },

    aligncenter: {
        flexDirection: 'column',
        justifyContent: 'center',
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
    },
    modalBackground: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        padding: 20,
    },
    innerContainer: {
        borderRadius: 10,
    },
    innerContainerTransparent: {
        backgroundColor: COLOR.WHITE,
        padding: 20,
    },
    appheader: {
        resizeMode: 'contain',
        height: 60,
        alignSelf: 'center',
    },
    loginform: {
        paddingHorizontal: 20,
        alignItems: 'stretch',
        paddingLeft: 25,
        paddingRight: 25,
    },
    loginbutton: {
        color: COLOR.BUTTON,
        fontSize: 16,
        alignSelf: 'center',
        paddingTop: 20,
        textAlign: 'center',
    },
    forminput: {
        padding: 5,
        marginBottom: 10,
        color: COLOR.ACCENT,
        height: 40,
        borderColor: COLOR.ACCENT,
        borderWidth: 1,
        borderRadius: 4,
    },
    useragent: {
        flex: 1,
        flexDirection: 'column',
    },
    selfview: {
        position: 'absolute',
        right: 20,
        bottom: 20,
        width: 100,
        height: 120,
    },
    remotevideo: {
        flex: 1,
    },
    videoPanel: {
        flex: 1,
        position: 'relative',
    },
    call_controls: {
        height: 70,
    },
    margin: {
        margin: 10,
    },
    call_connecting_label: {
        fontSize: 18,
        alignSelf: 'center',
    },
    headerButton: {
        color: COLOR.WHITE,
        fontSize: 16,
        alignSelf: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 5,
        textAlign: 'center',
    },
    incoming_call: {
        justifyContent: 'center',
        alignSelf: 'center',
        fontSize: 22,
    },
    slide: {
        flex: 1, // Take up all screen
        height:'100%',
        alignItems: "center", // Center horizontally
       
      },
      // Header styles
      header: {
        color: "#FFFFFF",
        // fontFamily: "Avenir",
        fontSize: 30,
        fontWeight: "bold",
        marginVertical: 3
      },
      // Text below header
      text: {
        color: "#FFFFFF",
        // fontFamily: "Avenir",
        fontSize: 18,
        marginHorizontal: 40,
        textAlign: "center"
      }
});
