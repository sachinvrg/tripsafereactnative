import { AsyncStorage, Platform, NetInfo } from 'react-native';
import moment from 'moment';
import strings from '../values/stringEn';
import Colors from '../values/colors';
import { GET } from '../hooks/constants';
import { useHttpFile } from '../hooks/useHttpss';

export default {
  /**
   * To Store the User data in LocalStorage
   * @param {*} key
   * @param {*} value
   */

  store_item_async: async (key, value) => {
    try {
      await AsyncStorage.setItem(key, `${value}`);
    } catch (error) { }
  },
  returnStatus: value => {
    switch (value) {
      case -1:
        return 'Deactive';
      case 0:
        return 'Unavailable';
      case 1:
        return 'Active';
      default:
        return 'Defult';
    }
  },
  fetch_item_async: async key => AsyncStorage.getItem(key),
  remove_item_async: async key => {
    try {
      await AsyncStorage.removeItem(key);
    } catch (error) { }
  },
  dateFormation(currentDate2) {
    var day2 = currentDate2.getDate();
    var month2 = currentDate2.getMonth() + 1;
    var year2 = currentDate2.getFullYear();
    var hours2 = currentDate2.getHours();
    var minutes2 = currentDate2.getMinutes();
    var ampm2 = hours2 >= 12 ? 'pm' : 'am';
    hours2 = hours2 % 12;
    hours2 = hours2 ? hours2 : 12; // the hour '0' should be '12'
    minutes2 = minutes2 < 10 ? '0' + minutes2 : minutes2;
    var strTime2 = hours2 + ':' + minutes2 + ' ' + ampm2;
    return `${day2}/${month2}/${year2}, ${strTime2}`;
  },

  validateMobileno(numericStr) {
    const phoneno = /^[0-9]{8,10}$/;
    // /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    if (new RegExp(phoneno).test(numericStr)) {
      return true;
    }
    return false;
  },

  validateMobileOrPhone(str) {
    const phoneno = /^\+[1-9]{1}[0-9]{3,14}$/;
    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    console.log('email field ', str.match(phoneno));
    if (str.match(phoneno)) {
      return { status: 'mobileNumber', value: str };
    } else if (str.match(regex)) {
      return { status: 'email', value: str };
    } else {
      return { status: false, value: '' };
    }
  },

  validateMobileEmail(str) {
    // const phoneno = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    const phoneno = /^[0-9]{8,10}$/;
    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (new RegExp(phoneno).test(str)) {
      return true;
    } else if (str.match(regex)) {
      return true;
    } else {
      return false;
    }
  },
  /**
   *
   * To find if an email is valid or not
   * @param {*} email
   * @returns true if valid false if not
   */
  validateSpace(value) {
    // //console.log("In ValidateSpace fun", value);
    const spaceRegex = new RegExp('\\s'); // /^[" "]*$/; // /\s/g;
    return spaceRegex.test(value);
  },
  validateNumber(number) {
    // const numberFormet = /^\d{10}$/;
    // /^[0-9]*$/
    const numberFormet = /^[0-9]*$/;
    return numberFormet.test(number);
  },
  validateEmail(email) {
    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(email);
  },
  stringHasSpecialChars(str) {
    const regex = /^[a-zA-Z0-9 ]*$/;
    return regex.test(str);
  },
  validateString(string) {
    // const charRegex = /^[a-zA-Z 0-9]*$/; // .*\\d+.*  /\d/;
    // const charRegex = new RegExp();
    return !/\d/.test(string);
    //
  },
  isNetworkConnected() {
    if (Platform.OS === 'ios') {
      return new Promise(resolve => {
        const handleFirstConnectivityChangeIOS = isConnected => {
          NetInfo.isConnected.removeEventListener(
            'connectionChange',
            handleFirstConnectivityChangeIOS,
          );
          resolve(isConnected);
        };
        NetInfo.isConnected.addEventListener(
          'connectionChange',
          handleFirstConnectivityChangeIOS,
        );
      });
    }
    return NetInfo.isConnected.fetch();
  },

  validateLoginForm(formData) {
    const iterator = Object.keys(formData);
    let error = false;
    if (iterator.length > 0) {
      const validationArr = [];
      for (let index = 0; index < iterator.length; index++) {
        if (formData[iterator[index]] === undefined) {
          formData[iterator[index]] = '';
        }
        if (formData[iterator[index]].required) {
          if (
            formData[iterator[index]].value !== undefined &&
            formData[iterator[index]].value !== '' &&
            formData[iterator[index]].value !== null &&
            `${formData[iterator[index]].value}`.trim() !== ''
          ) {
            if (formData[iterator[index]].propname === 'email') {
              if (!this.validateMobileEmail(formData[iterator[index]].value)) {
                formData[iterator[index]].err = true;
                formData[iterator[index]].errMsg =
                  strings.errors.validEmailMobileError;
              } else {
                formData[iterator[index]].err = false;
                formData[iterator[index]].errMsg = '';
              }
            }
            if (formData[iterator[index]].propname === 'password') {
              if (formData[iterator[index]].value.length < 8) {
                formData[iterator[index]].err = true;
                formData[iterator[index]].errMsg = strings.errors.Max_Limit;
              } else if (this.validateSpace(formData[iterator[index]].value)) {
                formData[iterator[index]].err = true;
                formData[iterator[index]].errMsg = strings.errors.noSpace;
              } else {
                formData[iterator[index]].err = false;
                formData[iterator[index]].errMsg = '';
              }
            }
          } else {
            formData[iterator[index]].err = true;
            formData[iterator[index]].errMsg = strings.errors.requiredField;
          }
        } else {
          formData[iterator[index]].err = false;
          formData[iterator[index]].errMsg = '';
        }
        validationArr.push(formData[iterator[index]].err);
      }

      if (validationArr.indexOf(true) !== -1) {
        error = true;
      } else {
        error = false;
      }
    } else {
      error = true;
    }
    return new Promise(resolve => {
      resolve({ error, formDataObj: formData });
    });
  },
  validatePasswordForm(formData) {
    const iterator = Object.keys(formData);
    let error = false;
    if (iterator.length > 0) {
      const validationArr = [];
      for (let index = 0; index < iterator.length; index++) {
        if (formData[iterator[index]] === undefined) {
          formData[iterator[index]] = '';
        }
        if (formData[iterator[index]].required) {
          if (
            formData[iterator[index]].value !== undefined &&
            formData[iterator[index]].value !== '' &&
            formData[iterator[index]].value !== null &&
            `${formData[iterator[index]].value}`.trim() !== ''
          ) {
            if (
              formData[iterator[index]].propname === 'confirmPassword' ||
              formData[iterator[index]].propname === 'currentPassword' ||
              formData[iterator[index]].propname === 'newPassword' ||
              formData[iterator[index]].propname === 'password'
            ) {
              if (formData[iterator[index]].value.length < 8) {
                formData[iterator[index]].err = true;
                formData[iterator[index]].errMsg = strings.errors.Max_Limit;
              } else if (this.validateSpace(formData[iterator[index]].value)) {
                formData[iterator[index]].err = true;
                formData[iterator[index]].errMsg = strings.errors.noSpace;
              } else {
                formData[iterator[index]].err = false;
                formData[iterator[index]].errMsg = '';
              }
            }
          } else {
            formData[iterator[index]].err = true;
            formData[iterator[index]].errMsg = strings.errors.requiredField;
          }
        } else {
          formData[iterator[index]].err = false;
          formData[iterator[index]].errMsg = '';
        }
        validationArr.push(formData[iterator[index]].err);
      }

      if (validationArr.indexOf(true) !== -1) {
        error = true;
      } else {
        error = false;
      }
    } else {
      error = true;
    }
    return new Promise(resolve => {
      resolve({ error, formDataObj: formData });
    });
  },
  validateForm(formData) {
    const iterator = Object.keys(formData);
    let error = false;
    if (iterator.length > 0) {
      const validationArr = [];
      for (let index = 0; index < iterator.length; index++) {
        if (formData[iterator[index]] === undefined) {
          formData[iterator[index]] = '';
        }
        if (formData[iterator[index]].required) {
          if (
            formData[iterator[index]].value !== undefined &&
            formData[iterator[index]].value !== '' &&
            formData[iterator[index]].value !== null &&
            `${formData[iterator[index]].value}`.trim() !== ''
          ) {
            if (
              formData[iterator[index]].propname === 'first_name' ||
              formData[iterator[index]].propname === 'last_name'
            ) {
              if (this.validateSpace(formData[iterator[index]].value)) {
                formData[iterator[index]].err = true;
                formData[iterator[index]].errMsg = strings.errors.noSpace;
              } else if (
                !this.validateString(formData[iterator[index]].value)
              ) {
                formData[iterator[index]].err = true;
                formData[iterator[index]].errMsg =
                  strings.errors.onlyAlphabatesAlowed;
              } else {
                formData[iterator[index]].err = false;
                formData[iterator[index]].errMsg = '';
              }
            } else if (formData[iterator[index]].propname === 'phone') {
              if (!this.validateMobileno(formData[iterator[index]].value)) {
                formData[iterator[index]].err = true;
                formData[iterator[index]].errMsg =
                  strings.errors.enterValidMobileno;
              } else {
                formData[iterator[index]].err = false;
                formData[iterator[index]].errMsg = '';
              }
            } else if (formData[iterator[index]].propname === 'email') {
              if (!this.validateEmail(formData[iterator[index]].value)) {
                formData[iterator[index]].err = true;
                formData[iterator[index]].errMsg =
                  strings.errors.validEmailError;
              } else {
                formData[iterator[index]].err = false;
                formData[iterator[index]].errMsg = '';
              }
            } else if (
              formData[iterator[index]].isNumber &&
              !this.validateNumber(formData[iterator[index]].value)
            ) {
              formData[iterator[index]].err = true;
              formData[iterator[index]].errMsg = strings.Only_Numbers;
            } else if (
              formData[iterator[index]].checkSpecialCharacter &&
              !this.stringHasSpecialChars(formData[iterator[index]].value)
            ) {
              formData[iterator[index]].err = true;
              formData[iterator[index]].errMsg =
                strings.errors.specialCharacterNotAllowed;
            } else if (
              formData[iterator[index]].propname === 'password' ||
              formData[iterator[index]].propname === 'confirmPassword' ||
              formData[iterator[index]].propname === 'oldPassword' ||
              formData[iterator[index]].propname === 'newPassword' ||
              formData[iterator[index]].propname === 'repeatPassword'
            ) {
              console.log('password model IF', formData[iterator[index]].value);
              if (formData[iterator[index]].value.length < 8) {
                formData[iterator[index]].err = true;
                formData[iterator[index]].errMsg = strings.errors.Max_Limit;
              } else if (this.validateSpace(formData[iterator[index]].value)) {
                formData[iterator[index]].err = true;
                formData[iterator[index]].errMsg = strings.errors.noSpace;
              } else if (
                formData[iterator[index]].propname === 'repeatPassword'
              ) {
                if (
                  formData[iterator[index]].value === formData.newPassword.value
                ) {
                  formData[iterator[index]].err = false;
                  formData[iterator[index]].errMsg = '';
                } else {
                  formData[iterator[index]].err = true;
                  formData[iterator[index]].errMsg = strings.errors.MatchPass;
                }
              } else {
                formData[iterator[index]].err = false;
                formData[iterator[index]].errMsg = '';
              }
            } else {
              formData[iterator[index]].err = false;
              formData[iterator[index]].errMsg = '';
            }
          } else if (formData[iterator[index]].propname === 'categories') {
            formData[iterator[index]].err = false;
            formData[iterator[index]].errMsg =
              'Please add category and sub-category.';
          } else {
            formData[iterator[index]].err = true;
            formData[iterator[index]].errMsg = strings.errors.requiredField;
          }
        } else {
          formData[iterator[index]].err = false;
          formData[iterator[index]].errMsg = '';
        }
        validationArr.push(formData[iterator[index]].err);
      }
      if (validationArr.indexOf(true) !== -1) {
        error = true;
      } else {
        error = false;
      }
    } else {
      error = true;
    }
    return new Promise(resolve => {
      resolve({ error, formDataObj: formData });
    });
  },

  processData: data => {
    const sundriesObj = {};
    const processedData = {};
    if (Array.isArray(data)) {
      for (let index = 0; index < data.length; index++) {
        const element = data[index];
        processedData[element.propname] = element.val;
      }
    } else {
      Object.keys(data).map(item => {
        if (data[item].element === 'image') {
          const tempArray = [];
          if (data[item].value[0].id) {
            const { id } = data[item].value[0];
            const obj = {};
            obj[id] = 'No notes available';
            tempArray.push(obj);
            processedData[item] = obj;
          }
        } else if (
          item === 'deutschPlugQuantity' ||
          item === 'conduitLength' ||
          item === '3CoreLength' ||
          item === '2CoreLength'
        ) {
          sundriesObj[data[item].sundries_id] = data[item].value;
          processedData.sundries = sundriesObj;
        }
        // else if (item === 'accessories' || item === 'cablesHarness') {
        //   // selectedData.map((item) => (item.value))
        //   processedData[item] = data[item].value.map((item1) => (item1.value));
        // }
        else {
          processedData[item] = data[item].value;
        }
      });
    }
    return processedData;
  },

  getMasterDataList(ListItem, masterData) {
    if (ListItem.options) {
      Object.keys(masterData.MasterData.data).map((item, index) => {
        if (ListItem.propname == item) {
          const singleArrayItem = masterData.MasterData.data[item];
          singleArrayItem.map((innerItem, index) => {
            const dataKey = Object.values(innerItem);
            innerItem.value = dataKey[0];
            if (ListItem.propname === 'equipmentType') {
              innerItem.label = `${dataKey[0]}`;
            } else {
              innerItem.label = `${dataKey[1]}`;
            }
          });
          ListItem.options = singleArrayItem;
        }
      });
      return ListItem.options;
    }
    return ListItem.options;
  },

  getQuantSum(ArrayItem) {
    let quantity = 0;
    ArrayItem.forEach(element => {
      quantity += element.quantity;
    });
    return quantity;
  },

  actionOnObject(object, action) {
    switch (action) {
      case 'delete':
      // return delete (object);
      case 1:
      // return delete (object);
      default:
        return object;
    }
  },

  getCurrentDateAndTime() {
    const today = new Date();
    const date = `${today.getFullYear()}-${(today.getMonth() > 8
      ? today.getMonth()
      : `0${today.getMonth()}`) + 1}-${
      today.getDate() > 9 ? today.getDate() : `0${today.getDate()}`
      }`;
    const time = `${today.getHours()}:${
      today.getMinutes() > 9 ? today.getMinutes() : `0${today.getMinutes()}`
      }:${
      today.getSeconds() > 9 ? today.getSeconds() : `0${today.getSeconds()}`
      }`;
    const dateTime = `${date} ${time}`;
    return dateTime;
  },

  miliSecondsToTime(duration) {
    const milliseconds = parseInt((duration % 1000) / 100);
    let seconds = Math.floor((duration / 1000) % 60);
    let minutes = Math.floor((duration / (1000 * 60)) % 60);
    let hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

    hours = hours < 10 ? `0${hours}` : hours;
    minutes = minutes < 10 ? `0${minutes}` : minutes;
    seconds = seconds < 10 ? `0${seconds}` : seconds;

    return `${hours}:${minutes}:${seconds}.${milliseconds}`;
  },

  SplitTime(numberOfHours) {
    console.log('numberOfHours', numberOfHours);

    var Days = Math.floor(numberOfHours / 24);
    var Remainder = numberOfHours % 24;
    var Hours = Math.floor(Remainder);
    var Minutes = Math.floor(60 * (Remainder - Hours));
    console.log('minutes', Days, Hours, Minutes);

    return { Days: Days, Hours: Hours, Minutes: Minutes };
  },

  getToolStatus(mStatus) {
    switch (mStatus) {
      case status.TOOL.UNAVAILABLE:
        return { color: Colors.Status.UNAVAILABLE, text: 'Unavailable' };
      case status.TOOL.ACTIVE:
        return { color: Colors.Status.AVAILABLE, text: 'Available' };
      case status.TOOL.CHECKEDOUT:
        return { color: Colors.Status.CHECKEDOUT, text: 'Checked out' };
      case status.TOOL.BOOKED:
        return { color: Colors.Status.RESERVED, text: 'Reserved' };
      case status.TOOL.STOLEN:
        return { color: Colors.Status.STOLEN, text: 'Stolen' };
      case status.TOOL.DAMAGED:
        return { color: Colors.Status.DAMAGED, text: 'Damaged' };
      case status.TOOL.LOST:
        return { color: Colors.Status.LOST, text: 'Lost' };
      case status.TOOL.DELETE:
        return { color: Colors.Status.DELETED, text: 'Deleted' };
      case status.TOOL.CANCELLED:
        return { color: Colors.Status.CANCELLED, text: 'Cancelled' };
      case status.TOOL.OUTOFSERVICES:
        return { color: Colors.Status.OUTOFSERVICES, text: 'Out for service' };
      case status.TOOL.RETURN_REQUEST:
        return { color: Colors.Status.RETURN_REQUEST, text: 'Return request' };
      case status.TOOL.DEFULT:
        return { color: Colors.Status.OUTOFSERVICES, text: '----' };
      default:
        return { color: '', text: '' };
    }
  },
  getLable(value) {
    switch (value) {
      case 'Booking':
      case 'Checkout':
      case 'Reservation':
      case 'Request':
      case 'Transfer':
      case 'Return':
        return `${value}s`;
      default:
        return value;
    }
  },
  getToolStatusByOrderItems(mStatus) {
    switch (mStatus) {
      case status.ORDERED_TOOLS.PENDING:
        return { color: Colors.Status.UNAVAILABLE, text: 'Pending' };
      case status.ORDERED_TOOLS.ACTIVE:
        return { color: Colors.Status.CHECKEDOUT, text: 'Checked out' };
      case status.ORDERED_TOOLS.CHECKEDOUT:
        return { color: Colors.Status.CHECKEDOUT, text: 'Checked out' };
      // case status.ORDERED_TOOLS.BOOKED:
      //   return {color: Colors.Status.RESERVED, text: 'Reserved'};
      case status.ORDERED_TOOLS.STOLEN:
        return { color: Colors.Status.STOLEN, text: 'Stolen' };
      case status.ORDERED_TOOLS.DAMAGED:
        return { color: Colors.Status.DAMAGED, text: 'Damaged' };
      case status.ORDERED_TOOLS.LOST:
        return { color: Colors.Status.LOST, text: 'Lost' };
      case status.ORDERED_TOOLS.DELETE:
        return { color: Colors.Status.DELETED, text: 'Deleted' };
      case status.ORDERED_TOOLS.CANCELLED:
        return { color: Colors.Status.CANCELLED, text: 'Cancelled' };
      case status.ORDERED_TOOLS.RETURN:
        return { color: Colors.Status.DELETED, text: 'Return' };
      case status.ORDERED_TOOLS.TRANSFERRED:
        return { color: Colors.Status.TRANSFERRED, text: 'Transferred' };
      case status.ORDERED_TOOLS.TRANSFER_REQUEST:
        return {
          color: Colors.Status.TRANSFER_REQUEST,
          text: 'Transfer request',
        };
      case status.ORDERED_TOOLS.RETURNED:
        return { color: Colors.Status.RETURN, text: 'Returned' };
      case status.ORDERED_TOOLS.RETURN_REQUEST:
        return { color: Colors.Status.RETURN_REQUEST, text: 'Return request' };
      default:
        return { color: '', text: '' };
    }
  },
  getToolStatusForCheckout(mStatus) {
    switch (mStatus) {
      case status.TOOLVIEWCHECKOUT.CHECKEDOUT:
        return { color: Colors.Status.CHECKEDOUT, text: 'Checked out' };
      case status.TOOLVIEWCHECKOUT.STOLEN:
        return { color: Colors.Status.STOLEN, text: 'Stolen' };
      case status.TOOLVIEWCHECKOUT.DAMAGED:
        return { color: Colors.Status.DAMAGED, text: 'Damaged' };
      case status.TOOLVIEWCHECKOUT.LOST:
        return { color: Colors.Status.LOST, text: 'Lost' };
      default:
        return { color: '', text: '' };
    }
  },

  getReturnOnDuration(value) {
    var ms = moment(moment(value), 'DD/MM/YYYY HH:mm:ss').diff(
      moment(new Date(), 'DD/MM/YYYY HH:mm:ss'),
    );
    var d = moment.duration(ms);
    if (d.minutes() > 0) {
      return d.years() > 0
        ? ``
        : `(${
        d.months() > 0
          ? d.months() + `${d.months() > 1 ? ' months ' : ' month '}`
          : ''
        }${
        d.days() > 0
          ? d.days() + `${d.days() > 1 ? ' days ' : ' day '}`
          : ''
        }${
        d.hours() > 0
          ? d.hours() < 10
            ? '0' + d.hours()
            : d.hours()
          : '00'
        }:${
        d.minutes() > 0
          ? d.minutes() < 10
            ? '0' + d.minutes()
            : d.minutes()
          : '00'
        } hrs)`;
    } else {
      return `(${
        Math.abs(d.months()) > 0
          ? `${Math.abs(d.months()) < 10 ? '0' : ''}${Math.abs(d.months())}` +
          `${Math.abs(d.months()) > 1 ? ' months ' : ' month '}`
          : ''
        }${Math.abs(d.days()) < 10 && Math.abs(d.days()) > 0 ? '0' : ''}${
        Math.abs(d.days()) > 0
          ? Math.abs(d.days()) +
          `${Math.abs(d.days()) > 1 ? ' days ' : ' day '}`
          : ''
        }${Math.abs(d.hours()) < 10 ? '0' : ''}${Math.abs(d.hours()) + ':'}${
        Math.abs(d.minutes()) < 10 ? '0' : ''
        }${Math.abs(d.minutes()) + ' hrs'} passed)`;
    }
  },

  getValidTillDuration(value) {
    var hours = moment(value).diff(moment(new Date()), 'hours');
    var minutes = moment(value).diff(moment(new Date()), 'minutes') % 60;

    if (minutes > 0) {
      return `${hours < 10 ? '0' + hours : hours}:${
        minutes < 10 ? '0' + minutes : minutes
        } hrs`;
    } else {
      return `${
        Math.abs(hours) < 10 ? '0' + Math.abs(hours) : Math.abs(hours)
        }:${
        Math.abs(minutes) < 10 ? '0' + Math.abs(minutes) : Math.abs(minutes)
        } hrs`;
    }
  },

  miliSecondsToSeconds(duration) {
    const seconds = Math.floor(duration / 1000);
    return seconds;
  },
  prepareFiltersForAPICalling(queryData, screenName = '') {
    console.log('QueryData====>', queryData);
    let queryString = '';

    if (
      queryData.hasOwnProperty('radioSelectedItem') &&
      queryData.radioSelectedItem &&
      queryData.radioSelectedItem.module &&
      queryData.radioSelectedItem.module != ''
    ) {
      queryString += '&module=' + queryData.radioSelectedItem.module;
    }
    if (
      queryData.hasOwnProperty('radioSelectedItem') &&
      queryData.radioSelectedItem &&
      queryData.radioSelectedItem.jobSite &&
      queryData.radioSelectedItem.jobSite != ''
    ) {
      queryString += '&jobSiteId=' + queryData.radioSelectedItem.jobSite;
    }
    if (
      queryData.hasOwnProperty('radioSelectedItem') &&
      queryData.radioSelectedItem &&
      queryData.radioSelectedItem.sortBy &&
      queryData.radioSelectedItem.sortBy != ''
    ) {
      queryString += '&sortBy=' + queryData.radioSelectedItem.sortBy;
    }
    if (
      queryData.hasOwnProperty('radioSelectedItem') &&
      queryData.radioSelectedItem &&
      queryData.radioSelectedItem.Section &&
      queryData.radioSelectedItem.module != ''
    ) {
      if (screenName === 'History') {
        queryString += '&section=' + queryData.radioSelectedItem.Section;
      } else {
        queryString += '&type=' + queryData.radioSelectedItem.Section;
      }
    }

    if (
      queryData.hasOwnProperty('radioSelectedItem') &&
      queryData.radioSelectedItem &&
      queryData.radioSelectedItem.avgQuantity &&
      queryData.radioSelectedItem.avgQuantity != ''
    ) {
      let quant = queryData.radioSelectedItem.avgQuantity.split('-');
      if (
        quant.length >= 2 &&
        queryData.radioSelectedItem.avgQuantity != '0-0' &&
        quant[1] > quant[0]
      ) {
        queryString = `&minQty=${quant[0]}&maxQty=${quant[1]}`;
      }
    }

    queryData.checkboxSelectedItem &&
      queryData.checkboxSelectedItem.category &&
      queryData.checkboxSelectedItem.category.length > 0 &&
      queryData.radioSelectedItem.module !== 'MANAGEUSER' &&
      queryData.radioSelectedItem.module !== 'PROFILE' &&
      queryData.checkboxSelectedItem.category.map(item => {
        queryString += '&toolId=' + item;
      });

    queryData.checkboxSelectedItem &&
      queryData.checkboxSelectedItem.trade &&
      queryData.checkboxSelectedItem.trade.length > 0 &&
      queryData.radioSelectedItem.module == 'MANAGEUSER' &&
      queryData.checkboxSelectedItem.trade.map(item => {
        queryString += '&tradeId=' + item;
      });

    queryData.checkboxSelectedItem &&
      queryData.checkboxSelectedItem.brands &&
      queryData.checkboxSelectedItem.brands.length > 0 &&
      queryData.checkboxSelectedItem.brands.map(item => {
        queryString += '&brandId=' + item;
      });

    queryData.checkboxSelectedItem &&
      queryData.checkboxSelectedItem.userType &&
      queryData.checkboxSelectedItem.userType.length > 0 &&
      queryData.radioSelectedItem.module == 'MANAGEUSER' &&
      queryData.checkboxSelectedItem.userType.map(item => {
        queryString += '&userType=' + item;
      });

    queryData.checkboxSelectedItem &&
      queryData.checkboxSelectedItem.SubSection &&
      queryData.checkboxSelectedItem.SubSection.length > 0 &&
      queryData.checkboxSelectedItem.SubSection.map(item => {
        queryString += '&subSection=' + item;
      });

    queryData.checkboxSelectedItem &&
      queryData.checkboxSelectedItem.roleId &&
      queryData.checkboxSelectedItem.roleId.length > 0 &&
      queryData.checkboxSelectedItem.roleId.map(item => {
        queryString += '&roleId=' + item;
      });

    queryData.checkboxSelectedItem &&
      queryData.checkboxSelectedItem.transferType &&
      queryData.checkboxSelectedItem.transferType.length > 0 &&
      queryData.checkboxSelectedItem.transferType.map(item => {
        queryString += '&status=' + item;
      });

    queryData.checkboxSelectedItem &&
      queryData.checkboxSelectedItem.status &&
      queryData.checkboxSelectedItem.status.length > 0 &&
      queryData.checkboxSelectedItem.status.map(item => {
        queryString += '&status=' + parseInt(item);
      });

    if (
      queryData.dateRangeSelectedItem &&
      queryData.dateRangeSelectedItem.fromDate &&
      queryData.dateRangeSelectedItem.toDate
    )
      queryString +=
        '&fromDate=' +
        new Date(queryData.dateRangeSelectedItem.fromDate).toISOString() +
        '&toDate=' +
        new Date(queryData.dateRangeSelectedItem.toDate).toISOString();
    return queryString;
  },
  callDownloadapi(requestObj) {
    useHttpFile(
      requestObj.url,
      requestObj.methodType,
      requestObj.authorization,
      requestObj.name,
      requestObj.type,
    )
      .then(res => {
        FileViewer.open(res.path())
          .then(() => { })
          .catch(error => {
            alert(strings.No_app_available);
          });
      })
      .catch(err => { });
  },

  //   getImageFormatData(propname, value) {
  //     var imageArray = []
  //     const data1 = imageList.reduce((currentData, newData) => ({ ...currentData, [newData.id]: newData.notesValue }), {});
  //     console.log("imageList view", data1, imageList);
  //     return
  //  },

  /**
   *
   *
   * @param {*} message message to show in Toast
   * @param {*} duration duration the Toast should be visible
   * @param {string} [type="danger"|"success"|"warning"]
   */
  // ShowToast: (message, duration, type = 'danger' | 'success' | 'warning') =>
  //   Toast.show({
  //     type,
  //     text: message,
  //     textStyle: { textAlign: 'center' },
  //     position: 'bottom',
  //     duration
  //   })
};
