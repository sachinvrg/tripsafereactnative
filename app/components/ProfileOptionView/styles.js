import { StyleSheet } from 'react-native';
import Util from '../../common/util';
import Colors from '../../values/colors';
// import { color } from 'react-native-reanimated';

const styles = StyleSheet.create({
  optionsRow: {
    width: Util.getWidth(85),
    flexDirection: 'row',
    marginTop: Util.getHeight(2),
    height: Util.getHeight(6),
    // backgroundColor: 'red'
  },
  leftView: {
    // backgroundColor:'green',
    width: Util.getWidth(7),
    height: Util.getHeight(6),
    marginRight:Util.getWidth(5),
    alignItems: 'center',
    justifyContent: 'center'
  },
  rightView: {
    // backgroundColor:'yellow',
    width: Util.getWidth(70),
    height: Util.getHeight(6),
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderBottomColor: Colors.BorderColor.BorderColorPrimary,
    borderBottomWidth: Util.getHeight(0.2)
  },
  rightViewLogout: {
    // backgroundColor:'yellow',
    width: Util.getWidth(70),
    height: Util.getHeight(6),
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
 
  },
  textStyle:{
    fontSize:Util.normalize(13),
    fontWeight:'bold',
    color:Colors.NavyBlue
    

    
  },
  textStyleLogout:{
    fontSize:Util.normalize(13),
    fontWeight:'bold',
    
    color: Colors.TextColor.SecondaryTextColor,

    
  },
});
export default styles;
