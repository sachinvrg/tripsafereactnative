import { StyleSheet } from 'react-native';
import Util from '../../common/util';
import Colors from '../../values/colors';

const styles = StyleSheet.create({
  cardView: {
    marginTop: 10,
    borderRadius: 15,
    width: Util.getWidth(88),
    height: Util.getHeight(25),
    backgroundColor: Colors.EXTRACOLORS.WHITE,
    paddingHorizontal: 50,
    paddingVertical: 12
  },
  textButton: {
    color: Colors.EXTRACOLORS.WHITE,
    fontSize: Util.normalize(13)
  },
  viewLeft: {
    justifyContent: 'flex-start', width: '80%', marginLeft: '-20%'
  },
  viewRight: {
    width: '60%',
    alignItems: 'center', justifyContent: 'center'
  },
  qrImage: {
    flex: 1,
    width: 120,
    height: 120,
    resizeMode: 'contain'
  },
  profilePicImage: {
    height: Util.getHeight(8),
    width: Util.getHeight(8),
    borderRadius: Util.getHeight(10),
    // backgroundColor:'red',
    marginLeft: '8%',
    marginTop:Util.getHeight(2)
    // paddingLeft: 10,
    // backgroundColor:"red"
    // overflow: 'hidden'
  },
  textName:{
    color: Colors.NavyBlue,
    fontSize:Util.normalize(14),
    fontWeight:'bold',
    marginLeft:'8%'
  },
  textId:{
    color: Colors.TextColor.SecondaryTextColor,
    fontSize:Util.normalize(10),
    // fontWeight:'bold',
    marginLeft:'8%'
  },
  textIdValue:{
    color: Colors.NavyBlue,
    fontSize:Util.normalize(10),
    fontWeight:'bold',
  }
});
export default styles;
