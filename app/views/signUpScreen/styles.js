import { StyleSheet } from 'react-native';
import Util from '../../common/util';
import Colors from '../../values/colors';

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  splashLogo: {
    height: Util.getHeight(30),
    width: Util.getWidth(50),
  },
  container: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
    // alignSelf: 'baseline'
  },
  containerScroll: {
    flex: 1,
    flexDirection: "column"
  },

  rememberMeText: {

    fontSize: Util.normalize(13),
  },
  boxContainer: {

    flexDirection: 'column',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',

  },
  loginBox: {
    flex: 1,
    flexDirection: 'column',
    width: '100%',
    height: Util.getHeight(95),
    marginTop:Util.getHeight(4)



  },
  loginContinue: {
    textAlign: 'left',
    color: Colors.NavyBlue,
    fontSize: Util.normalize(25),
    fontWeight: 'bold',
    marginTop: '8%',
    marginLeft: '-15%'

  },
  createAccount: {
    textAlign: 'left',
    color: Colors.NavyBlue,
    fontSize: Util.normalize(25),
    fontWeight: 'bold',
    marginTop: '3%',
    marginLeft: '-15%'

  },
  box1: {
    flexGrow: 1,
    position: 'absolute',
    top: 20,
    alignSelf: 'baseline',
    width: "100%",
    marginLeft: '20%',
    marginRight: '20%',
    //justifyContent: 'center',
    // alignContent: 'center',
    height: Util.getHeight(84),
    alignSelf: 'center',
    borderRadius: Util.getWidth(5),
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: 'white'


  },
  box2: {
    paddingLeft: 10,
    paddingRight: 10,
    marginLeft: '25%',
    marginRight: '25%',
    position: 'absolute',
    top: 30,
    alignSelf: 'baseline',
    width: "100%",
    alignContent: 'center',
    justifyContent: 'center',
    height: Util.getHeight(84),
    borderRadius: Util.getWidth(5),

    alignSelf: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    opacity: 0.8,

  },
  box3: {
    paddingLeft: 10,
    paddingRight: 10,
    marginLeft: '28%',
    marginRight: '28%',
    position: 'absolute',
    top: 40,
    alignSelf: 'baseline',
    alignContent: 'center',
    justifyContent: 'center',
    width: "100%",
    alignSelf: 'center',
    height: Util.getHeight(84),
    borderRadius: Util.getWidth(5),
    alignItems: 'center',
    backgroundColor: 'white',
    opacity: 0.5,


  },
  imageContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center'
  },
  imageStyle: {
    marginTop: '5%', flex: 1,
    width: 80,
    height: 40,
    resizeMode: 'contain'
  },
  pleaseEnterCredential: {
    marginTop: 15,
    marginBottom: '5%',
    textAlign: 'left',
    marginLeft: '-20%',
    color: Colors.TextColor.SecondaryTextColor,
    fontSize: Util.normalize(12),
    fontWeight: 'bold'
  },
  inputLable: {
    marginTop: '1%',
    marginBottom: '1%',
    textAlign: 'left',
    alignSelf:'flex-start',
    marginLeft:'2%',
    color: Colors.TextColor.SecondaryTextColor,
    fontSize: Util.normalize(12),
    fontWeight: 'bold'
  },
  radioForgetView: {
    flexDirection: 'row',
    width: '95%',
    // justifyContent: 'space-between',
    alignItems: 'center',
    // marginRight:'13%'
    // paddingLeft: '3%',
    // marginHorizontal: 7,
  },
  checkbox: {
    height: 15,
    width: 15,
    borderWidth: 1.2,
    borderRadius: 2,
  },
  forgetPsw: {
    color: '#3366FF',
    fontSize: Util.normalize(10),

  },
  byClicking: {
    // color: '#3366FF',
    fontSize: Util.normalize(10),

  },


});
export default styles;
