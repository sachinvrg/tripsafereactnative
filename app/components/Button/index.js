import React, { useContext } from 'react';
import {
    View,
    TouchableOpacity,
    ScrollView,
    ImageBackground,
    TextInput,
    Image,
} from 'react-native';
import { SafeAreaView } from 'react-navigation';
import {
    Input,
    Icon,
    Text,
    Button,
    Checkbox,
} from 'galio-framework';

import Styles from './styles';
import Colors from '../../values/colors';


export const ButtonComponent = (props) => {
    const { buttonText, onClick } = props;
    return (
        <TouchableOpacity onPress={onClick}>
            <View style={Styles.button}>
                <Text style={Styles.textButton}>{buttonText}</Text>
            </View>
        </TouchableOpacity>
    )
};

