import React, { Component } from "react";
import {
  AppRegistry,
  Image,
  ImageBackground,
  StyleSheet, // CSS-like styles
  Text,
  StatusBar, // Renders text
  View // Container component
} from "react-native";
import Images from '../../assets/Images/imagePath/images';

// import Icon from "react-native-vector-icons/Ionicons";

import Swiper from "./Swiper";
import styles from './Styles';

export default class Screen extends Component {
  render() {
    return (
      
      <Swiper navigation={this.props.navigation}>

        <ImageBackground style={styles.slide} source={Images.Bg}>
          <Image
            style={{ width: "100%", marginTop: "10%" }}
            source={Images.onboarding1}
            resizeMode="cover"
          />

          <Text style={styles.header}>one</Text>
          <Text style={styles.text}>one</Text>
        </ImageBackground>
        <ImageBackground style={styles.slide} source={Images.Bg}>
          <Image
            style={{ width: "100%", marginTop: "10%" }}
            source={Images.onboarding2}
            resizeMode="cover"
          />
          <Text style={styles.header}>two</Text>
          <Text style={styles.text}>two</Text>
        </ImageBackground>
        {/* Third screen */}
        <ImageBackground style={styles.slide} source={Images.Bg}>
          <Image
            style={{ width: "100%", marginTop: "10%" }}
            source={Images.onboarding3}
            resizeMode="cover"
          />
          <Text style={styles.header}>three</Text>
          <Text style={styles.text}>three</Text>
        </ImageBackground>
      </Swiper>
    );
  }
}

AppRegistry.registerComponent("Screen", () => Screen);