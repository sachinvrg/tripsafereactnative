// @ created by shivani lakhena
import { StyleSheet, Platform } from 'react-native';
import Util from '../../common/util';
import Colors from '../../values/colors';

const Styles = StyleSheet.create({
  textStyle: {
    fontSize: Util.normalize(18),
    color: Colors.EXTRACOLORS.ICONCOLOR,
  },
  horizontalView1: {

    height: '20%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginVertical: 8
  },
  horizontalView2: {
    flexDirection: 'row', width: '80%', alignItems: 'center', justifyContent: 'space-evenly'

  },
  iconForgotPass: {
    // flex: 1,
    width: Util.getWidth(55),
    // backgroundColor:'red',
    marginTop:Util.getHeight(2),
    height: Util.getHeight(30),
    
    // resizeMode: 'contain'
  },
  popup: {
    backgroundColor: "#FFF",

    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,

    height: Util.getHeight(85),
    position: 'absolute',
    bottom: 0,
    width: '100%'

  },
  iconStyle: {
    flex: 1,
    width: 20,
    height: 20,
    resizeMode: 'contain'
  },
  closeButton: {
    width: 40,
    height: 40,
    borderRadius: 10,
    shadowColor: '#000000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 5,
    position: 'absolute',
    right: 20,
    top: 20,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 4,
    alignSelf: 'flex-end',
    backgroundColor: Colors.EXTRACOLORS.WHITE
    // backgroundColor: 'red'

  },
  textFloatingButton: {
    textAlign: 'right', width: '65%', justifyContent: 'flex-start',
    fontSize: Util.normalize(13),
    color: 'white',
    fontWeight: 'bold'

  },

  profilePicImage: {
    height: Util.getHeight(15),
    width: Util.getHeight(15),
    borderRadius: Util.getHeight(10),

    marginTop: Util.getHeight(5)

  },

  textName: {
    color: Colors.NavyBlue,
    fontSize: Util.normalize(20),
    marginTop: Util.getHeight(2),
    fontWeight: 'bold',
  },
  textId: {
    color: Colors.TextColor.SecondaryTextColor,
    fontSize: Util.normalize(14),
    marginTop: Util.getHeight(1),

    // fontWeight:'bold',
  },
  textIdValue: {
    color: Colors.NavyBlue,
    fontSize: Util.normalize(14),
    marginTop: Util.getHeight(1),

    fontWeight: 'bold',
  },
  textViewStyle: {
    padding: Util.getWidth(3),
    marginTop: Util.getHeight(2),
    marginLeft: Util.getWidth(2),
  },
  textViewStyleTwo: {
    padding: Util.getWidth(1),
    marginLeft: Util.getWidth(2),
  },
  textViewStyleThree: {
    padding: Util.getWidth(1),
  },
  floatingLabel: {
    height: 54, width: 54,
    borderRadius: 54 / 2,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    position: "absolute",
    bottom: Util.getHeight(13), right: 18
  },
  floatingLabe2: {
    height: 54, width: 54,
    borderRadius: 54 / 2,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    // right: 18
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.MODELBACKGROUDCOLOR,
    height: Util.getHeight(100),
    width: Util.getWidth(100),
  },
  containerProfile: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    backgroundColor: Colors.MODELBACKGROUDCOLORPROFILE,
    height: Util.getHeight(100),
    width: Util.getWidth(100),
  },
  innerContainerTransparentStyle: {
    width: Util.getWidth(82),
    backgroundColor: Colors.EXTRACOLORS.WHITE,
    borderRadius: Util.getWidth(4),
    padding: Util.getWidth(7),
  },
  btnView: {
    width: Util.getWidth(68),
    height: 50,
    marginVertical: Util.getHeight(1),
    borderWidth: 2,
    borderColor: Colors.AppTheme.ColorPrimary,
    backgroundColor: Colors.AppTheme.AppBackground,
  },
  btnSecond: {
    backgroundColor: Colors.AppTheme.ColorPrimary,
  },
  btnText: {
    color: Colors.AppTheme.ColorPrimary,
    fontSize: Util.normalize(15),
    fontWeight: 'bold',
    fontFamily: Util.getFontFamily('regular'),
  },
  headingView: {
    justifyContent: 'center',
    alignContent: 'center',
    marginVertical: Util.getHeight(1),
  },
  headingStyle: {
    alignSelf: 'center',
    textTransform: 'capitalize',
    color: Colors.EXTRACOLORS.ICONCOLOR,
    fontWeight: 'bold',
    fontSize: Util.normalize(16),
    fontFamily: Util.getFontFamily('regular'),
  },
  dateView: {
    marginVertical: Util.getHeight(3),
  },
  rangeBtnViewStyle: { flexDirection: 'row' },
  rangeBtn: {
    width: '45%',
    marginTop: Util.getHeight(2.5),
    backgroundColor: Colors.MODELNUMBERBTNCOLOR,
    borderWidth: 1.5,
    borderColor: Colors.AppTheme.ColorPrimary,
  },
  rangeBtnInactive: {
    width: '45%',
    marginTop: Util.getHeight(2.5),
    backgroundColor: Colors.MODELNUMBERBTNCOLOR,
  },
  rangeBtnInactiveText: {
    color: Colors.EXTRACOLORS.MUTED,
    fontWeight: 'bold',
    fontFamily: Util.getFontFamily('regular'),
  },
  rangeBtnText: {
    color: Colors.EXTRACOLORS.ICONCOLOR,
    fontWeight: 'bold',
    fontFamily: Util.getFontFamily('regular'),
  },
  arrowIconStyle: {
    alignSelf: 'center',
    marginTop: Util.getHeight(2.5),
    marginHorizontal: Util.getWidth(1),
  },
  datePickerModelStyle: {
    height: Util.getHeight(20),
    width: Util.getWidth(67),
  },
});
export default Styles;
