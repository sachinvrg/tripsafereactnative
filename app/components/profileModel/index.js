// @ created by shivani lakhena
import React, { useState } from 'react';
import {
  View,
  Modal,
  Image,
  KeyboardAvoidingView,
  TouchableOpacity,
} from 'react-native';
import { Text, Button, Icon } from 'galio-framework/src';
import { ScrollView } from 'react-native-gesture-handler';
// Hooks, Api component calling

// Global helper classes
import Colors from '../../values/colors';
import strings from '../../values/stringEn';

// Local styles and Images
import styles from './style';
import images from '../../assets/Images/imagePath/images';
import Util from '../../common/util';

const ProfileViewModal = props => {
  const {
    ismodel,
    image,
    userName,
    userId,
    lastUpdatedDate,
    isClose,
    hideTitle,
    cancelText,
    button,
    onShow,
    qrImage

  } = props;

  const [modalVisible, setModalVisible] = useState(ismodel);
  const showModalVisible = visible => {
    onShow(false);
    setModalVisible(visible);
  };

  return (
    <View >
      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => showModalVisible(false)}>

        <View
          style={[
            styles.containerProfile,
          ]}>
          <View style={[styles.popup]}>
            <View style={{ width: "100%", flexDirection: 'row', paddingVertical: '5%', justifyContent: 'space-between' }} >
              {/* <Text style={styles.resetPassword}>{strings.resetPassword}</Text> */}
              <TouchableOpacity onPress={onShow} style={styles.closeButton}>
                <Image
                  style={
                    styles.iconStyle
                  }
                  source={images.crossIcon}
                  tintColor={'black'}
                />
              </TouchableOpacity>
            </View>
            <View style={{ width: "100%", alignItems: 'center' }} >

              <Image
                source={image}
                style={styles.profilePicImage}
              />
              <Text style={styles.textName}>
                {userName}
              </Text>
              <Text style={styles.textId}>
                {'ID No:  '}<Text style={styles.textIdValue}>{userId}</Text>
              </Text>

              <Text style={styles.textId}>
                {'Last Updated: '}<Text style={styles.textId}>{lastUpdatedDate}</Text>
              </Text>

              <Image
                style={
                  styles.iconForgotPass
                }
                source={qrImage}
              />
            </View>

          </View>
        </View>


      </Modal >
    </View >
  );
};
export default ProfileViewModal;
