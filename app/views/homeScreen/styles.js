import Util from '../../common/util';
import Colors, { getTheme } from '../../values/colors';
import { Platform, StyleSheet, Dimensions } from 'react-native';
import { THEMEKEY } from '../../hooks/constants';

const homeStyle = StyleSheet.create({
    // SIDE MENU STYLE
    mainView: {
        padding: '8%',
        paddingTop: '10%',
        paddingBottom: '30%',
        height: '95%',
        backgroundColor: Colors.EXTRACOLORS.CARDCOLOR,
    },
    profileView: {
        paddingLeft: '4%'
    },
    viewImageContainer: {
        // backgroundColor: 'red',
        flexDirection: 'row',
        width: Util.getWidth(85),
        height: Util.getHeight(18),
        marginTop: Util.getHeight(1),
    },
    
    border: {
        flexDirection: 'row',
        backgroundColor: Colors.BorderColor.BorderColorPrimary,
        width: Util.getWidth(85),
        height: Util.getHeight(0.15),
    },
    textName: {
        color: Colors.NavyBlue,
        fontSize: Util.normalize(20),
        // marginTop: Util.getHeight(2),
        fontWeight: 'bold',
    },
    textId: {
        color: Colors.TextColor.SecondaryTextColor,
        fontSize: Util.normalize(12),
        // marginTop: Util.getHeight(1),

        // fontWeight:'bold',
    },
    buttonBottom: {
        height: 50, width: 50,
        // backgroundColor: 'red',
        alignItems: 'center',
        position: "absolute",
        bottom: 15, right: 20
    },
    viewLeft: {

        justifyContent: 'flex-start', width: Util.getWidth(20),
    },
    viewRight: {
        width: Util.getWidth(60),
        marginLeft: Util.getWidth(10),
        justifyContent: 'center',
        // backgroundColor: 'red'
    },
    qrImage: {
        flex: 1,
        width: 120,
        height: 120,
        resizeMode: 'contain'
    },
    profilePicImage: {
        height: Util.getHeight(14),
        width: Util.getHeight(14),
        borderRadius: Util.getHeight(10),
        borderColor: Colors.THEME.SUCCESS,
        borderWidth: Util.getWidth(0.5),
        marginTop: Util.getHeight(2),
        alignItems: 'center',
        justifyContent: 'center'

    },
    cameraView: {
        height: Util.getHeight(4.5),
        width: Util.getHeight(4.5),
        borderRadius: Util.getHeight(2.5),
        backgroundColor: Colors.EXTRACOLORS.WHITE,
        marginTop: '-35%',
        marginRight: '-15%',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'flex-end',
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 5 },
        shadowOpacity: 0.5,
        shadowRadius: 20,
        alignItems: 'center',
        elevation: 10,

    },
    profilePic: {
        height: Util.getHeight(12),
        width: Util.getHeight(12),
        borderRadius: Util.getHeight(10),
        borderColor: Colors.THEME.SUCCESS,

    },
    cameraIcon: {
        height: Util.getHeight(2),
        width: Util.getHeight(2),
        resizeMode: 'contain'


    },
    viewContainer: {
        flex: 1, width: '100%',
        borderTopLeftRadius: 30,
        backgroundColor: Colors.EXTRACOLORS.WHITE,
        borderTopRightRadius: 30,
        alignItems: 'center'
    },
    bgActive: {
        backgroundColor: '#d0f0c0',
        width: Util.getWidth(24),
        marginLeft: Util.getWidth(2),
        marginRight: Util.getWidth(2)
        , paddingVertical: Util.getHeight(0.7)
        , paddingHorizontal: Util.getWidth(2), borderRadius: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },

    tabButton: {
        height: Util.getHeight(18), width: Util.getWidth(40), borderRadius: 10,
        marginHorizontal: Util.getWidth(2),
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 1,
        shadowRadius: 5,
        alignItems: 'center',
        // justifyContent: 'center',
        elevation: 4,
        // ali:'flex-end',
        backgroundColor: Colors.EXTRACOLORS.WHITE
        // backgroundColor: 'red'

    },

    tabButton2: {
        height: Util.getHeight(18), width: Util.getWidth(40), borderRadius: 10,
        marginHorizontal: Util.getWidth(2),
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 1,
        shadowRadius: 5,
        alignItems: 'center',
        // justifyContent: 'center',
        elevation: 4,
        // ali:'flex-end',
        // backgroundColor: Colors.EXTRACOLORS.WHITE
        backgroundColor: '#3ECFFA'

    },

    linearGradient: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        // borderRadius: 5,

        width: '100%',
    },
    closeButton: {
        marginRight: '-10%',
        width: 40,
        height: 40,
        borderRadius: 10,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 1,
        shadowRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 4,
        // ali:'flex-end',
        backgroundColor: Colors.EXTRACOLORS.WHITE
        // backgroundColor: 'red'

    },
    container: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center",
        width: '100%'
        // alignSelf: 'baseline'
    },
    containerScroll: {
        flex: 1,

        // backgroundColor:'#113CB4'

    },
    imageStyle: {
        marginTop: '2%',
        width: Util.getWidth(15),
        height: Util.getWidth(15),
        borderRadius: Util.getWidth(15) / 2,
    },
    imageStyle2: {
        marginTop: '2%',
        width: Util.getWidth(15),
        height: Util.getWidth(15),
        borderRadius: Util.getWidth(15) / 2,
        marginLeft: '4%'
    },
    name: {
        letterSpacing: 0.5,
        fontWeight: 'bold',
        paddingLeft: Util.getWidth(2.5),
        paddingTop: '5%',
        color: Colors.THEME.MODELNUMBERHEADINGCOLOR,
        fontFamily: Util.getFontFamily('regular'),
        fontStyle: 'normal',
        fontSize: Util.normalize(15),
        lineHeight: 19,
    },
    nameProflle: {
        letterSpacing: 0.5,
        paddingLeft: Util.getWidth(2.5),
        paddingTop: '2%',
        color: Colors.EXTRACOLORS.LIGHTTEXT,
        fontFamily: Util.getFontFamily('regular'),
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: Util.normalize(11),
        lineHeight: 14,
    },
    options: {
        paddingLeft: Util.getWidth(5),
        color: Colors.TEXTCOLORS.SIDEBARTEXT,
        fontFamily: Util.getFontFamily('regular'),
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: Util.normalize(13),
        lineHeight: 16,
    },
    line: {
        height: 1,
        width: '100%',
        marginVertical: Util.getHeight(2.5),
        backgroundColor: Colors.MODELNUMBERBTNCOLOR,
    },
    iconContainer: {
        flexDirection: 'row',
        width: Util.getWidth(50),
        justifyContent: 'space-evenly',
    },

    rightIcon: {
        height: 40,
        width: 40,
        alignItems: 'center',
        justifyContent: 'center',
    },
    heightWidthZero: {
        height: 0,
        width: 0,
    },
    filterView: {
        alignSelf: 'flex-end',
        marginRight: '8%',
        height: Util.getHeight(4),
        marginBottom: Util.getHeight(1),
        alignItems: 'center',
        flexDirection: 'row',
        // marginTop: '2%',
    },
    filterText: {
        color: Colors.EXTRACOLORS.ICONCOLOR,
        marginLeft: 5,
        fontFamily: Util.getFontFamily('regular'),
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: Util.normalize(11),
        lineHeight: 14,
    },
    countText: {
        flexWrap: 'wrap',
        color: Colors.EXTRACOLORS.APPTEXT,
        fontFamily: Util.getFontFamily('regular'),
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: Util.normalize(55),
        lineHeight: Util.normalize(65),
        //backgroundColor:'red'
    },
    countTextStyle: {
        flexWrap: 'wrap',
        fontWeight: 'bold',
        color: Colors.HEADERTITLE,
        fontSize: Util.normalize(14.8),
        fontFamily: Util.getFontFamily('regular'),
        fontStyle: 'normal',
        width: Util.getWidth(30),
        height: Util.normalize(38),
        textAlign: 'center',
        //backgroundColor:'green'
    },
    countTextSettings: {
        flexWrap: 'wrap',
        fontWeight: 'bold',
        color: Colors.APPTHEME,
        fontSize: Util.normalize(14.8),
        fontFamily: Util.getFontFamily('regular'),
        fontStyle: 'normal',
    },
    cardView: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardViewSettings: {
        marginHorizontal: '2%',
        marginBottom: 15,
        borderWidth: 1.5,
        borderRadius: 10,
        backgroundColor: Colors.EXTRACOLORS.CARDCOLOR,
        borderColor: Colors.APPTHEME,
        height: Platform.OS === 'android' ? Util.getHeight(24) : Util.getHeight(20),
        // flex: 1,
        width: Util.getWidth(42),
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: Colors.EXTRACOLORS.BORDER,
    },
    cardView2: {
        marginHorizontal: '2%',
        marginBottom: 15,
        borderWidth: 1.5,
        borderRadius: 10,
        backgroundColor: Colors.EXTRACOLORS.CARDCOLOR,
        borderColor: Colors.EXTRACOLORS.BORDER,
        height: Platform.OS === 'android' ? Util.getHeight(24) : Util.getHeight(20),
        // flex: 1,
        width: Util.getWidth(44),
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: Colors.EXTRACOLORS.BORDER,
    },
    contentStyles: {
        paddingHorizontal: '3%',
        paddingBottom: Util.getHeight(18),
        paddingTop: Util.getHeight(0.4),
    },
    bottomView: {
        height: Util.getHeight(11.5),
        width: '100%',
        paddingHorizontal: Util.getWidth(3),
        backgroundColor: Colors.AppTheme.AppBackground,
        flexDirection: 'row',
        borderTopWidth: 0.7,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderColor: Colors.MODELNUMBERBTNCOLOR,
        shadowColor: Colors.EXTRACOLORS.GREY,
        shadowOpacity: 0.7,
        shadowRadius: 3,
        shadowOffset: {
            height: 1,
            width: 1,
        },


    },
    iconView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    // badgeStyle1: {
    //   width: Util.getWidth(6.6),
    //   height: Util.getWidth(6.6),
    //   justifyContent: 'center',
    //   alignItems: 'center',
    //   // borderWidth: 0.6,

    //   // borderColor: Colors.TextColor.ErrorPrimary,
    //   // marginTop: '-50%',
    //   alignSelf: 'flex-end',
    //   // padding: 3,
    //   borderRadius: Util.getWidth(3.3),
    //   position: 'absolute',
    //   top: 0,
    //   right: 10,

    //   // overflow: 'visible',
    // },
    badgeStyle: {
        backgroundColor: Colors.TextColor.ErrorPrimary,
        width: Util.getWidth(6.6),
        height: Util.getWidth(6.6),
        justifyContent: 'center',
        alignItems: 'center',
        // borderWidth: 0.6,

        // borderColor: Colors.TextColor.ErrorPrimary,
        // marginTop: '-50%',
        alignSelf: 'flex-end',
        padding: 3,
        borderRadius: Util.getWidth(3.3),
        position: 'absolute',
        top: 0,
        right: 10,
        overflow: 'visible',
    },
    countBubble: {
        color: Colors.EXTRACOLORS.WHITE,
        fontSize: Util.normalize(10),
        fontWeight: 'bold',
        letterSpacing: 0.32,
        fontFamily: Util.getFontFamily('regular'),
    },

    parent: {
        flex: 1,
    },
    container: {
        marginHorizontal: 20,
        alignItems: 'center',
        flex: 1,
        paddingBottom: 25,
    },

    toolImgtext: {
        paddingVertical: 5,
        fontFamily: Util.getFontFamily('regular'),
        color: Colors.TextColor.LabelTextPrimary,
        fontSize: Util.normalize(14),
    },
    inputView: {
        borderColor: Colors.EXTRACOLORS.APPTEXT,
        elevation: 0.5,
        shadowColor: Colors.EXTRACOLORS.APPTEXT,
    },
    helpText: {
        fontFamily: Util.getFontFamily('regular'),
        color: 'green',
    },
    btnStyle: {
        height: 50,
        alignSelf: 'center',
        marginVertical: 25,
    },
    errorMsg: {
        paddingVertical: 5,
        fontFamily: Util.getFontFamily('regular'),
        color: Colors.THEME.ERROR,
        fontSize: 16 * 0.8,
        fontStyle: 'italic',
    },
    crossIcon: {
        position: 'absolute',
        right: 2,
        top: 2,
        backgroundColor: 'white',
    },

    listViewUnavailable: {
        maxHeight: Util.getHeight(70),
        minHeight: Util.getHeight(8),
        marginHorizontal: Util.getWidth(1),
        borderColor: Colors.EXTRACOLORS.DEARKBORDER,
        borderWidth: 0.5,
        paddingHorizontal: Util.getWidth(2),
        borderRadius: Util.getWidth(2),
        // marginHorizontal: 10,
    },
    width_100: {
        width: '100%',
    },
    mHorizontal15: {
        marginHorizontal: 15,
    },

    reportListHeader: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        alignItems: 'center',
        paddingHorizontal: '5%',
    },
    mTop: {
        marginTop: 30,
        marginHorizontal: 10,
    },
    horizontalScroll: {
        flexDirection: 'row',
        marginHorizontal: '5%',
        marginTop: '-8%', height: '50%',
        justifyContent: 'flex-start'
    },
    bottomContainer: {
        flex: 1, backgroundColor: 'white', marginTop: '15%'
    },
    fontStyle: {
        fontSize: Util.normalize(12),
        width: Util.getWidth(28),
        marginBottom: '-15%',
        fontWeight: 'bold'

    },
    bottomFont: {
        fontSize: Util.normalize(8),
        color: '#04AC22',
        fontWeight: 'bold',
        marginHorizontal: Util.getWidth(1.2)

    },
    fontStyle2: {
        fontSize: Util.normalize(12),
        width: Util.getWidth(28),
        color: Colors.EXTRACOLORS.WHITE,
        marginBottom: '-15%',
        fontWeight: 'bold'
    },
    iconBg2: {
        alignSelf: 'flex-start', marginLeft: '13%', marginTop: '15%', marginBottom: '5%',
        width: 40, height: 40, borderRadius: 20, backgroundColor: '#FF881B',
        justifyContent: 'center', alignItems: 'center'
    },
    iconBg3: {
        alignSelf: 'flex-start', marginLeft: '13%', marginTop: '15%', marginBottom: '5%',
        width: 40, height: 40, borderRadius: 20, backgroundColor: '#FE585C',
        opacity: 0.6,
        justifyContent: 'center', alignItems: 'center'
    },
    iconBg1: {
        alignSelf: 'flex-start', marginLeft: '13%', marginTop: '15%', marginBottom: '5%',
        width: 40, height: 40, borderRadius: 20, backgroundColor: 'white',
        justifyContent: 'center', alignItems: 'center'
    },
    iconStyle: {
        flex: 1,
        width: 20,
        height: 20,
        resizeMode: 'contain'
    }, recentActivity: {
        color: '#091850',
        fontSize: Util.normalize(13),
        fontWeight: 'bold',
        marginLeft: Util.getWidth(8)


    }, cover: {
        backgroundColor: "rgba(0,0,0,.5)",
    },
    sheet: {
        position: "absolute",
        top: Dimensions.get("window").height,
        left: 0,
        right: 0,
        height: "100%",
        width: '100%',
        justifyContent: "flex-end",
    },
    popup: {
        backgroundColor: "#FFF",
        marginHorizontal: 10,
        borderTopLeftRadius: 25,
        borderTopRightRadius: 25,
        alignItems: "center",
        justifyContent: "center",
        height: Util.getHeight(75),
        minHeight: 80,
    },
});
export default homeStyle;
