
import React, { Component } from "react";
import {
  StyleSheet, 
  Text, 
  TouchableOpacity, 
  View 
} from "react-native";

export default class Button extends Component {
  render({ onPress } = this.props) {
    return (
      <TouchableOpacity onPress={onPress}>
        <View style={styles.button}>
          <Text style={styles.text}>{this.props.text}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    borderRadius: 50,
    backgroundColor:"#04AC22", 
    paddingHorizontal: 60,
    paddingVertical: 12 
  },
  text: {
    color: "#FFFFFF",
  }
});