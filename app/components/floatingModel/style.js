// @ created by shivani lakhena
import { StyleSheet, Platform } from 'react-native';
import Util from '../../common/util';
import Colors from '../../values/colors';

const Styles = StyleSheet.create({
  textStyle: {
    fontSize: Util.normalize(18),
    color: Colors.EXTRACOLORS.ICONCOLOR,
  },
  horizontalView1: {

    height: '20%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginVertical:8
  },
  horizontalView2: {
    flexDirection: 'row', width: '80%', alignItems: 'center', justifyContent: 'space-evenly'

  },
  textFloatingButton: {
    textAlign: 'right', width: '65%', justifyContent: 'flex-start',
    fontSize:Util.normalize(13),
    color:'white',
    fontWeight:'bold'

  },
  textViewStyle: {
    padding: Util.getWidth(3),
    marginTop: Util.getHeight(2),
    marginLeft: Util.getWidth(2),
  },
  textViewStyleTwo: {
    padding: Util.getWidth(1),
    marginLeft: Util.getWidth(2),
  },
  textViewStyleThree: {
    padding: Util.getWidth(1),
  },
  floatingLabel: {
    height: 54, width: 54,
    borderRadius: 54 / 2,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    position: "absolute",
    bottom: Util.getHeight(13), right: 18
  },
  floatingLabe2: {
    height: 54, width: 54,
    borderRadius: 54 / 2,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    // right: 18
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.MODELBACKGROUDCOLOR,
    height: Util.getHeight(100),
    width: Util.getWidth(100),
  },
  innerContainerTransparentStyle: {
    width: Util.getWidth(82),
    backgroundColor: Colors.EXTRACOLORS.WHITE,
    borderRadius: Util.getWidth(4),
    padding: Util.getWidth(7),
  },
  btnView: {
    width: Util.getWidth(68),
    height: 50,
    marginVertical: Util.getHeight(1),
    borderWidth: 2,
    borderColor: Colors.AppTheme.ColorPrimary,
    backgroundColor: Colors.AppTheme.AppBackground,
  },
  btnSecond: {
    backgroundColor: Colors.AppTheme.ColorPrimary,
  },
  btnText: {
    color: Colors.AppTheme.ColorPrimary,
    fontSize: Util.normalize(15),
    fontWeight: 'bold',
    fontFamily: Util.getFontFamily('regular'),
  },
  headingView: {
    justifyContent: 'center',
    alignContent: 'center',
    marginVertical: Util.getHeight(1),
  },
  headingStyle: {
    alignSelf: 'center',
    textTransform: 'capitalize',
    color: Colors.EXTRACOLORS.ICONCOLOR,
    fontWeight: 'bold',
    fontSize: Util.normalize(16),
    fontFamily: Util.getFontFamily('regular'),
  },
  dateView: {
    marginVertical: Util.getHeight(3),
  },
  rangeBtnViewStyle: { flexDirection: 'row' },
  rangeBtn: {
    width: '45%',
    marginTop: Util.getHeight(2.5),
    backgroundColor: Colors.MODELNUMBERBTNCOLOR,
    borderWidth: 1.5,
    borderColor: Colors.AppTheme.ColorPrimary,
  },
  rangeBtnInactive: {
    width: '45%',
    marginTop: Util.getHeight(2.5),
    backgroundColor: Colors.MODELNUMBERBTNCOLOR,
  },
  rangeBtnInactiveText: {
    color: Colors.EXTRACOLORS.MUTED,
    fontWeight: 'bold',
    fontFamily: Util.getFontFamily('regular'),
  },
  rangeBtnText: {
    color: Colors.EXTRACOLORS.ICONCOLOR,
    fontWeight: 'bold',
    fontFamily: Util.getFontFamily('regular'),
  },
  arrowIconStyle: {
    alignSelf: 'center',
    marginTop: Util.getHeight(2.5),
    marginHorizontal: Util.getWidth(1),
  },
  datePickerModelStyle: {
    height: Util.getHeight(20),
    width: Util.getWidth(67),
  },
});
export default Styles;
