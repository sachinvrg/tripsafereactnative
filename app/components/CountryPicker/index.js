import React, { useContext, useState } from 'react';
import {
    View,
    TouchableOpacity,
    ScrollView,
    ImageBackground,
    TextInput,
    Image,
} from 'react-native';
import { SafeAreaView } from 'react-navigation';
import {
    Input,
    Icon,
    Text,
    Button,
    Checkbox,
} from 'galio-framework';

import CountryPicker from 'react-native-country-picker-modal'
import PhoneInput from 'react-native-phone-input'

// import App from './App';
import Styles from './styles';
import Colors from '../../values/colors';



export const CountryPickerComponent = (props) => {
    // const [countryCode, setCountryCode] = useState<CountryCode>('FR')
    const [countryCode, setCountryCode] = useState('IN')
    const [country, setCountry] = useState(null)
    const [withCountryNameButton, setWithCountryNameButton] = useState(
        false,
    )
    const [withFlag, setWithFlag] = useState(true)
    const [withEmoji, setWithEmoji] = useState(true)
    const [withFilter, setWithFilter] = useState(true)
    const [withAlphaFilter, setWithAlphaFilter] = useState(false)
    const [withCallingCode, setWithCallingCode] = useState(false)
    const onSelect = (country) => {
        setCountryCode(country.cca2)
        setCountry(country)
    }
    const { help, iconLeft, onChangeText, value, label, labelText, iconLeftShow, onFocus, onBlur, placeholder, secureTextEntry, extraStyle, ActiveTint } = props;
    return (
        <View>
            <View style={[Styles.formGroup]}>
                {label && <Text style={ActiveTint ? Styles.inputLableActive : Styles.inputLable}>{labelText}</Text>
                }
                <View style={ActiveTint ? Styles.inputViewActive : Styles.inputViewInActive}>
                    {iconLeftShow &&
                        <View style={Styles.inputRow}  >
                            <PhoneInput  />
                            {/* <CountryPicker
                                {...{
                                    countryCode,
                                    withFilter,
                                    withFlag,
                                    
                                    withCountryNameButton,
                                    // withAlphaFilter,
                                    withCallingCode,
                                    // withEmoji,
                                    onSelect,
                                }}
                                visible withFlag={true}
                            /> */}

                        </View>
                    }

                    <TextInput
                        onFocus={onFocus}
                        onBlur={onBlur}
                        style={Styles.textInput}
                        placeholder={placeholder}
                        maxLength={10}
                        keyboardType={'phone-pad'}
                        secureTextEntry={secureTextEntry}
                        onChangeText={onChangeText}

                        value={value}
                    />

                    <View style={Styles.rightIconView}  >

                        {/* <Image
        source={Images.email}
        placeholder="Email"
    /> */}
                    </View>

                </View>
                {help != null && help != '' && <Text style={Styles.helpText}>{help}</Text>}


            </View>
        </View>
    )
};

