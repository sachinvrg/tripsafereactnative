import NetInfo from '@react-native-community/netinfo';
import { BaseUrl } from '../network/Networksettings';
import RNFetchBlob from 'rn-fetch-blob';
import { Platform } from 'react-native';
import Toast from 'react-native-simple-toast';

const axios = require('axios');
export const useHttp = async (method, type, token, body) => {
  return new Promise(async function (resolve, reject) {

    NetInfo.fetch().then(state => {

      if (!state.isConnected) {

        Toast.show(
          'Please check your internet connection and try again!',
          Toast.LONG,
        );

      }

    });
  
    const url = BaseUrl + method;
    console.log('Api Request====>', url, type, body, `Bearer ${token}`);
    console.log('body===>', JSON.stringify(body));

    axios({
      method: type,
      url: url,
      headers: {
        'content-type': 'application/json',
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
      data: JSON.stringify(body),
    })
      .then(response => {

        console.log('Api Response====>', response);
        return resolve(response);
      })
      .catch(err => {
        console.log('in catch123567', err);
        return reject(err.response.data);
      });
  });
};
// let call;
const makeRequestCreator = () => {
  let call;
  return (url, headers) => {
    if (call) {
      call.cancel();
    }
    console.log('First request canceled cancel', url, headers);
    call = axios.CancelToken.source();
    return axios.get(url, headers, {
      cancelToken: call.token,
    });
  };
};

const get = makeRequestCreator();

export const useHttpGet = async (method, token) => {
  return new Promise(async function (resolve, reject) {
    var connectionInfo = await NetInfo.getConnectionInfo();
    if (connectionInfo.type == 'none' || connectionInfo.type == undefined) {
      Toast.show(
        'Please check your internet connection and try again!',
        Toast.LONG,
      );
      return reject('No network');
    }
    const url = BaseUrl + method;
    try {
      const res = await get(url, token);
      return resolve(res);
    } catch (err) {
      console.log('in catch123', err);
      if (axios.isCancel(err)) {
      }
    }
  });
};


export const useHttpFormDataMethod = async (method, type, token, body) => {
  return new Promise(async function (resolve, reject) {
    NetInfo.fetch().then(state => {

      if (!state.isConnected) {

        Toast.show(
          'Please check your internet connection and try again!',
          Toast.LONG,
        );
        return reject('No network');


      }

    });

    const url = BaseUrl + method;

    console.log('Api Request====>', url, type, body, `Bearer ${token}`);
    console.log('body===>', JSON.stringify(body));



    await fetch(
      url,
      {
        method: type,
        body: body,
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'multipart/form-data; ',
        },
      },
    )
      .then((res) => res.json())
      .then((response) => {
        console.log('response====>', response);

        return resolve(response);
      })
      .catch((error) => {
        console.log('error=====>', error);
        return reject(err);
      });
  });
};




export const useHttpForm = async (method, type, token, body) => {
  return new Promise(async function (resolve, reject) {

    // var connectionInfo = await NetInfo.getConnectionInfo();
    // if (connectionInfo.type == 'none' || connectionInfo.type == undefined) {
    //   Toast.show(
    //     'Please check your internet connection and try again!',
    //     Toast.LONG,
    //   );
    //   return reject('No network');
    // }
    const url = BaseUrl + method;
    console.log('api called url', JSON.stringify(body));
    console.log('baseurl link 123456', url, type, body, `Bearer ${token}`, {
      'content-type': 'multipart/form-data',
      Authorization: `Bearer ${token}`,
    });
    axios({
      method: type,
      url: url,
      headers: {
        Accept: 'application/json',
        // 'content-type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      data: body,
    })
      .then(response => {
        console.log('response api', response);
        // if (response.status === 200) {
        //   return resolve(response);
        // } else if (response.status === 201) {
        //   return resolve(response);
        // }
        throw Error(response);
      })
      .catch(err => {
        console.log('Err==123==>', err.response);
        console.log('in catch12345', JSON.stringify(err));
        // return reject(err.response.data.meta);
      });
  });
};
export const useHttpFile = async (method, type, token, name, ext) => {
  return new Promise(async function (resolve, reject) {
    // var connectionInfo = await NetInfo.getConnectionInfo();
    // if (connectionInfo.type == 'none' || connectionInfo.type == undefined) {
    //   Toast.show(
    //     'Please check your internet connection and try again!',
    //     Toast.LONG,
    //   );
    //   return reject('No network');
    // }
    const url = BaseUrl + method;
    console.log('api called url', url);
    console.log('baseurl link', url, type, name, `Bearer ${token}`);

    const { config, fs } = RNFetchBlob;


    const { dirs: { DownloadDir, DocumentDir } } = RNFetchBlob.fs;
    const aPath = Platform.select({ ios: DocumentDir, android: DownloadDir });
    const fPath = `${aPath}/Report${new Date().getTime()}${ext}`;
    const configOptions = Platform.select({
      ios: {
        fileCache: true,
        path: fPath,
        appendExt: ext,
      },
      android: {
        fileCache: false,
        appendExt: ext,
        addAndroidDownloads: {
          useDownloadManager: true, //uses the device's native download manager.
          notification: true,
          title: `${name} file report`, // Title of download notification.
          path: DownloadDir + `/${name}Report${new Date().getTime()}${ext}`, // this is the path where your download file will be in
          description: 'Downloading file.',
        },
      },
    });

    // let options = {
    //   fileCache: true,
    //   // appendExt : extension, //Adds Extension only during the download, optional
    //   addAndroidDownloads: {
    //     useDownloadManager: true, //uses the device's native download manager.
    //     notification: true,
    //     title: `${name} file report`, // Title of download notification.
    //     path: DownloadDir + `/${name}Report${new Date().getTime()}${ext}`, // this is the path where your download file will be in
    //     description: 'Downloading file.',
    //   },
    // };

    config(configOptions)
      .fetch(type, url, {
        Authorization: `Bearer ${token}`,
      })
      .then(response => {
        console.log('response in', response.path());
        return resolve(response);
      })
      .catch(err => {
        console.log(' response in catch123', err);
        return reject(err);
      });
  });
};
export const useHttpFileCsv = async (method, type, token, name, ext, bodyData) => {
  return new Promise(async function (resolve, reject) {
    var connectionInfo = await NetInfo.getConnectionInfo();
    if (connectionInfo.type == 'none' || connectionInfo.type == undefined) {
      Toast.show(
        'Please check your internet connection and try again!',
        Toast.LONG,
      );
      return reject('No network');
    }
    const url = BaseUrl + method;
    console.log('api called url', url);
    console.log('baseurl link', url, type, `/${name}Report${new Date().getTime()}${ext}`, `Bearer ${token}`, bodyData);

    const { config, fs } = RNFetchBlob;
    const android = RNFetchBlob.android
    let DownloadDir = fs.dirs.DownloadDir; // this is the Downloads directory.
    RNFetchBlob
      .config({
        fileCache: true,
        path: DownloadDir + `/${name}${new Date().getTime()}${ext}`,
      }).
      fetch(type, url, {
        Authorization: `Bearer ${token}`,
        'content-type': 'multipart/form-data',
      },
        bodyData
      )
      .then(response => {
        console.log('response in');
        return resolve(response);
      })
      .catch(err => {
        console.log(' response in catch123', err);
        return reject(err);
      });
  });
};

export const HttpCallWithNonRepeatation = async (method, type, token, body) => {
  return new Promise(async function (resolve, reject) {
    const auth = token === null ? '' : `Bearer ${token}`;
    let url = BaseUrl + method;
    try {
      const res = await get(url, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: auth,
        },
      });
      console.log('Api Response====>', res);
      return resolve(res);
    } catch (err) {
      console.log('Error====>', err.message);
      if (axios.isCancel(err)) {


      }
      return reject(err);
    }
  });
};
