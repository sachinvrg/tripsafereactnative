/** Created By Jay Soni **/
import React, { useEffect, useContext, useState } from 'react';
import { View, Platform, TouchableOpacity } from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
// import fs from 'react-native-fs';
import RNFetchBlob from 'rn-fetch-blob';
import { decode } from 'base64-arraybuffer';

// Hooks, Api component calling
import AppContex from '../../hooks/app-context';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-simple-toast';


// Global helper classes

// Local styles
import Styles from './styles';
import { useHttp, useHttpForm, useHttpFormDataMethod } from '../../hooks/useHttpss';
import { uploadUserDetails, BaseUrl } from '../../network/Networksettings';
import { POST, USERDETAILS } from '../../hooks/constants';
import helper from '../../common/helper';

const ImagePickerView = props => {
  const { navigation, style, imagePath, fromCamera } = props;
  const context = useContext(AppContex);
  const { state } = context;
  const [loader, setLoader] = useState(false);

  const uploadImageFromExplorer = () => {
    const { _onChangeVal } = props;
    console.log('ImagePicker', ImagePicker);

    ImagePicker.openPicker({
      cropping: true,
      mediaType: 'photo',
      multiple: false,
    }).then(images => {
      console.log('images===>', images.path);
      uploadImageApi(images);
    });
  };

  const uploadImageFromCamera = () => {
    const { _onChangeVal } = props;
    console.log('ImagePicker', ImagePicker);

    ImagePicker.openCamera({
      cropping: true,
      multiple: false,
    }).then(images => {
      console.log('images===>', state, images.path);


      // uploadImageApi(images);
    });
  };

  const uploadImageApi = async (images) => {
    const { _onChangeVal } = props;

    let filePath = '';
    if (Platform.OS === 'ios') {
      filePath = images.path.replace('file:', '');
    } else {
      filePath = images.path; setLoader(false);

    }
    setLoader(true);
    const dataform = new FormData();
      dataform.append('first_name', state.UserDetails.first_name),
      dataform.append('last_name', state.UserDetails.last_name),
      dataform.append('email', state.UserDetails.email),
      dataform.append('phone', state.UserDetails.phone),

      dataform.append('avatar', {
        uri: filePath,
        type: 'image/jpg',
        name: 'image.jpg',
      });


    setLoader(true);
    await useHttpFormDataMethod(uploadUserDetails, POST, state.UserDetails.authorization, dataform)
      .then(res => {
        setLoader(false);
        console.log("response", res);
        let userDataObject = res.data;
        let tempUser = { ...state.UserDetails, ...userDataObject };
        context.dispatch({
          type: USERDETAILS,
          payload: tempUser,
        });
        helper.store_item_async(USERDETAILS, JSON.stringify(tempUser));
        setLoader(false);


        Toast.show(
          'Updated successfully',
          Toast.LONG,
        );


      })
      .catch(err => {
        console.log('error', err);

        setLoader(false);



      });


  };

  return (
    <View>
      <Spinner visible={loader} />
      <TouchableOpacity
        style={style}
        onPress={() =>
          fromCamera ? uploadImageFromCamera() : uploadImageFromExplorer()
        }>
        {props.children}
      </TouchableOpacity>

    </View>

  );
};

export default ImagePickerView;
