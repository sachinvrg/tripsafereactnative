import React, { useContext } from 'react';
import {
    View,
    TouchableOpacity,
    ScrollView,
    ImageBackground,
    TextInput,
    Image,
} from 'react-native';
import { SafeAreaView } from 'react-navigation';
import {
    Input,
    Icon,
    Text,
    Button,
    Checkbox,
} from 'galio-framework';

import Styles from './styles';
import Colors from '../../values/colors';
import styles from './styles';
import images from '../../assets/Images/imagePath/images';


export const ProfileOptionVew = (props) => {
    const { onClick, iconLeft, iconRightShow, optionText, } = props;
    return (
        <TouchableOpacity onPress={onClick} style={styles.optionsRow}>
            <View style={styles.leftView}>
                <Image source={iconLeft} />


            </View>
            <View style={optionText === 'Logout' ? styles.rightViewLogout : styles.rightView}>
                <Text style={optionText === 'Logout' ? styles.textStyleLogout : styles.textStyle}>
                    {optionText}
                </Text>

                {iconRightShow && <Image source={images.arrowRight} />}
            </View>
        </TouchableOpacity>
    )
};

