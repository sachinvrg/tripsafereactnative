import React, { useContext } from 'react';
import {
  View,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Image,
} from 'react-native';
import { SafeAreaView } from 'react-navigation';
import {
  Input,
  Icon,
  Text,
  Button,
  Checkbox,
} from 'galio-framework';
// import { Image } from 'react-native-elements';


// Hooks, Api component calling

// Global helper classes
import GlobalStyles from '../../assets/css/globalStyle';

// Local styles
import strings from '../../values/stringEn';
import Colors from '../../values/colors';
import styles from './styles';
// import images from '../../assets/images';
import helper from '../../common/helper';
import Util from '../../common/util';
import { ROUTES_KEY } from '../../hooks/constants';
import appContext from '../../hooks/app-context';


export const TabWithButton = props => {
  const {
    isLeftView,
    leftLabelText,
    rightLabelText,
    onLeftClick,
    onRightClick,
    onMiddleClick,
    isMiddleView,
    isRightView,
    middleLabelText,
    onFilterClick,
    extraStyles,
    filterVisible,
  } = props;
  return (
    <View style={styles.innerTab}>
      <View style={styles.tab}>
        <TouchableOpacity onPress={onLeftClick}>
          <Text style={isLeftView ? styles.tabText : styles.tabTextDisabled}>
            {leftLabelText}
          </Text>
          {isLeftView && <View style={styles.tabBorder} />}
        </TouchableOpacity>

        <TouchableOpacity onPress={onMiddleClick}>
          <Text style={isMiddleView ? styles.tabText : styles.tabTextDisabled}>
            {middleLabelText}
          </Text>
          {isMiddleView && <View style={styles.tabBorder} />}
        </TouchableOpacity>

        <TouchableOpacity onPress={onRightClick}>
          <Text style={isRightView ? styles.tabText : styles.tabTextDisabled}>
            {rightLabelText}
          </Text>
          {isRightView && <View style={styles.tabBorder} />}
        </TouchableOpacity>
      </View>

    </View>
  );

};
