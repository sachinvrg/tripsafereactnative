import { StyleSheet } from 'react-native';
import Util from '../../common/util';
import Colors from '../../values/colors';

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  splashLogo: {
    height: Util.getHeight(30),
    width: Util.getWidth(50),
  },
  container: {
    flex: 1,
    resizeMode: 'cover',
    width: '100%'
    // alignSelf: 'baseline'
  },
  formGroup: {
    paddingBottom: Util.getHeight(1.5),
    // backgroundColor:'red',
    width: Util.getWidth(78)
  },
  inputLable: {
    marginTop: '1%',
    marginBottom: '1%',
    textAlign: 'left',
    alignSelf: 'flex-start',
    marginLeft: '2%',
    color: Colors.TextColor.SecondaryTextColor,
    fontSize: Util.normalize(9),
    fontWeight: 'bold'
  },
  inputLableActive: {
    marginTop: '1%',
    marginBottom: '1%',
    textAlign: 'left',
    alignSelf: 'flex-start',
    marginLeft: '2%',
    color: Colors.EXTRACOLORS.ActiveColor,
    fontSize: Util.normalize(9),
    fontWeight: 'bold'
  },
  textInput: {
    flex: 0.8, height: Util.getHeight(7),color:Colors.NavyBlue
  },
  helpText: {
    fontSize: Util.normalize(9),
    color: Colors.THEME.ERROR,
    marginLeft: '2%',
    marginBottom: '-2.5%'

  },
  inputViewInActive: {
    borderWidth: 1,
    width: Util.getWidth(78),
    height: Util.getHeight(7),
    borderRadius: 10,
    flexDirection: 'row',
    borderColor: 'lightgrey',
  },
  inputViewActive: {
    borderWidth: 1,
    width: Util.getWidth(78),
    height: Util.getHeight(7),
    borderRadius: 10,
    flexDirection: 'row',
    borderColor: Colors.EXTRACOLORS.ActiveColor,
  },


  inputRow: {
    justifyContent: 'center', alignContent: 'center', paddingLeft: 12,
    flex: 0.1,
    height: Util.getHeight(7),
  },
  rightIconView: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    alignContent: 'center',
    // paddingLeft: 12,
    flex: 0.2,
    height: Util.getHeight(7),
  },
  input: {
    width: Util.getWidth(75),
    height: Util.getHeight(6.5),
    backgroundColor: Colors.inputBackground,
    borderRadius: 2.5
  },
  datePicker: {
    width: Util.getWidth(75),
    height: Util.getHeight(6.5),
    backgroundColor: Colors.inputBackground,
    borderRadius: 2.5,
    position: 'absolute',
    top: 10,
    opacity: 0
  },
  calender: {
    height: 20,
    width: 20
  }
});
export default styles;
