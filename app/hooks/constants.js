export const USERDETAILS = 'USERDETAILS';
export const CREDENTIALS = 'CREDENTIALS';
export const ISLOADING = 'ISLOADING';
export const CARTLIST = 'CARTLIST';
export const LOGOUT = 'LOGOUT';
export const POST = 'POST';
export const PUT = 'PUT';
export const PATCH = 'PATCH';
export const GET = 'GET';
export const THEMEKEY = 1;
export const DELETE = 'DELETE';
export const ISDEV = true;
export const ACTIVATE_USER = 1;
export const DEACTIVATE_USER = -1;
export const DELETE_USER = 99;
export const SCREENINDEX = 'SCREENINDEX';

export const USER_TYPE = 'USER_TYPE';

export const ROUTES_KEY = 'ROUTES_KEY';
export const TRANSFER_TYPE_INCOMING = 1;
export const TRANSFER_TYPE_OUTGOING = 0;
