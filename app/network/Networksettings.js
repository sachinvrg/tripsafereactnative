export const BaseUrl = 'http://ec2-13-233-175-234.ap-south-1.compute.amazonaws.com:8000/api';


export const signIn = '/login';

export const signUp = '/register'
export const logOut = '/logout'
export const forgotPassword = '/password/forgot';

export const uploadUserDetails = '/profile/personal';

export const GetUserDetails = '/profile';

export const MasterData = '/user/master';
