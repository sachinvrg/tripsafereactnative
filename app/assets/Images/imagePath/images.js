const images = {
  splashBg: require("../splash.png"),
  splashBgNew: require("../splashBg.png"),

  AppLogo: require("../LOGO.png"),


  appLogo: require("../logo.png"),
  logoV: require("../logo_v.png"),
  loginHeader: require("../loginHeader.png"),
  lock: require("../icon-lock.png"),
  email: require("../icon-email.png"),
  bell: require("../bell.png"),
  dropDown: require("../angle-up.png"),

  backIcon: require("../Back.png"),
  calender: require("../Fill.png"),
  address: require("../address.png"),


  plusMore: require("../plus_more.png"),
  settings: require("../Authorisation.png"),
  sendLocation: require("../Finddoctor.png"),

  firstAdd: require("../firstAid.png"),
  notePad: require("../passportIds.png"),

  cross: require("../cross_2.png"),
  arrowRight: require("../collapsed.png"),

  personalDetails: require("../personalDetails.png"),
  healthCertificates: require("../healthCertificates.png"),

  passport_ids2: require("../passport_ids2.png"),
  Authorisation2: require("../Authorisation2.png"),
  help_support: require("../help_support.png"),
  healthCertificates: require("../healthCertificates.png"),
  logout: require("../logout.png"),


  scan: require("../scan.png"),
  AddTest: require("../AddTest.png"),
  checkIn: require("../checkIn.png"),
  AddTravelDoc: require("../AddTravelDoc.png"),


  QuickCheckIn: require("../QuickCheckIn.png"),


  ic: require("../qr.png"),
  profileImage: require("../Ellipse575.png"),
  checked: require("../ic_radio_active.png"),
  camera: require("../camera2.png"),



  user: require("../user.png"),
  home: require("../home.png"),
  profile: require("../profile.png"),
  blackjack: require("../blackjack.png"),
  activeGame: require("../activeGame.png"),
  homeGrid1: require("../home_grid_1.png"),
  homeGrid2: require("../home_grid_2.png"),
  homeGrid3: require("../home_grid_3.png"),
  menuIcon: require("../menu-icon.png"),
  notification: require("../notification.png"),
  Bg: require("../blue_back.png"),
  LayerBottom: require("../bg_bottom.png"),
  LayerMid: require("../bg_mid.png"),
  LayerTop: require("../bg_top.png"),

  iconFb: require("../icon-facebook.png"),
  iconGoogle: require("../icon-google.png"),
  iconApple: require("../icon-apple.png"),

  dot: require("../inActiveDot.png"),
  dotActive: require("../activeDot.png"),


  invite: require("../menu/invite.png"),
  activityIcon: require("../menu/activityIcon.png"),
  cardIcon: require("../menu/cardIcon.png"),
  clockIcon: require("../menu/clockIcon.png"),
  crossIcon: require("../cross.png"),
  forgotPassword: require("../forgot_password.png"),

  contactIcon: require("../menu/contactIcon.png"),
  gamesIcon: require("../menu/gamesIcon.png"),
  settingsIcon: require("../menu/settingsIcon.png"),
  shareIcon: require("../menu/shareIcon.png"),
  onboarding1: require("../onboarding1.png"),
  onboarding2: require("../onboarding2.png"),
  onboarding3: require("../onboarding3.png"),



  demo: require("../demo/demo.png"),
  userImage: require("../demo/user.png"),


  noImage: require("../no-image.png"),
  coinYellow: require("../coin_yellow.png"),

  BottomBar: {
    dashboradActive: require('../home_active.png'),
    recentTestActive: require('../Records_active.png'),
    placesActive: require('../places_active.png'),
    myAccountActive: require('../Account_active.png'),

    dashboardeInactive: require('../home.png'),
    recentTestInactive: require('../Records.png'),
    placesInactive: require('../places.png'),
    myAccountInactive: require('../Account.png'),
  }


};

export default images;
