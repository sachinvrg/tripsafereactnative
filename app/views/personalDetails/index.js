
import React, { useContext, useEffect, useState } from 'react';
import { View, Image, FlatList, ScrollView, StatusBar } from 'react-native';
import { Text } from 'galio-framework';

// Hooks, Api component calling
import AppContext from '../../hooks/app-context';
import { useHttp, useHttpFormDataMethod } from '../../hooks/useHttpss';
import {
    GET, POST, USERDETAILS,

} from '../../hooks/constants';


import strings from '../../values/stringEn';
import moment from 'moment';

// Local styles
import styles from './styles';
import Colors from '../../values/colors';
import Util from '../../common/util';
import helper from '../../common/helper';
import { TouchableOpacity } from 'react-native-gesture-handler';
import HeaderComponent from '../../components/HeaderComponent';
import LinearGradient from 'react-native-linear-gradient';
import images from '../../assets/Images/imagePath/images';
import { TabWithButton } from '../../components/ExportedComponents';
import { TextInputComponent } from '../../components/TextInput';
import { AppDatePicker } from '../../components/DatePicker';
import { PickerDropDown } from '../../components/PickerDropDown';
import RNPickerSelect from 'react-native-picker-select';
var mailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { CountryPickerComponent } from '../../components/CountryPicker';
import { ButtonComponent } from '../../components/Button';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-simple-toast';

import { uploadUserDetails } from '../../network/Networksettings';

const PersonalDetailsScreen = props => {
    const { navigation } = props;
    const context = useContext(AppContext);
    const [emailCorrect, setEmailCorrect] = useState(false);

    const [emailCorrectTab3, setEmailCorrectTab3] = useState(false);

    const [loader, setLoader] = useState(false);

    const { state } = context;
    const [tabOne, setTabOne] = useState(true);
    const [tabTwo, setTabTwo] = useState(false);
    const [tabThree, setTabThree] = useState(false);
    const [isFocusedDOB, setIsFocusedDOB] = useState(false);
    const [formData, setFormData] = useState({

        dob: {
            err: false,
            errMsg: '',
            propname: 'dob',
            value: '',
            required: true,
        },

       sex:{
        err: false,
        errMsg: '',
        propname: 'sex',

        value: '',
        required: true,
       
      
       },  
       email: {
        err: false,
        errMsg: '',
        propname: 'email',
        value: '',
        required: true,
        checkMobile: true,
    },
    phone: {
        err: false,
        errMsg: '',
        propname: 'phone',
        value: '',
        required: true,
        checkMobile: true,
    },
    address: {
        err: false,
        errMsg: '',
        propname: 'address',
        value: '',
        required: true,
        checkMobile: true,
    },
    });

    const [formDataTab3, setFormDataTab3] = useState({

        bloodgroup: {
            err: false,
            errMsg: '',
            propname: 'blood',
            value: '',
            required: true,
        },

      
    
    travelInsurance: {
        err: false,
        errMsg: '',
        propname: 'travelInsurance',
        value: '',
        required: true,
        checkMobile: true,
    },

    doctorName: {
        err: false,
        errMsg: '',
        propname: 'doctorName',
        value: '',
        required: true,
        checkMobile: true,
    },
    email: {
        err: false,
        errMsg: '',
        propname: 'email',
        value: '',
        required: true,
        checkMobile: true,
    },
    phone: {
        err: false,
        errMsg: '',
        propname: 'phone',
        value: '',
        required: true,
        checkMobile: true,
    },
   
    
    });

    useEffect(() => {
setLoader(true);
        helper
        .fetch_item_async(USERDETAILS)
        .then(data => {
            const parsedData = JSON.parse(data);
            console.log("parsed data",parsedData);
            
            if (parsedData) {
                // setFormData(parsedData);

                console.log("formData effect",formData);
                Object.entries(parsedData).map(item => {
                    console.log('MapedUserDetails', item);
                    formData.hasOwnProperty(item[0]) &&
                      (formData[item[0]].value = `${item[1] !== 'null' && item[1] !== null ? item[1] : ''}`);
                  });
                  setLoader(false);

            }
            setLoader(false);

        })
        .catch(() => { });
    }, []);

    const isTabOne = () => {
        setTabOne(true)
        setTabTwo(false)
        setTabThree(false)

    }
    const isTabTwo = () => {
        setTabOne(false)
        setTabThree(false)

        setTabTwo(true)

    }

    const isTabThree = () => {
        setTabOne(false)
        setTabThree(true)
        setTabTwo(false)

    }

   
    const changeText = (name, value) => {
      console.log('SELECTED GENDER',value);
      if (name === 'email') {

        if (value.match(mailformat)) {
            setEmailCorrect(true);
        } else {
            setEmailCorrect(false);
        }
    }
        setFormData({
            ...formData,
            [name]: {
                ...formData[name],
                err: false,
                errMsg: '',
                value,
                propname: name,
            },
        });
    };

    const changeTextTab3 = (name, value) => {
        console.log('SELECTED GENDER',value);
        if (name === 'email') {
  
          if (value.match(mailformat)) {
              setEmailCorrectTab3(true);
          } else {
              setEmailCorrectTab3(false);
          }
      }
          setFormDataTab3({
              ...formDataTab3,
              [name]: {
                  ...formDataTab3[name],
                  err: false,
                  errMsg: '',
                  value,
                  propname: name,
              },
          });
      };


    const onSaveClick = () => {
        setLoader(true);
        helper.validateLoginForm(formData).then(({ error, formDataObj }) => {
            if (!error) {
               
                const saveDetailsRequest = helper.processData(formData);
                console.log('saveDetailsRequest',saveDetailsRequest);
                
                updateDetails(saveDetailsRequest);
            } else {
                setFormData(formDataObj);
                setTimeout(() => {
                    setLoader(false);
                }, 100);
            }
        });
    };

    const updateDetails = async body => {
        // body.email = body.email.toLowerCase();

        const dataform = new FormData();
        dataform.append('first_name', state.UserDetails.first_name),
        dataform.append('last_name', state.UserDetails.last_name),
        dataform.append('email', body.email.toLowerCase()),
        dataform.append('phone', body.phone),
        dataform.append('dob', body.dob),
        dataform.append('sex', body.sex),
        dataform.append('address', body.address),

  
        setLoader(true);
        await useHttpFormDataMethod(uploadUserDetails, POST, state.UserDetails.authorization, dataform)
            .then(res => {
                setLoader(false);
                console.log("response", res);

                let userDataObject = res.data;
                let tempUser = { ...state.UserDetails, ...userDataObject };
                context.dispatch({
                  type: USERDETAILS,
                  payload: tempUser,
                });
                helper.store_item_async(USERDETAILS, JSON.stringify(tempUser));
              
                Toast.show(
                    'Updated successfully',
                    Toast.LONG,
                  );

            })
            .catch(err => {
                console.log('error', err);

                setLoader(false);

            });
    };

    return (
        <LinearGradient
            colors={['#113CB4', '#091850']}

            style={styles.linearGradient}
        >
            <StatusBar barStyle='light-content' hidden={false} backgroundColor="#113CB4" />
            <Spinner visible={loader} />

            <HeaderComponent
                iconLeft={true}
                navigateBack={() => navigation.goBack()}
                title="Personal Details"


            />
            <View style={styles.viewContainer}>

                <TabWithButton


                    leftLabelText={'Personal'}
                    middleLabelText={'Next of Kin'}
                    onLeftClick={isTabOne}
                    rightLabelText={'Health Details'}
                    isLeftView={tabOne}
                    isMiddleView={tabTwo}
                    isRightView={tabThree}

                    onMiddleClick={isTabTwo}
                    onRightClick={isTabThree}


                />
                    <View style={styles.TabContainer}>
                    <KeyboardAwareScrollView
                    extraScrollHeight={10}
                    showsVerticalScrollIndicator={false}
                    enableOnAndroid={true}
                    scrollEnabled
                    keyboardShouldPersistTaps={'always'}>
                        <ScrollView   showsVerticalScrollIndicator={false}>

                    {tabOne && 
                    <View style={styles.tabViewOne}>


                               <AppDatePicker
                                    label
                                    iconLeftShow={true}
                                    labelText={strings.birthday}                                   
                                    iconLeft={images.calender}
                                    placeholder={strings.dateOfBirth}
                                    setSelectedDate={text => changeText('dob', text)}
                                    help={formData.dob.errMsg}
                                    value={formData.dob.value !="" && formData.dob.value !=null ? moment(formData.dob.value).format('LL'):formData.dob.value}
                                    selectedDate={formData.dob.value !="" && formData.dob.value !=null ? moment(formData.dob.value).format('LL'):formData.dob.value}


                                />



                            <PickerDropDown
                                label
                                labelText={"Biological Sex"}
                                style={styles.inputView}
                                propname={'sex'}
                                onChangeText={(propname, text) => changeText(propname, text)}
                                maxLength={40}
                                color={Colors.EXTRACOLORS.DARKBLUE}
                 
                                help={formData.sex.errMsg}
                                options={gender}
                                placeholder={{label: 'Select Gender'}}
                                value={formData.sex.value}
                             selectedValue={formData.sex.value}
                            />

                           <TextInputComponent
                                label
                                labelText={"Email"}
                                placeholder={strings.enterEmail}
                                iconLeftShow={false}
                                iconRight={images.checked}
                                iconRightShow={emailCorrect ? true : false}
                                onChangeText={text => changeText('email', text)}
                                help={formData.email.errMsg}
                                value={formData.email.value}

                            />

                                <CountryPickerComponent
                                    // iconLeftShow={true}
                                    label
                                    labelText={strings.contactNumber}
                                   
                                    placeholder={strings.enterMobileNumber}
                                    onChangeText={text => changeText('phone', text)}
                                    help={formData.phone.errMsg}
                                    value={formData.phone.value}
                                />
                                <TextInputComponent
                                    iconLeftShow={true}
                                    label
                                    labelText={strings.address}
                                    
                                    placeholder={strings.enterLocation}
                                    onChangeText={text => changeText('address', text)}
                                    help={formData.address.errMsg}
                                    iconLeft={images.address}
                                    value={formData.address.value}

                                />

                            <View style={{ position:'absolute',bottom:0 }}>
                                <ButtonComponent
                                    onClick={() => onSaveClick()}
                                    buttonText={strings.save} />
                            </View>
                                
                    </View>
                   }
                    {tabTwo && <Text>
                     tab2 under working
                    
                    
                    </Text>}
                    {tabThree && 
                      <View style={styles.tabViewThree}>


                     <Text style={styles.textParegraph}>
                         {strings.paregraph}
                          
                     </Text>


                   
                  <TextInputComponent
                       label
                       labelText={"Blood Group"}
                       placeholder={strings.enterBloodGroup}
                       iconLeftShow={false}
                       onChangeText={text => changeTextTab3('bloodgroup', text)}
                       help={formDataTab3.bloodgroup.errMsg}
                       value={formDataTab3.bloodgroup.value}

                   />
                   <TextInputComponent
                           label
                           labelText={'Travel Insurance Number'}
                           
                           placeholder={strings.enterTravelInsurance}
                           onChangeText={text => changeText('address', text)}
                           help={formDataTab3.travelInsurance.errMsg}
                           value={formDataTab3.travelInsurance.value}

                       />


                           <PickerDropDown
                                label
                                labelText={"Biological Sex"}
                                style={styles.inputView}
                                propname={'sex'}
                                onChangeText={(propname, text) => changeText(propname, text)}
                                maxLength={40}
                                color={Colors.EXTRACOLORS.DARKBLUE}
                 
                                help={formData.sex.errMsg}
                                options={gender}
                                placeholder={{label: 'Select Gender'}}
                                value={formData.sex.value}
                             selectedValue={formData.sex.value}
                            />

                            <PickerDropDown
                                label
                                labelText={"Biological Sex"}
                                style={styles.inputView}
                                propname={'sex'}
                                onChangeText={(propname, text) => changeText(propname, text)}
                                maxLength={40}
                                color={Colors.EXTRACOLORS.DARKBLUE}
                 
                                help={formData.sex.errMsg}
                                options={gender}
                                placeholder={{label: 'Select Gender'}}
                                value={formData.sex.value}
                             selectedValue={formData.sex.value}
                            /> 

                       <Text style={{alignSelf:'flex-start',fontWeight:'bold',marginVertical:10}} >
                       {'Personal Doctor Details'}
                       </Text>

                       <TextInputComponent
                                label
                                labelText={"Doctor Name"}
                                placeholder={strings.enterDoctorName}
                                iconLeftShow={false}
                                onChangeText={text => changeTextTab3('doctorName', text)}
                                help={formDataTab3.doctorName.errMsg}
                                value={formDataTab3.doctorName.value}

                            />

                       <TextInputComponent
                                label
                                labelText={"Email"}
                                placeholder={strings.enterEmail}
                                iconLeftShow={false}
                                iconRight={images.checked}
                                iconRightShow={emailCorrectTab3 ? true : false}
                                onChangeText={text => changeTextTab3('email', text)}
                                help={formDataTab3.email.errMsg}
                                value={formDataTab3.email.value}

                            />
                            <CountryPickerComponent
                                    // iconLeftShow={true}
                                    label
                                    labelText={strings.contactNumber}
                                   
                                    placeholder={strings.enterMobileNumber}
                                    onChangeText={text => changeTextTab3('phone', text)}
                                    help={formDataTab3.phone.errMsg}
                                    value={formDataTab3.phone.value}
                                />

                       

                   <View style={{ position:'absolute',bottom:10 }}>
                       <ButtonComponent
                           onClick={() => onSaveClick()}
                           buttonText={strings.save} />
                   </View>
                       
           </View>




                    }
                    </ScrollView>
                    </KeyboardAwareScrollView>
                    </View>



            </View>

        </LinearGradient >
    );
};

export default PersonalDetailsScreen;
const gender = [
    {label: 'Male', value: 'male'},
    {label: 'Female', value: 'female'},
    {label: 'Other', value: 'other'},

  ];