import React, { useEffect, useContext } from 'react';
import { Platform, StyleSheet, StatusBar, Text, View } from "react-native";
import AppContex from '../../hooks/app-context';

import Screen from "./Screen";
import { FixScreenWithoutPadding } from "../../components/WrapperView/FixScreenWithoutPadding";

const Home = props => {
  const { navigation } = props;

  const context = useContext(AppContex);
  const { state } = context;
  useEffect(() => {
  

  }, []);


  return (
    <Screen navigation={navigation} />

  );
};

export default Home;


// export default class Home extends Component {
//   static navigationOptions = {
//     headerStyle: {
//       backgroundColor: "#091850",
//       elevation: null
//     },
//     header: null
//   };

//   screenView = () => {
//     return (
//       <Screen navigation={this.props.navigation} />
//     )
//   }
//   render() {
//     return (
//       // <View style={styles.container}>
//       //   <StatusBar barStyle="light-content" backgroundColor="#091850" />
//       //   <Screen navigation={this.props.navigation} />
//       // </View>
//       <FixScreenWithoutPadding children={this.screenView()} />
//     );
//   }
// }




// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: "center",
//     alignItems: "center",
//     backgroundColor: "#F5FCFF"
//   },
//   welcome: {
//     fontSize: 20,
//     textAlign: "center",
//     margin: 10
//   },
//   instructions: {
//     textAlign: "center",
//     color: "#333333",
//     marginBottom: 5
//   }
// });