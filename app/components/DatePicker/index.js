import React, { useContext } from 'react';
import {
    View,
    TouchableOpacity,
    ScrollView,
    ImageBackground,
    TextInput,
    Image,
} from 'react-native';
import { SafeAreaView } from 'react-navigation';
import {
    Input,
    Icon,
    Text,
    Button,
    Checkbox,
} from 'galio-framework';

import Styles from './styles';
import Colors from '../../values/colors';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import strings from '../../values/stringEn';


export const AppDatePicker = (props) => {
    const { date,selectedDate = '', setSelectedDate = '', value, help, onChangeText, iconRight, iconRightShow, iconLeft, type, label, labelText, iconLeftShow, onFocus, onBlur, placeholder, secureTextEntry, extraStyle, ActiveTint } = props;

    console.log('help', help);

    return (
        <View>
            <View style={[Styles.formGroup]}>
                {label && <Text style={ActiveTint ? Styles.inputLableActive : Styles.inputLable}>{labelText}</Text>
                }
                <View style={ActiveTint ? Styles.inputViewActive : Styles.inputViewInActive}>
                    {iconLeftShow &&
                        <View style={Styles.inputRow}  >
                            <Image
                                resizeMode={'contain'}
                                style={{ alignSelf: 'center' }}
                                source={iconLeft}
                                tintColor={Colors.EXTRACOLORS.ActiveColor}
                            />
                        </View>
                    }

                    <TextInput
                        onFocus={onFocus}
                        onBlur={onBlur}
                        editable={false}
                        style={Styles.textInput}
                        placeholder={placeholder}
                        value={selectedDate}
                        textColor={Colors.NavyBlue}
                    />
                    <DatePicker
                        mode={'date'}
                        // maxDate={moment().subtract(16, "years")}
                        date={date}
                        formate={'ll'}
                        textColor={Colors.NavyBlue}
                        onDateChange={(select) => setSelectedDate(select)}
                        style={Styles.datePicker}
                        initialDate={moment().subtract(16, "years")}
                        confirmBtnText={strings.confirm}
                        cancelBtnText={strings.canecl}
                    // minDate={moment().subtract(16, "years")}
                    />


                </View>
                {help != null && help != '' && <Text style={Styles.helpText}>{help}</Text>}

            </View>


        </View>
    )
};

