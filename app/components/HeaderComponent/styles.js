import {StyleSheet, Platform} from 'react-native';
import Util from '../../common/util';
import Colors, {getTheme} from '../../values/colors';
import {THEMEKEY} from '../../hooks/constants';

const homeStyle = StyleSheet.create({
  // SIDE MENU STYLE

  iconContainer: {
    flexDirection: 'row',
    // width: Util.getWidth(50),
    justifyContent: 'space-evenly',
  },
  iconContainerTwo: {
    height: Util.getHeight(7),
    justifyContent: 'center',
    // alignItems: 'center',
    paddingHorizontal: Util.getWidth(3),
    borderRadius: 20,
    alignSelf: 'center',
    alignItems: 'center',
    right: Util.getWidth(2),
  },
  iconContainerSelectAll: {
    flexDirection: 'row',
    height: Util.getHeight(7),
    justifyContent: 'space-evenly',
    // alignItems: 'center',
    // paddingHorizontal: Util.getWidth(3),
    borderRadius: 20,
    // alignSelf: 'center',
    //right: Util.getWidth(2),
  },
  titleStyle: {
    fontWeight: 'bold',
    fontSize: Util.normalize(23),
    textAlign:'left',
    color: Colors.EXTRACOLORS.WHITE,
    // marginLeft:'-90%'
  },

  titleStyleTwo: {
    fontWeight: 'bold',
    fontSize: Util.normalize(16),
    color: Colors.HEADERTITLE,
    marginLeft: '-8%',
  },
  badgeStyle: {
    backgroundColor: Colors.AppTheme.ColorPrimary,
    width: Util.getWidth(6.6),
    height: Util.getWidth(6.6),
    justifyContent: 'center',
    alignItems: 'center',
    // borderWidth: 0.6,

    borderColor: Colors.EXTRACOLORS.DEARKBORDER,
    marginTop: '-105%',
    alignSelf: 'flex-end',
    padding: 3,
    borderRadius: Util.getWidth(4),
  },

  checkBoxStyle: {
    borderRadius: Util.getWidth(5),
    borderWidth: 1.2,
    height: Util.getWidth(5),
    width: Util.getWidth(5),

    marginRight: Util.getWidth(12),
  },
  rightIcon: {
    height: Util.getHeight(6),
    width: Util.getHeight(6),
    marginRight: '6%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconStyle: {
    width: 25,
    height: 30,
    // tintColor: Colors.THEME.BACKGROUND,
  },
  moreIcon: {
    height: Util.getHeight(3.5),
    width: Util.getWidth(4.5),
    resizeMode: 'contain',
    // padding:,
    alignSelf: 'center',
    tintColor: Colors.EXTRACOLORS.MUTED,
  },
  clearAll: {
    color: Colors.AppTheme.ColorPrimary,
    alignSelf: 'center',
    fontSize: Util.normalize(14),
    fontWeight: 'bold',
    position: 'absolute',
    right: Util.getWidth(6),
    fontFamily: Util.getFontFamily('regular'),
  },
  selectAll: {
    color: Colors.HEADERTITLE,
    alignSelf: 'center',
    fontSize: Util.normalize(14),
    fontWeight: 'bold',
    marginLeft: '-8%',

    right: Util.getWidth(6),
    fontFamily: Util.getFontFamily('regular'),
  },
  delete: {
    height: 30,
    backgroundColor: Colors.MODELNUMBERBTNCOLOR,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
    marginHorizontal: 10,
    borderRadius: 20,
    alignSelf: 'center',
  },
  addTools: {
    height: Util.getHeight(5),
    backgroundColor: Colors.APPTHEME,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 15,
    marginHorizontal: Util.getWidth(4),
    borderRadius: Util.getHeight(3),
    alignSelf: 'center',
    marginBottom: 2.5,
  },
  invite: {
    height: Util.getHeight(4),
    backgroundColor: Colors.AppTheme.ColorPrimary,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: Util.getHeight(2.7),
    paddingVertical: Util.getHeight(1),
    marginHorizontal: Util.getHeight(1),
    borderRadius: Util.getHeight(3),
    alignSelf: 'center',
  },
  export: {
    height: Util.getHeight(4),
    backgroundColor: Colors.AppTheme.ColorPrimary,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: Util.getHeight(2.7),
    paddingVertical: Util.getHeight(1),
    marginHorizontal: Util.getHeight(3),
    borderRadius: Util.getHeight(3),
    alignSelf: 'center',
  },
  more: {
    height: Util.getHeight(5.4),
    justifyContent: 'center',
    backgroundColor:'white',
    // alignItems: 'center',
    paddingHorizontal: Util.getWidth(3),
    borderRadius: 8,
    // alignSelf: 'center',
    right: Util.getWidth(0.5),
    //top: -10,
  },
  iconBackStyle: {
    height: Util.getHeight(5.4),
    justifyContent: 'center',
   
    paddingHorizontal: Util.getWidth(3),
    
  },
  deleteText: {
    marginLeft: 4,
    fontFamily: Util.getFontFamily('regular'),
  },
  toolText: {
    marginLeft: 4,
    fontSize: Util.normalize(12),
    fontFamily: Util.getFontFamily('regular'),
    color: Colors.EXTRACOLORS.WHITE,
  },

  inviteUser: {
    marginLeft: 4,
    fontFamily: Util.getFontFamily('regular'),
    fontSize: Util.normalize(12),
    color: Colors.THEME.BACKGROUND,
  },
  countText: {
    color: 'white',
    alignSelf: 'center',
    fontFamily: Util.getFontFamily('regular'),
    fontSize: Util.normalize(10),
    paddingTop: 3,
  },
});
export default homeStyle;
