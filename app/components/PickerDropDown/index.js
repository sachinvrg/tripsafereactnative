import React, { useContext } from 'react';
import {
    View,
    TouchableOpacity,
    ScrollView,
    ImageBackground,
    TextInput,
    Image,
} from 'react-native';
import { SafeAreaView } from 'react-navigation';
import {
    Input,
    Icon,
    Text,
    Button,
    Checkbox,
} from 'galio-framework';

import Styles from './Styles';

import Colors from '../../values/colors';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import strings from '../../values/stringEn';
import RNPickerSelect from 'react-native-picker-select';
import Util from '../../common/util';
import images from '../../assets/Images/imagePath/images';


export const PickerDropDown = (props) => {
    const { isEditable,
        maxLength,
        selectedValue,
        options,
        propname,
        onChangeText,

        disabled,
        value,
        help,
        iconRight,
        iconRightShow,
        iconLeft,
        type,
        label,
        labelText,
        iconLeftShow,
        onFocus,
        onBlur,
        placeholder,
        secureTextEntry,
        extraStyle,
        ActiveTint } = props;

    console.log('help', help);

    return (
        <View>
            <View style={[Styles.formGroup]}>
                {label && <Text style={ActiveTint ? Styles.inputLableActive : Styles.inputLable}>{labelText}</Text>
                }
                <View style={Styles.inputViewInActive}>

                    <View style={{
                        width: Util.getWidth(78),
                        justifyContent:'center',
                        // backgroundColor: 'red',
                        height: Util.getHeight(7),
                    }}>
                        <RNPickerSelect
                            value={selectedValue}
                            onValueChange={(value, index) => {
                                onChangeText(propname, value);
                            }}
                            placeholder={placeholder}
                            showPlaceholder={
                                selectedValue &&
                                    selectedValue != NaN &&
                                    selectedValue != null &&
                                    selectedValue != 'null' &&
                                    selectedValue != undefined &&
                                    selectedValue != 'undefined'
                                    ? true
                                    : false
                            }
                            items={options}
                            Icon={() => {
                                return null;
                            }}
                            style={{
                                inputIOS: {
                                    backgroundColor: 'transparent',
                                    color: Colors.EXTRACOLORS.DARKBLUE,
                                    fontSize: Util.normalize(15),
                                },
                                inputAndroid: {
                                    color: Colors.EXTRACOLORS.DARKBLUE,
                                    fontSize: Util.normalize(15),
                                },
                            }}
                            disabled={disabled}
                        />



                    </View>



                    <View style={Styles.rightIconView}>

                        <Image
                            resizeMode={'contain'}
                            source={images.dropDown}
                            placeholder="Email"
                        />
                    </View>

                </View>
                {help != null && help != '' && <Text style={Styles.helpText}>{help}</Text>}

            </View>


        </View>
    )
};

