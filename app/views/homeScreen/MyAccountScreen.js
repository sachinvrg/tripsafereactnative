
import React, { useContext, useEffect, useState } from 'react';
import { View, Image, FlatList, StatusBar } from 'react-native';
import { Text } from 'galio-framework';
import Spinner from 'react-native-loading-spinner-overlay';
import { NavigationActions, StackActions } from 'react-navigation';

// Hooks, Api component calling
import AppContext from '../../hooks/app-context';
import { useHttp } from '../../hooks/useHttpss';
import {
  GET,
  USERDETAILS,
  POST
} from '../../hooks/constants';

// Global helper classes
// import HeaderComponent, {
//   AdminHeaderComponent,
// } from '../../components/DrawerHeader';

import strings from '../../values/stringEn';

// Local styles
import styles from './styles';
import Colors from '../../values/colors';
import Util from '../../common/util';
import helper from '../../common/helper';
import { TouchableOpacity } from 'react-native-gesture-handler';
import HeaderComponent from '../../components/HeaderComponent';
import LinearGradient from 'react-native-linear-gradient';
import images from '../../assets/Images/imagePath/images';
import { ProfileOptionVew } from '../../components/ProfileOptionView';
import { logOut } from '../../network/Networksettings';
import ImagePickerView from '../../components/ImagePicker';


const RecentTestScreen = props => {
  const { navigation } = props;
  const context = useContext(AppContext);
  const { state } = context;
  const { UserDetails } = state;

  const [loader, setLoader] = useState(false);
  const [formData, setFormData] = useState({
    image: {
      value: '',
      propname: 'image',
      err: false,
      required: false,
      errMsg: '',
    },
  });
  const [error, setError] = useState(false);

  useEffect(() => {


  }, []);


  const changeText = (name, value) => {
    console.log('name', name, value);
    setFormData({
      ...formData,
      [name]: {
        ...formData[name],
        err: false,
        errMsg: '',
        value,
        propname: name,
      },
    });
  };

  const logOutApiCall = () => {
    setLoader(true);
    useHttp(logOut, POST, state.UserDetails.authorization)
      .then(res => {
        console.log('userdata on home screen', res.data.data);

        helper.remove_item_async(USERDETAILS);

        // navigation.replace('LoginScreen');

        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'LoginScreen' })],
        });

        props.navigation.dispatch(resetAction);
        setTimeout(() => {
          setLoader(false);
        }, 500);
      })
      .catch(_err => {
        console.log("_erronlogout", _err.message);

        setTimeout(() => {
          setLoader(false);
        }, 500);
      });
  };




  return (
    <LinearGradient
      colors={['#113CB4', '#091850']}

      style={styles.linearGradient}
    >
      <StatusBar barStyle='light-content' hidden={false} backgroundColor="#113CB4" />
      {<Spinner visible={loader} />}

      <HeaderComponent
        title="My Account"
        notification={true}

      />
      <View style={styles.viewContainer}>

        <View style={styles.viewImageContainer}>
          <View style={styles.viewLeft}>
            <View style={styles.profilePicImage}>
              <Image
                source={UserDetails.avatar ? { uri: `${UserDetails.avatar}` } : images.profileImage}
                style={styles.profilePic}
              />
            </View>


            <ImagePickerView
              imagePath={path => changeText('image', path)}
              style={styles.cameraView}>
              <Image
                source={images.camera}
                style={styles.cameraIcon}
              />
            </ImagePickerView>
            {/* <View style={styles.cameraView}>
              <TouchableOpacity>
                <Image
                  source={images.camera}
                  style={styles.cameraIcon}
                />
              </TouchableOpacity>
            </View> */}

          </View>
          <View style={styles.viewRight}>
            <Text style={styles.textName}>
              {`${UserDetails.first_name} ${UserDetails.last_name}`}
            </Text>
            <Text style={styles.textId}>
              {`${UserDetails.email}`}
            </Text>


          </View>

        </View>
        <View style={styles.border} />

        <ProfileOptionVew
          onClick={() => navigation.navigate("PersonalDetails")}

          iconLeft={images.personalDetails}
          optionText={"Personal Details"}
          iconRightShow={true} />
        <ProfileOptionVew
          iconLeft={images.healthCertificates}

          optionText={"Health Certificates"}
          iconRightShow={true} />
        <ProfileOptionVew
          iconLeft={images.passport_ids2}

          optionText={"Passports & ID’s"}
          iconRightShow={true} />
        <ProfileOptionVew
          iconLeft={images.Authorisation2}

          optionText={"Authorisation & Access Control"}
          iconRightShow={true} />
        <ProfileOptionVew
          iconLeft={images.help_support}

          optionText={"Help & Support"}
          iconRightShow={true} />
        <ProfileOptionVew
          iconLeft={images.logout}

          onClick={() => logOutApiCall()}
          optionText={"Logout"}
        />




      </View>

    </LinearGradient >
  );
};

export default RecentTestScreen;
