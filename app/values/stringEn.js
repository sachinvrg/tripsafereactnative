import LocalizedStrings from 'react-native-localization';

const strings = new LocalizedStrings({
  'en-US': {
    // login screen
    enterEmail: 'Enter your email',
    enterFirstName: 'Enter First Name',
    enterLocation:'Enter your address',
    enterLastName: 'Enter Last Name',
    enterPassword: 'Enter your password',
    enterBloodGroup:'Enter blood group',
    enterTravelInsurance:'Travel insurance number',
    enterMobileNumber: 'Enter Mobile Number',
    enterDoctorName: 'Enter doctor name',

    forgotPsw: 'Forgot Password?',
    resetPassword: 'Reset Password',
    byClicking: 'by clicking above button you agree to',
    rememberMe: 'Remember Me',
    login: 'Log into your account',
    loginToContinue: 'Login to continue',
    createAccount: 'Create an account',
    turmPrivacy: 'T&C / Privacy policy',
    pleaseEnterCredentials: 'Please enter your credentials to login',
    firstName: 'First Name',
    birthday: 'Birthday',
    dateOfBirth: 'Date of Birth',
    lastName: 'Last Name',
    contactNumber: 'Contact Number',
    password: 'Password',
    email: 'Email',
    emailAddress: 'EMAIL ADDRESS',
    forgotPasswordText: 'Forgot your Login Password?',
    address:'Address',

    save:'Save',

    paregraph:'This functionality is here to help you in case of an Emergency. It is only used by first responders to get access to critical information in an event that your unable to provide it to them.',

    forgetSentence: 'No problem. Enter the email address associated with your account. And we will email you instructions to reset your password',
    canecl: 'Cancel',
    confirm:'Confirm',

    // signup screen
    signUp: 'Sign Up',
    update: 'Update',
    propName: {
      firstName: 'firstName',
      lastName: 'lastName',
      email: 'email',
      mobileNumber: 'mobileNumber',
      newPassword: 'newPassword',
      repeatPassword: 'repeatPassword',
      password: 'password',
    },

    errors: {
      noSpace: 'No Space Allowed',
      requiredField: 'This field is required.',
      specialCharacterNotAllowed: 'special character not allowed',
      validEmailError: 'Enter a valid email.',
      // validEmailMobileError: 'Enter a valid email or mobile number.',
      validEmailMobileError: 'Enter a valid email.',

      onlyAlphabatesAlowed: 'Only contain alphabetical characters.',
      Max_Limit: 'Please enter minimum eight/8 characters',
      MatchPass: 'New Password and Repeat Password does not match',
      enterValidMobileno: 'Enter a valid mobile Number',
    },

  },
});
export default strings;
