/** Created By jay soni **/
import React, { useEffect, useContext } from 'react';
import { View, StatusBar, ImageBackground, Image } from 'react-native';

// Hooks, Api component calling
import AppContex from '../../hooks/app-context';
import {
  USERDETAILS, CREDENTIALS

} from '../../hooks/constants';
// Global helper classes
import GlobalStyles from '../../assets/css/globalStyle';

// Local styles
import styles from './styles';
import images from '../../assets/Images/imagePath/images';
import helper from '../../common/helper';
import { useHttp } from '../../hooks/useHttpss';

const SplashScreen = props => {
  const { navigation } = props;
  console.log("props>>>", props);

  const context = useContext(AppContex);
  console.log("context>>>", context);

  const { state } = context;
  const authUserData = helper.fetch_item_async(USERDETAILS);
  console.log('authUserData', authUserData);



  useEffect(() => {
    setTimeout(() => {

      authUserData
        .then(data => {


          if (data && data != 'null') {
            context.dispatch({
              type: USERDETAILS,
              payload: JSON.parse(data),
            });

            navigation.replace('Home')

          } else {
            navigation.replace('IntroSlider');
          }

        })
        .catch(() => {
          navigation.replace('IntroSlider');
        });



    }, 1000);

  }, []);


  return (
    <View style={GlobalStyles.ViewStyle}>
      <StatusBar hidden={true} translucent={true} />

      <Image source={images.splashBg} style={styles.container} />

    </View>
  );
};

export default SplashScreen;
