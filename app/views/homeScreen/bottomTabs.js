import React from 'react';
import { Platform } from 'react-native';
import { Icon } from 'galio-framework';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import MyHomeScreen from './HomeScreen';
import RecentTestScreen from './RecentTestScreen';
import LoginScreen from '../loginScreen/index'




import TXTabBar from './bottomView';
import RecentVisitedScreen from './RecentVisitedScreen';
import MyAccountScreen from './MyAccountScreen';

const HomeStack = createStackNavigator(
    {
        HomeScreen: { screen: MyHomeScreen },


    },

    {
        defaultNavigationOptions: {
            gesturesEnabled: false,
            header: null,
        },
    },

);

const RecentTestStack = createStackNavigator({
    RecentTestScreen: {
        screen: RecentTestScreen,
        navigationOptions: ({ navigation }) => ({
            header: null
        }),
    },
});
const RecentVisitedStack = createStackNavigator({
    RecentVisitedScreen: {
        screen: RecentVisitedScreen,
        navigationOptions: ({ navigation }) => ({
            header: null
        }),
    },
});
const MyAccountStack = createStackNavigator({
    MyAccountScreen: {
        screen: MyAccountScreen,
        navigationOptions: ({ navigation }) => ({
            header: null
        }),
    },
});

const TabNavigator = createBottomTabNavigator(
    {
        Dashboard: {
            screen: MyHomeScreen,
        },

        RecentTest: {
            screen: RecentTestStack
        },
        Places: {
            screen: RecentVisitedStack
        },
        MyAccount: {
            screen: MyAccountStack
        }

    },
    {
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarComponent: ({ navigation }) => <TXTabBar navigation={navigation} />,
        }),

        swipeEnabled: true,
        // animationEnabled: true,
        tabBarOptions: {
            showLabel: false,
        },
    },
);

export default createAppContainer(TabNavigator);