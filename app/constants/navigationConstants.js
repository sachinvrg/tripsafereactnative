export const BottomNavOption = [
    {
        active: 'dashboradActive',
        InActive: 'dashboardeInactive',
        screen: 'Dashboard',
        name:'Home',
        index: 0,
    },
    {
        active: 'recentTestActive',
        InActive: 'recentTestInactive',
        screen: 'RecentTest',
        name:'Recent Tests',
        index: 1,
    },
    {
        active: 'placesActive',
        InActive: 'placesInactive',
        screen: 'Places',
        name:'Places',

        index: 2,
    },
    {
        active: 'myAccountActive',
        InActive: 'myAccountInactive',
        screen: 'MyAccount',
        name:'Account',

        index: 3,
    },

]